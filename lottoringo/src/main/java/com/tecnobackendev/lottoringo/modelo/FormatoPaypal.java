package com.tecnobackendev.lottoringo.modelo;

public class FormatoPaypal {
	private String token;
	private String tokenUsuario;
	private String cuentaPaypal;
	
	public FormatoPaypal() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getToken() {
		return token;
	}
	public String getCuentaPaypal() {
		return cuentaPaypal;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public void setCuentaPaypal(String cuentaPaypal) {
		this.cuentaPaypal = cuentaPaypal;
	}

	public String getTokenUsuario() {
		return tokenUsuario;
	}

	public void setTokenUsuario(String tokenUsuario) {
		this.tokenUsuario = tokenUsuario;
	}

	@Override
	public String toString() {
		return "FormatoPaypal [token=" + token + ", tokenUsuario=" + tokenUsuario + ", cuentaPaypal=" + cuentaPaypal
				+ "]";
	}
	
	

}
