package com.tecnobackendev.lottoringo.modelo;

import com.tecnobackendev.lottoringo.entidades.Sorteos;

public class FormatoSorteosStatus {
	private Sorteos sorteos;
	private boolean vigente;
	
	public FormatoSorteosStatus() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Sorteos getSorteos() {
		return sorteos;
	}

	public boolean isVigente() {
		return vigente;
	}

	public void setSorteos(Sorteos sorteos) {
		this.sorteos = sorteos;
	}

	public void setVigente(boolean vigente) {
		this.vigente = vigente;
	}
	
	

}
