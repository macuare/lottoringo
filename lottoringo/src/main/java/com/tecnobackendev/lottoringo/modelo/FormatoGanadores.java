package com.tecnobackendev.lottoringo.modelo;

public class FormatoGanadores {
	private int id;
	private String monto;
	private String imagenB64;
	private String nombreUsuario;
	private String fechaGanador;
	
	public FormatoGanadores() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public String getMonto() {
		return monto;
	}

	public String getImagenB64() {
		return imagenB64;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public String getFechaGanador() {
		return fechaGanador;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public void setImagenB64(String imagenB64) {
		this.imagenB64 = imagenB64;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public void setFechaGanador(String fechaGanador) {
		this.fechaGanador = fechaGanador;
	}

	@Override
	public String toString() {
		return "FormatoGanadores [id=" + id + ", monto=" + monto + ", imagenB64=" + imagenB64 + ", nombreUsuario="
				+ nombreUsuario + ", fechaGanador=" + fechaGanador + "]";
	}
	
	
	

}
