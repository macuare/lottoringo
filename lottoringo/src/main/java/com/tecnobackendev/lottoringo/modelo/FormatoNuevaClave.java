package com.tecnobackendev.lottoringo.modelo;

public class FormatoNuevaClave {
	private String usuario;
	private int codigoPin;
	private String contraseña;
	
	public FormatoNuevaClave() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public FormatoNuevaClave(String usuario, int codigoPin, String contraseña) {
		super();
		this.usuario = usuario;
		this.codigoPin = codigoPin;
		this.contraseña = contraseña;
	}

	public String getUsuario() {
		return usuario;
	}

	public int getCodigoPin() {
		return codigoPin;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public void setCodigoPin(int codigoPin) {
		this.codigoPin = codigoPin;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	@Override
	public String toString() {
		return "FormatoNuevaClave [usuario=" + usuario + ", codigoPin=" + codigoPin + ", contraseña=" + contraseña
				+ "]";
	}
	
	

}
