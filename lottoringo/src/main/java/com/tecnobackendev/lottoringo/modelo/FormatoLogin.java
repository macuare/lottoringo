package com.tecnobackendev.lottoringo.modelo;

public class FormatoLogin {
	private String usuario;
	private String contraseña;
	
	public FormatoLogin(String usuario, String contraseña) {
		super();
		this.usuario = usuario;
		this.contraseña = contraseña;
	}

	public String getUsuario() {
		return usuario;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	@Override
	public String toString() {
		return "FormatoLogin [usuario=" + usuario + ", contraseña=" + contraseña + "]";
	}
	
	
	
	
	

}
