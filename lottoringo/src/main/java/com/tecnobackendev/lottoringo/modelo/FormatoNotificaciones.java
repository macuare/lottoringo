package com.tecnobackendev.lottoringo.modelo;

public class FormatoNotificaciones {
	private String tokenFirebase;
	private String token;
	
	
	
	public FormatoNotificaciones() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getTokenFirebase() {
		return tokenFirebase;
	}

	public void setTokenFirebase(String tokenFirebase) {
		this.tokenFirebase = tokenFirebase;
	}

	public String getToken() {
		return token;
	}


	public void setToken(String token) {
		this.token = token;
	}


	@Override
	public String toString() {
		return "FormatoNotificaciones [tokenFirebase=" + tokenFirebase + ", token=" + token + "]";
	}


	
	
	

}
