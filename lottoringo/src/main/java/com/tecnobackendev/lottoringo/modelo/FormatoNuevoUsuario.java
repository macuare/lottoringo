package com.tecnobackendev.lottoringo.modelo;

import java.util.Date;

public class FormatoNuevoUsuario {
	
	private String primerNombre;
	private String segundoNombre;
	private String correo;
	private String clave;
	private String telefono;
	private Date fechaNacimiento;
	private String cuentaPaypal;
	private int numeroPin;
	private String imagenB64;
	
	
	public FormatoNuevoUsuario() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	public String getImagenB64() {
		return imagenB64;
	}
	public void setImagenB64(String imagenB64) {
		this.imagenB64 = imagenB64;
	}
	public String getPrimerNombre() {
		return primerNombre;
	}
	public String getSegundoNombre() {
		return segundoNombre;
	}
	public String getCorreo() {
		return correo;
	}
	public String getClave() {
		return clave;
	}
	public String getTelefono() {
		return telefono;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public String getCuentaPaypal() {
		return cuentaPaypal;
	}
	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}
	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public void setCuentaPaypal(String cuentaPaypal) {
		this.cuentaPaypal = cuentaPaypal;
	}

	public int getNumeroPin() {
		return numeroPin;
	}

	public void setNumeroPin(int numeroPin) {
		this.numeroPin = numeroPin;
	}



	@Override
	public String toString() {
		return "FormatoNuevoUsuario [primerNombre=" + primerNombre + ", segundoNombre=" + segundoNombre + ", correo="
				+ correo + ", clave=" + clave + ", telefono=" + telefono + ", fechaNacimiento=" + fechaNacimiento
				+ ", cuentaPaypal=" + cuentaPaypal + ", numeroPin=" + numeroPin + ", imagenB64=" + imagenB64 + "]";
	}

	

}
