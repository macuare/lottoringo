package com.tecnobackendev.lottoringo.modelo;

public class FormatoComprarBoleto {
	private String tokenUsuario;
	private String token;
	private int idSorteo;
	private int bola1;
	private int bola2;
	private int bola3;
	private int bola4;
	private int bola5;
	private int bolaSuerte;
	
	public FormatoComprarBoleto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getTokenUsuario() {
		return tokenUsuario;
	}

	public String getToken() {
		return token;
	}

	public int getIdSorteo() {
		return idSorteo;
	}

	public int getBola1() {
		return bola1;
	}

	public int getBola2() {
		return bola2;
	}

	public int getBola3() {
		return bola3;
	}

	public int getBola4() {
		return bola4;
	}

	public int getBola5() {
		return bola5;
	}

	public int getBolaSuerte() {
		return bolaSuerte;
	}

	public void setTokenUsuario(String tokenUsuario) {
		this.tokenUsuario = tokenUsuario;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setIdSorteo(int idSorteo) {
		this.idSorteo = idSorteo;
	}

	public void setBola1(int bola1) {
		this.bola1 = bola1;
	}

	public void setBola2(int bola2) {
		this.bola2 = bola2;
	}

	public void setBola3(int bola3) {
		this.bola3 = bola3;
	}

	public void setBola4(int bola4) {
		this.bola4 = bola4;
	}

	public void setBola5(int bola5) {
		this.bola5 = bola5;
	}

	public void setBolaSuerte(int bolaSuerte) {
		this.bolaSuerte = bolaSuerte;
	}

	@Override
	public String toString() {
		return "FormatoComprarBoleto [tokenUsuario=" + tokenUsuario + ", token=" + token + ", idSorteo=" + idSorteo
				+ ", bola1=" + bola1 + ", bola2=" + bola2 + ", bola3=" + bola3 + ", bola4=" + bola4 + ", bola5=" + bola5
				+ ", bolaSuerte=" + bolaSuerte + "]";
	}
	
	

}
