package com.tecnobackendev.lottoringo.modelo;

public class FormatoGenerarPin {
	private String correo;

	
	public FormatoGenerarPin() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	@Override
	public String toString() {
		return "FormatoGenerarPin [correo=" + correo + "]";
	}
	
	
}
