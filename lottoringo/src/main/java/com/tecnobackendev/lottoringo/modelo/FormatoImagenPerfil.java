package com.tecnobackendev.lottoringo.modelo;

public class FormatoImagenPerfil {
	private String token;
	private String tokenUsuario;
	private String imagenB64;
	
	public FormatoImagenPerfil() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getToken() {
		return token;
	}

	public String getTokenUsuario() {
		return tokenUsuario;
	}

	public String getImagenB64() {
		return imagenB64;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setTokenUsuario(String tokenUsuario) {
		this.tokenUsuario = tokenUsuario;
	}

	public void setImagenB64(String imagenB64) {
		this.imagenB64 = imagenB64;
	}

	@Override
	public String toString() {
		return "FormatoImagenPerfil [token=" + token + ", tokenUsuario=" + tokenUsuario + ", imagenB64=" + imagenB64
				+ "]";
	}


	
	
}
