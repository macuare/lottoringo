package com.tecnobackendev.lottoringo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LottoringoApplication {

	public static void main(String[] args) {
		SpringApplication.run(LottoringoApplication.class, args);
	}

}
