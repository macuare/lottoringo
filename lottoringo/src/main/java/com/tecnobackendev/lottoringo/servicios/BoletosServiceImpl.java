package com.tecnobackendev.lottoringo.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.tecnobackendev.lottoringo.entidades.Boletos;
import com.tecnobackendev.lottoringo.entidades.Sorteos;
import com.tecnobackendev.lottoringo.entidades.Usuarios;
import com.tecnobackendev.lottoringo.repositorios.BoletosRepository;

@Service
public class BoletosServiceImpl implements BoletosService{
	
	@Autowired
	private BoletosRepository repositorio;

	@Override
	public List<Boletos> listarBoletos() {
		// TODO Auto-generated method stub
		return repositorio.findAll();
	}

	@Override
	public List<Boletos> listarBoletos(int indicePagina, int cantidadElementos) {
		// TODO Auto-generated method stub
		Pageable lote = PageRequest.of(indicePagina, cantidadElementos, Sort.by("idBoletos").descending());
		return repositorio.findAll(lote).getContent();
	}

	@Override
	public List<Boletos> buscarBoletosPorUsuario(Usuarios usuario) {
		// TODO Auto-generated method stub
		Boletos boleto = new Boletos();
		boleto.setUsuarios(usuario);
		
		ExampleMatcher em = ExampleMatcher.matching().withIgnorePaths("idBoletos","bola1","bola2","bola3","bola4","bola5","bolaSuerte");
		
		
		
		Example<Boletos> example = Example.of(boleto, em);
		
		return repositorio.findAll(example);
	}

	@Override
	public List<Boletos> buscarBoletosPorSorteo(Sorteos sorteo) {
		// TODO Auto-generated method stub
		Boletos boleto = new Boletos();
		boleto.setSorteos(sorteo);
		
		ExampleMatcher em = ExampleMatcher.matching().withIgnorePaths("idBoletos","bola1","bola2","bola3","bola4","bola5","bolaSuerte");
		
		Example<Boletos> example = Example.of(boleto, em);
		
		return repositorio.findAll(example);
	}

	@Override
	public Optional<Boletos> buscarBoletosPorId(int id) {
		// TODO Auto-generated method stub
		return repositorio.findById(id); 
	}

	@Override
	public Boletos agregarNuevoBoleto(Boletos boleto) {
		// TODO Auto-generated method stub
		return repositorio.save(boleto);
	}

	@Override
	public Boletos actualizarBoleto(Boletos boleto) {
		// TODO Auto-generated method stub
		return repositorio.save(boleto);
	}

	@Override
	public void borrarBoleto(int id) {
		// TODO Auto-generated method stub
		repositorio.deleteById(id);
	}

}
