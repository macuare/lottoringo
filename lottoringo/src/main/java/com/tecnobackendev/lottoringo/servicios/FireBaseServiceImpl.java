package com.tecnobackendev.lottoringo.servicios;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.google.firebase.messaging.TopicManagementResponse;
import com.tecnobackendev.lottoringo.FcmSettings;

@Service
public class FireBaseServiceImpl implements FireBaseService{
	@Autowired
	private FcmSettings fcmSettings;
	
	private final String TOPICO = "LottoRingoNews";
	
	@Override
	public void inicializarServicio() {
		
		Path p = Paths.get(fcmSettings.getServiceAccountFile());
		
	    try (InputStream serviceAccount = Files.newInputStream(p)) {
	      FirebaseOptions options = 
	    		  new FirebaseOptions.Builder()
	    		  					 .setCredentials(GoogleCredentials.fromStream(serviceAccount))
	    		  					 .build();
	      try {
	    	  FirebaseApp.initializeApp(options);
	    	  System.out.println("Incializacion completa");
	      }catch(IllegalStateException e) {
	    	  System.out.println("Incializacion existente");
	      }
	      
	      
	    }
	    catch (IOException e) {
	     System.out.println("Error intentando inicilizar firebase -> "+e.toString());
	    }
		
	}

	@Override
	public void enviarMensaje(String titulo, String mensaje) {
		this.mensajeTopico(titulo, mensaje);
	}
		
	private void mensajeTopico(String titulo, String mensaje) {
		// The topic name can be optionally prefixed with "/topics/".
		//String topic = "LottoRingoNews";

		// See documentation on defining a message payload.
		Message message = Message.builder()
		    .setNotification(new Notification(titulo, mensaje))
		    .setAndroidConfig(AndroidConfig.builder()
		            .setTtl(3600 * 1000)
		            .setNotification(AndroidNotification.builder()
		                .setIcon("logo")
		                .setColor("#2BA7EF")
		                .setSound("default")
		                .build())
		            .build())
		    .setTopic(TOPICO)
		    .build();

		// Send a message to the devices subscribed to the provided topic.
		String response = null;
		try {
			response = FirebaseMessaging.getInstance().send(message);
		} catch (FirebaseMessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Response is a message ID string.
		System.out.println("Successfully sent message: " + response);
	}
	
	 public Message androidMessage() {
		    // [START android_message]
		    Message message = Message.builder()
		        .setAndroidConfig(AndroidConfig.builder()
		            .setTtl(3600 * 1000) // 1 hour in milliseconds
		            .setPriority(AndroidConfig.Priority.NORMAL)
		            .setNotification(AndroidNotification.builder()
		                .setTitle("$GOOG up 1.43% on the day")
		                .setBody("$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.")
		                .setIcon("stock_ticker_update")
		                .setColor("#f45342")
		                .build())
		            .build())
		        .setTopic("industry-tech")
		        .build();
		    // [END android_message]
		    return message;
		  }
	 
	 @Override
	 public void suscripcionTopico(String tokenFirebase) {
		// These registration tokens come from the client FCM SDKs.
		 List<String> registrationTokens = Arrays.asList(
				 tokenFirebase
		 );

		 // Subscribe the devices corresponding to the registration tokens to the
		 // topic.
		 TopicManagementResponse response = null;
		try {
			response = FirebaseMessaging.getInstance().subscribeToTopic(
			     registrationTokens, TOPICO);
		} catch (FirebaseMessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 // See the TopicManagementResponse reference documentation
		 // for the contents of response.
		 System.out.println(response.getSuccessCount() + " tokens were subscribed successfully");
	 }

}
