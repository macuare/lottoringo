package com.tecnobackendev.lottoringo.servicios;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.tecnobackendev.lottoringo.entidades.Sorteos;
import com.tecnobackendev.lottoringo.repositorios.SorteosRepository;

@Service
public class SorteosServiceImpl implements SorteosService{
	@Autowired
	private SorteosRepository sorteosRepository;
	
	@Override
	public List<Sorteos> listarSorteos() {
		// TODO Auto-generated method stub
		return sorteosRepository.findAll();
	}

	@Override
	public List<Sorteos> listarSorteos(int indicePagina, int cantidadSorteos) {
		// TODO Auto-generated method stub
		Pageable pagina = PageRequest.of(indicePagina, cantidadSorteos, Sort.by("fechaSorteo").descending()); 
		
		return sorteosRepository.findAll(pagina).getContent();
	}
	
	@Override
	public Optional<Sorteos> buscarSorteoPorId(int id) {
		// TODO Auto-generated method stub
		return sorteosRepository.findById(id);
	}

	@Override
	public Optional<Sorteos> buscarSorteoPorFechaSorteo(Date fechaSorteo) {
		// TODO Auto-generated method stub
		Sorteos sorteo = new Sorteos();
		sorteo.setFechaSorteo(fechaSorteo);
		
		Example<Sorteos> example = Example.of(sorteo);
		
		return sorteosRepository.findOne(example);
	}

	@Override
	public Sorteos agregarNuevoSorteo(Sorteos sorteo) {
		// TODO Auto-generated method stub
		return sorteosRepository.save(sorteo);
	}

	@Override
	public Sorteos actualizarSorteo(Sorteos sorteo) {
		// TODO Auto-generated method stub
		return sorteosRepository.save(sorteo);
	}

	@Override
	public void borrarSorteo(int id) {
		// TODO Auto-generated method stub
		sorteosRepository.deleteById(id);
		
	}

}
