package com.tecnobackendev.lottoringo.servicios;

import java.util.Date;
import java.util.List;
import java.util.Optional;


import com.tecnobackendev.lottoringo.entidades.Sorteos;

public interface SorteosService {
	public List<Sorteos> listarSorteos();	
	public List<Sorteos> listarSorteos(int indicePagina, int cantidadSorteos);
	
	public Optional<Sorteos> buscarSorteoPorId(int id);
	public Optional<Sorteos> buscarSorteoPorFechaSorteo(Date fechaSorteo);
	public Sorteos agregarNuevoSorteo(Sorteos sorteo);
	public Sorteos actualizarSorteo(Sorteos sorteo);
	public void borrarSorteo(int id);
}
