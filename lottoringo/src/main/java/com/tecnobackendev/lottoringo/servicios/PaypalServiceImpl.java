package com.tecnobackendev.lottoringo.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import com.tecnobackendev.lottoringo.entidades.Paypal;
import com.tecnobackendev.lottoringo.entidades.Usuarios;
import com.tecnobackendev.lottoringo.repositorios.PaypalRepository;

@Service
public class PaypalServiceImpl implements PaypalService{
	
	@Autowired
	private PaypalRepository paypalRepository;

	@Override
	public List<Paypal> listarPaypals() {
		// TODO Auto-generated method stub
		return paypalRepository.findAll();
	}

	@Override
	public Optional<Paypal> buscarPaypalsPorCorreo(String correoElectronico) {
		// TODO Auto-generated method stub
		Paypal paypal = new Paypal();
		paypal.setCorreo(correoElectronico);
		
		ExampleMatcher exampleMatcher = 
				ExampleMatcher.matching()
							  .withIgnorePaths("idPaypal");
		
		Example<Paypal> example = Example.of(paypal, exampleMatcher);
		
		return paypalRepository.findOne(example);
	}

	@Override
	public Optional<Paypal> buscarPaypalPorUsuario(String tokenUsuario) {
		// TODO Auto-generated method stub
		Usuarios usuarios = new Usuarios();
		usuarios.setIdUsuarios(tokenUsuario);
		
		Paypal paypal = new Paypal();
		paypal.setUsuarios(usuarios);
		
		ExampleMatcher exampleMatcher = 
				ExampleMatcher.matching()
							  .withIgnorePaths("idPaypal");
		
		Example<Paypal> example = Example.of(paypal, exampleMatcher);
		
		return paypalRepository.findOne(example);
	}

	@Override
	public Paypal agregarNuevaCuentaPaypal(Paypal paypal) {
		// TODO Auto-generated method stub
		return paypalRepository.saveAndFlush(paypal);
	}

	@Override
	public Paypal actualizarPaypal(Paypal paypal) {
		// TODO Auto-generated method stub
		return paypalRepository.saveAndFlush(paypal);
	}

	@Override
	public void borrarPaypal(int idPaypal) {
		// TODO Auto-generated method stub
		paypalRepository.deleteById(idPaypal);
		
	}

}
