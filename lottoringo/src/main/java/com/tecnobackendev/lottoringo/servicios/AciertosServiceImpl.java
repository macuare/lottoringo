package com.tecnobackendev.lottoringo.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tecnobackendev.lottoringo.entidades.Aciertos;
import com.tecnobackendev.lottoringo.repositorios.AciertosRepository;

@Service
public class AciertosServiceImpl implements AciertosService{

	@Autowired
	private AciertosRepository aciertosRepository;

	@Override
	public List<Aciertos> listarAciertos() {
		// TODO Auto-generated method stub
		return aciertosRepository.findAll();
	}

	@Override
	public Optional<Aciertos> buscarAciertoPorId(int id) {
		// TODO Auto-generated method stub
		return aciertosRepository.findById(id);
	}

	@Override
	public Aciertos agregarNuevoAcierto(Aciertos acierto) {
		// TODO Auto-generated method stub
		return aciertosRepository.save(acierto);
	}

	@Override
	public Aciertos actualizarAcierto(Aciertos acierto) {
		// TODO Auto-generated method stub
		return aciertosRepository.save(acierto);
	}

	@Override
	public void borrarAcierto(int id) {
		// TODO Auto-generated method stub
		aciertosRepository.deleteById(id);
	}

}
