package com.tecnobackendev.lottoringo.servicios;

public interface PagosDigitalService {
	
	public void crearPago();
	public void enviarPago();

}
