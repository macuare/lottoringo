package com.tecnobackendev.lottoringo.servicios;

import java.util.List;
import java.util.Optional;

import com.tecnobackendev.lottoringo.entidades.Boletos;
import com.tecnobackendev.lottoringo.entidades.Sorteos;
import com.tecnobackendev.lottoringo.entidades.Usuarios;


public interface BoletosService {
	public List<Boletos> listarBoletos();	
	public List<Boletos> listarBoletos(int indicePagina, int cantidadElementos);
	public List<Boletos> buscarBoletosPorUsuario(Usuarios usuario);
	public List<Boletos> buscarBoletosPorSorteo(Sorteos sorteo);
	public Optional<Boletos> buscarBoletosPorId(int id);
	public Boletos agregarNuevoBoleto(Boletos boleto);
	public Boletos actualizarBoleto(Boletos boleto);
	public void borrarBoleto(int id);
	

}
