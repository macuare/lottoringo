package com.tecnobackendev.lottoringo.servicios;

import java.util.Date;

public interface CalendarioService {
	public Date tiempoActual();
	public Date agregarHorasTiempoActual(Date actual, long horasAgregadas);
	public boolean verificarValidezTiempoExpiracion(Date expiracion);
	public String fechaTexto(Date actual);
	
}
