package com.tecnobackendev.lottoringo.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tecnobackendev.lottoringo.entidades.Roles;
import com.tecnobackendev.lottoringo.repositorios.RolesRepository;

@Service
public class RolesServiceImpl implements RolesService {
	
	@Autowired
	private RolesRepository rolesRepository;

	@Override
	public List<Roles> listarRoles() {
		// TODO Auto-generated method stub
		return rolesRepository.findAll();
	}

	@Override
	public Optional<Roles> buscarRolesPorId(int idRoles) {
		// TODO Auto-generated method stub
		return rolesRepository.findById(idRoles);
	}

}
