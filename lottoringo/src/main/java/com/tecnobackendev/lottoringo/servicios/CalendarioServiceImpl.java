package com.tecnobackendev.lottoringo.servicios;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.springframework.stereotype.Service;

@Service
public class CalendarioServiceImpl implements CalendarioService{
	
	@Override
	public Date tiempoActual() {
		LocalDateTime tiempo = LocalDateTime.now(ZoneId.systemDefault());
		Instant instant = tiempo.toInstant(ZoneOffset.UTC);
		Date date = Date.from(instant);
		
		return date;
	}
	
	@Override
	public Date agregarHorasTiempoActual(Date actual, long horasAgregadas) {
		
		LocalDateTime tiempo = actual.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		Instant instant = tiempo.plusHours(horasAgregadas).toInstant(ZoneOffset.UTC);
		Date date = Date.from(instant);
		
		return date;
	}
	
	@Override
	public boolean verificarValidezTiempoExpiracion(Date expiracion) {
		// TODO Auto-generated method stub
		//LocalDateTime fechaActual = LocalDateTime.now(ZoneId.of("UTC"));
		LocalDateTime fechaActual = LocalDateTime.now(ZoneId.systemDefault());
		LocalDateTime fechaExpiracion = expiracion.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		
//		System.out.println("actual:"+fechaActual);
//		System.out.println("expiracion:"+fechaExpiracion);
//		System.out.println("comparacion:"+fechaActual.isAfter(fechaExpiracion));
		
		return fechaActual.isAfter(fechaExpiracion);
	}
	
	@Override
	public String fechaTexto(Date actual) {
		
		LocalDateTime tiempo = 
				actual.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		
		return tiempo.format(DateTimeFormatter.ofPattern("MMMM dd, yyyy"));
	}
	
	
}
