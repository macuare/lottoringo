package com.tecnobackendev.lottoringo.servicios;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService{
	@Autowired
	private JavaMailSender emailSender;

	@Override
	public void enviarCorreoSimple(String destinatario, String tituloCorreo, String mensaje) {
		// TODO Auto-generated method stub
		SimpleMailMessage correo = new SimpleMailMessage();
		correo.setTo(destinatario);
		correo.setSubject(tituloCorreo);
		correo.setText(mensaje);
		
		emailSender.send(correo);
		
		
	}

	@Override
	public void enviarCorreoHTML(String destinatario, String tituloCorreo, String mensaje) {
		// TODO Auto-generated method stub
		MimeMessage mensajeria = emailSender.createMimeMessage();
		
		try {
			// Habilitando multipar y con encrustacion inline
			MimeMessageHelper aux = new MimeMessageHelper(mensajeria, true);
			aux.setTo(destinatario);
			aux.setSubject(tituloCorreo);

			aux.setText("<html>\n" +
					"<body>\n" +
					"\n" + mensaje +"\n" +
					"</body>\n" +
					"</html>", true);


			// Enviando el correo al destinatario
			emailSender.send(mensajeria);

		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
