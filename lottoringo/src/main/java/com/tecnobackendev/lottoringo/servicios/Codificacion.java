package com.tecnobackendev.lottoringo.servicios;

public interface Codificacion {
	
	public String codificar(String texto);
	
	public String codificarSHA1(String texto);
}
