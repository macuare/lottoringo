package com.tecnobackendev.lottoringo.servicios;

import java.util.List;
import java.util.Optional;

import com.tecnobackendev.lottoringo.entidades.Pines;

public interface PinesService {
	public List<Pines> listarPines();	
	public Optional<Pines> buscarPinesPorId(int idPin);
	public Optional<Pines> buscarPinesPorPin(int pin);
	public Pines agregarNuevoPin();
	public Pines actualizarPin(Pines pines);
	public void borrarPin(int idPin);
}
