package com.tecnobackendev.lottoringo.servicios;

public interface FireBaseService {
	public void inicializarServicio();
	public void enviarMensaje(String titulo, String mensaje);
	public void suscripcionTopico(String tokenFirebase);

}
