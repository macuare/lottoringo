package com.tecnobackendev.lottoringo.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.tecnobackendev.lottoringo.entidades.Usuarios;
import com.tecnobackendev.lottoringo.repositorios.UsuariosRepository;

@Service
public class UsuariosServiceImpl implements UsuariosService{
	

	@Autowired
	private UsuariosRepository usuariosRepository;

	@Override
	public List<Usuarios> listarLosUsuarios() {
		// TODO Auto-generated method stub
		return usuariosRepository.findAll();
	}

	@Override
	public Optional<Usuarios> buscarPorToken(String tokenUsuario) {
		// TODO Auto-generated method stub
		Usuarios usuarios = new Usuarios();
		usuarios.setIdUsuarios(tokenUsuario);
		
		Example<Usuarios> example = Example.of(usuarios);
		
		return usuariosRepository.findOne(example);
	}

	@Override
	public Optional<Usuarios> buscarPorUsuario(String nombreUsuario) {
		// TODO Auto-generated method stub
		System.out.println(nombreUsuario);
		
		Usuarios usuarios = new Usuarios();
		if(nombreUsuario.contains("@"))
			usuarios.setCorreo(nombreUsuario);
		else
			usuarios.setNombreUsuario(nombreUsuario);
		
		System.out.println(usuarios.toString());
		
		Example<Usuarios> example = Example.of(usuarios);
		
		return usuariosRepository.findOne(example);
	}

	
	@Override
	public Optional<Usuarios> buscarPorCorreo(String correoUsuario) {
		// TODO Auto-generated method stub
		Usuarios usuarios = new Usuarios();
		usuarios.setCorreo(correoUsuario);
		
		Example<Usuarios> example = Example.of(usuarios);
		
		return usuariosRepository.findOne(example);
	}

	@Override
	public Usuarios agregarNuevoUsuario(Usuarios usuario) {
		// TODO Auto-generated method stub
		return usuariosRepository.saveAndFlush(usuario);
	}

	@Override
	public Usuarios actualizarUsuario(Usuarios usuario) {
		// TODO Auto-generated method stub
		return usuariosRepository.saveAndFlush(usuario);
	}

	@Override
	public void borrarUsuario(String tokenUsuario) {
		// TODO Auto-generated method stub
		Usuarios usuarios = new Usuarios();
		usuarios.setIdUsuarios(tokenUsuario);
		
		usuariosRepository.delete(usuarios);
		
	}
	
	

}
