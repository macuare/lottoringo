package com.tecnobackendev.lottoringo.servicios;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ArchivosServiceImpl implements ArchivosService{
	@Autowired
	private Codificacion codificacion;
	@Autowired
	private ClasificacionMIME mime;
	
	@Value("${ruta.base}")
	private String rutaBase;
	
	
	
	 @Override
	    public Path rutaNormalizadaLocal(String nombreArchivo) throws MalformedURLException, URISyntaxException{
	        String rutaOriginal =  getClass().getResource("ArchivosServiceImpl.class").getFile();
	        //System.out.println("ruta ->"+rutaOriginal);
	        String rutaReal = rutaOriginal.substring(0, rutaOriginal.indexOf("STDXML.jar")).concat(nombreArchivo);
	        URL urlNueva = new URL(rutaReal);
	                
	       
	        return Paths.get(urlNueva.toURI());    
	    }
	    
	    @Override
	    public Path normalizarRuta(String rutaActual){
	        Path rutaModificada = null;
	        URL urlNueva = null;
	       
	        try {
	            urlNueva = Paths.get(rutaActual).toUri().toURL();
	            //System.out.println("rutaExterna1: "+urlNueva.getPath());
	            
	            rutaModificada = Paths.get(urlNueva.toURI());
	            //System.out.println("rutaExterna2: "+ruta.toString());
	            
	        } catch (MalformedURLException | URISyntaxException ex) { 
	            Logger.getLogger(ArchivosServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
	        }
	        
	        return rutaModificada;
	    }
	    
	    @Override
	    public Stream<String> leerAchivoLocal(String nombreArchivo) {
	        Stream<String> lineas = null;
	        Path ruta = null;
	          
	        try {
	            
	            ruta = this.rutaNormalizadaLocal(nombreArchivo);
	             System.out.println("path: "+ruta.toString());
	    
	            lineas = Files.lines(ruta, Charset.forName("UTF-8"));
	           
	            
	        } catch (IOException | URISyntaxException ex) {
	            Logger.getLogger(ArchivosServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
	        }finally{
	          // if(lineas != null) lineas.close();
	        }
	        
	        return lineas;
	    }
	    
	    @Override
	    public Stream<String> leerAchivoExterno(String rutaArchivo) {
	        Stream<String> lineas = null;
	        
	        try {
	            lineas = Files.lines(this.normalizarRuta(rutaArchivo), Charset.forName("UTF-8"));
	        } catch (IOException ex) {
	            Logger.getLogger(ArchivosServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
	        }
	        
	        return lineas;
	    }

	    @Override
	    public Stream<Path> listarDirectoriosyArchivos(String rutaBase) {
	        Stream<Path> directorios = null;
	        
	        try {
	            Path rutaNormalizada = this.normalizarRuta(rutaBase);
	            System.out.println("Privilegios: -> Lectura = "+Files.isReadable(rutaNormalizada));
	           
	            directorios = Files.walk(rutaNormalizada);
	            
	        } catch (IOException ex) {
	            Logger.getLogger(ArchivosServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
	        }
	        
	        return directorios;
	    }

	    @Override
	    public List<Path> listarDirectoriosyArchivosSupervisado(String rutaBase) {
//	      Supervisor supervisor = new Supervisor();
//	        
//	        try {
//	            Path rutaNormalizada = this.normalizarRuta(rutaBase);
//	            System.out.println("Privilegios: -> Lectura = "+Files.isReadable(rutaNormalizada));
//	           
//	            // Iniciando el analisis supervisado del directorio
//	            Files.walkFileTree(rutaNormalizada, supervisor);
//	            
//	        } catch (IOException ex) {
//	            Logger.getLogger(ArchivoImpl.class.getName()).log(Level.SEVERE, null, ex);
//	        }
//	        
//	        return Supervisor.contenido;
	    	return null;
	    }
	    
	    

	    @Override
	    public void copiarArchivo(Path archivoOriginal, Path rutaArchivo) {
	        
	        try {
	            System.out.println("copia: "+Files.copy(archivoOriginal, rutaArchivo, StandardCopyOption.REPLACE_EXISTING));
	                  
	        } catch (IOException ex) {
	            Logger.getLogger(ArchivosServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
	        }
	        
	    }
	    
	    @Override
	    public Path verificarDuplicidadDirectorio(Path ruta) {
	      Path salida = ruta;
	       // System.out.println("verificar: "+ruta);
	       // System.out.println("repetido: "+Files.exists(ruta));
	      if(Files.exists(ruta)){
	           LocalDateTime ldt = LocalDateTime.now();
	           String fechaHora = ldt.toString() // 2018-11-14T13:06:42.703
	                                 .replaceAll("-", "")
	                                 .replaceAll(":", "")
	                                 .replace("T", "-")
	                                 .replace(".", "");
	         salida = Paths.get(ruta.toString().concat("(").concat(fechaHora).concat(")"));
	      }
	       
	      return salida;
	    }

	    private Path verificarArchivoExistente(Path archivoActual){
	        Path salida = archivoActual;
	        
	        
	        if(Files.exists(archivoActual)){
	           String nombreArchivo = archivoActual.getFileName().toString();
	           // System.out.println("existe: "+archivoActual.getFileName()); 
	           
	           
	           if(nombreArchivo.contains("(")){ // se verifica si el existente ya fue numerado anteriormente para buscar la secuencia
	              String numero = nombreArchivo.substring(nombreArchivo.indexOf("(")+1, nombreArchivo.indexOf(")"));
	             //  System.out.println("numero: "+numero);
	              int indice = Integer.valueOf(numero);
	              
	              String nuevoNombre = nombreArchivo.replace("("+indice+")", "("+(indice+1)+")");
	              Path nuevaRuta = archivoActual.getParent().resolve( nuevoNombre );
	              
	            //   System.out.println("traia numeracion: nuevo: "+nuevaRuta);
	              
	               salida = this.verificarArchivoExistente(nuevaRuta);
	                      
	           }else{ // si no ha sido numerado se agrega un numero
	                 
	             salida = this.verificarArchivoExistente(
	                     archivoActual.getParent().resolve(archivoActual.getFileName().toString().replace(".", "(1)."))
	             );
	             
	            // System.out.println("agregandoNumeracion-> "+salida);
	           }        
	            
	         //  System.out.println("duplicadoCorregido:: "+salida);
	        }
	       
	        
	       return salida;
	    }
	    
	    @Override
	    public void crearNuevoArchivoSecuencial(Path archivoFuente, Path archivoDestino){
	        archivoDestino = this.verificarArchivoExistente(archivoDestino);
	        
	        try {
	            System.out.println("destino: "+archivoDestino);
	            Files.copy(archivoFuente, archivoDestino, StandardCopyOption.REPLACE_EXISTING);
	        } catch (IOException ex) {
	            Logger.getLogger(ArchivosServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
	        }
	        
	    }

	    @Override
	    public Path fusionRutas(Path directorioBase, Path directorioOrigen, Path directorioDestino) {
	       return Paths.get(
	               directorioDestino.toString(), 
	               directorioBase.subpath(
	                       directorioOrigen.getNameCount()-1, 
	                       directorioBase.getNameCount() 
	               ).toString()
	       );
	    }
	    
	    

	    @Override
	    public Path verificarDirectorioBaseDestino(Path directorioBase, Path directorioOrigen, Path directorioDestino) {
	       Path fusion = this.fusionRutas(directorioBase, directorioOrigen, directorioDestino); 
	       Path nuevoDirectorioBase = this.verificarDuplicidadDirectorio(fusion);
	       
	       if(fusion.compareTo(nuevoDirectorioBase) == 0 ) 
	           return null;
	       else
	           return nuevoDirectorioBase;          
	    }

	    @Override
	    public void escribirArchivo(Path rutaDestino, List<String> lineas) {
	       
	        try {
	             if(Files.exists(rutaDestino)){ // escribe al final
	                    Files.write(rutaDestino, lineas, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
	             }else{ // escribe todo el archivo
	                   Files.write(rutaDestino, lineas, Charset.forName("UTF-8"), StandardOpenOption.WRITE);
	             }
	             
	        } catch (IOException ex) {
	                Logger.getLogger(ArchivosServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
	        }
	        
	        
	    }
	
	    private String extraccionMIMEBase64(String texto) {
	    	//data:image/png
	    	return 
		    	mime.getMimeTipos()
		    		.entrySet()
		    		.stream()
		    		.filter(s -> s.getValue().equalsIgnoreCase(texto.split(":")[1]))
		    		.findFirst()
		    		.get()
		    		.getKey();
	    }
	    
	    @Override
		public String guardarArchivoImagen(String nombreArchivo, String base64) {
			// TODO Auto-generated method stub
	    	// data:image/png;base64,caracteres
	    	String partes[] = base64.split(";");
			byte[] data = DatatypeConverter.parseBase64Binary(partes[1].replace("base64", ""));
			
			Path rutaArchivo = Paths.get(rutaBase, nombreArchivo+"."+this.extraccionMIMEBase64(partes[0]));
		
			try {
				Files.write(rutaArchivo, data);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
			System.out.println(rutaArchivo.toString());
			return rutaArchivo.getFileName().toString();
		}
	    
	@Override
	public String convertirImagenBase64(String nombreArchivo) {
		// TODO Auto-generated method stub
		
		 String codificacionB64 = null;
		 byte[] contenido;
		 
		 // Cargando el elemento del disco duro local
		 Path rutaImagen = Paths.get(rutaBase, nombreArchivo);
		 
		 if(!Files.exists(rutaImagen)) {
			  nombreArchivo = "generica.png";
			  rutaImagen = Paths.get(rutaBase, nombreArchivo);
		 }
		 
		 String nombreImagen = nombreArchivo;
		 
		 try {
			contenido = Files.readAllBytes(rutaImagen);
			
			String codMime = mime.getMimeTipos()
		 			  .entrySet().stream()
		 			  .filter(s -> nombreImagen.endsWith(s.getKey()))
		 			  .findFirst()
		 			  .get()
		 			  .getValue();
			
			codificacionB64 = Base64.getEncoder().encodeToString(contenido);
			
			// Creando el protocolo de identificacion del archivo. tipo;base64,codificacion
			codificacionB64 = "data:"+codMime+";base64,"+codificacionB64;
			
		 } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 }
		 
		 //System.out.println(codificacionB64);
		 
		return codificacionB64;
	}
	

}
