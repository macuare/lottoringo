package com.tecnobackendev.lottoringo.servicios;

import java.util.Map;

public interface ClasificacionMIME {

	public Map<String, String> getMimeTipos();

	public void setMimeTipos(Map<String, String> mimeTipos);

}