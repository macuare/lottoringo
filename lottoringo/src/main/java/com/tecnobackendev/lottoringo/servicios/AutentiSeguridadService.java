package com.tecnobackendev.lottoringo.servicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.tecnobackendev.lottoringo.entidades.Autenticacion;
import com.tecnobackendev.lottoringo.entidades.Usuarios;

@Service
public class AutentiSeguridadService implements UserDetailsService {

	@Autowired
	private UsuariosService usuariosService;

	@Autowired
	private AutenticacionService autenticacionService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		System.out.println("Activando Proceso de Autenticacion");

		System.out.println("Modo Usuario Detectado: " + username);
		return this.verificandoUsuario(username);

	}

	private User verificandoUsuario(String nombreUsuario) throws UsernameNotFoundException {
		System.out.println("Analizando");
		
		// Verificando la existencia del usuario
		Optional<Usuarios> usuarioLogin;
		if(nombreUsuario.contains("@"))
			usuarioLogin = usuariosService.buscarPorCorreo(nombreUsuario);
		else
			usuarioLogin = usuariosService.buscarPorUsuario(nombreUsuario);
		
		// Buscando la contraseña
		if(usuarioLogin.isPresent()) { 
			System.out.println("Usuario: "+nombreUsuario+" Existe");
			
			// Cargando la clave y rol
			Optional<Autenticacion> autenticacion;
			autenticacion = autenticacionService.buscarAutenticacionPorUsuario(usuarioLogin.get().getNombreUsuario());
				
			if(autenticacion.isPresent()) {
				List<GrantedAuthority> nivelesDeAccesos = new ArrayList<>();
				nivelesDeAccesos.add(new SimpleGrantedAuthority("ROLE_"+autenticacion.get().getRoles().getTipo()));
				
				System.out.println("Rol: "+nivelesDeAccesos.toString());
				
				// Se envia la informacion de interes para autenticacion
				return new User(usuarioLogin.get().getNombreUsuario(), "{noop}"+autenticacion.get().getPassword(), nivelesDeAccesos);
				
			}else {
				throw new UsernameNotFoundException("Fallo de seguridad. Autenticacion no existe");
			}
			
		}else {
			throw new UsernameNotFoundException("Fallo de seguridad. Usuario no existe");
		}
		
	}

//	private User verificandoToken(String tokenSpring) throws UsernameNotFoundException{
//		Tokens tokenActual = tokensService.buscarPorTokenSpring(tokenSpring);
//		
//		if(tokenActual != null && tokenActual.getTokenSpring().equalsIgnoreCase(tokenSpring)) {
//			Contratacion contratacion = null; 
//			List<GrantedAuthority> nivelesDeAccesos = null;
//			try {
//				//analizar el caso cuanto la persona es contratada para varios salones
//				contratacion = contratacionService.listarLosContratos()					
//								.stream()
//								.filter(s -> s.getUsuarios().getTokenUsuario().equalsIgnoreCase(tokenActual.getUsuarios().getTokenUsuario()))
//								.findFirst()
//								.get();
//				//Agregando el nivel de acceso para este cliente
//				nivelesDeAccesos = new ArrayList<>(); //Modificar para cuando el mismo usuario tiene varios niveles de acceso para el mismo local
//				nivelesDeAccesos.add(new SimpleGrantedAuthority("ROLE_"+contratacion.getRoles().getTipo()));
//			}catch(NoSuchElementException e) {
//				System.out.println("No se puede AUTENTICAR "+tokenSpring);
//				throw new UsernameNotFoundException("Fallo de seguridad");
//			}
//			
//			//Se envia la informacion de interes para autenticacion
//			return new User(tokenActual.getTokenSpring(), tokenActual.getTokenSpring(), nivelesDeAccesos);
//			
//		}else {
//			System.out.println("El token no existe: "+tokenSpring);
//			throw new UsernameNotFoundException("Token: " + tokenSpring+" NO EXISTE...!!!");
//		}
//		
//	}

//	private void manejadorTokenCliente(String tokenUsuario) {
//		System.out.println("Creando token del usuario");
//		
//		
//		Tokens tokens = new Tokens();
//		Usuarios usuario = new Usuarios();
//		List<Tokens> elementos = null;
//		
//		String valor = encriptar.codificar(UUID.randomUUID().toString()+System.currentTimeMillis());
//		
//		
//		elementos = tokensService.listarTokens();
//		
//		if(elementos == null || elementos.isEmpty()) {
//			usuario.setTokenUsuario(tokenUsuario);
//			
//			tokens.setTokenFirebase(UUID.randomUUID().toString()); // Se establece un valor aleatorio que posteriormente se podra modificar
//			tokens.setTokenSpring(valor);
//			tokens.setUsuarios(usuario);
//			System.out.println("BD Sin Tokens. Creando el Primero");
//		}else {
//
//			for(Tokens tks : elementos) { //listando todos los tokens del sistema
//				if(tks.getUsuarios().getTokenUsuario().equalsIgnoreCase(tokenUsuario)) {
//					tokens = tks; // se recupera toda la plantilla y se modifica solo el elemento de interes
//					tokens.setTokenSpring(valor); //se modifica el token spring
//					
//					System.out.println("modificando Token existente");
//					break;
//				}
//			}
//			
//			//System.out.println("salida tokens: "+tokens.getTokenSpring());
//			
//			if(tokens.getTokenSpring() == null) { //significa que no encontro concidencia con el usuario actual
//				//Se crea un nuevo token para el usuario existente.
//				usuario.setTokenUsuario(tokenUsuario);
//				
//				tokens.setTokenFirebase(UUID.randomUUID().toString()); // Se establece un valor aleatorio que posteriormente se podra modificar
//				tokens.setTokenSpring(valor);
//				tokens.setUsuarios(usuario);
//				System.out.println("No existe Token de Usuario en el registro. Creando uno nuevo.");
//			}
//			
//		}
//		
//		tokensService.agregarNuevoToken(tokens);
//	}
}
