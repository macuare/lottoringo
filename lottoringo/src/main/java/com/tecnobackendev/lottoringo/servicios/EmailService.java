package com.tecnobackendev.lottoringo.servicios;

public interface EmailService {
	public void enviarCorreoSimple(String destinatario, String tituloCorreo, String mensaje);
	public void enviarCorreoHTML(String destinatario, String tituloCorreo, String mensaje);
}
