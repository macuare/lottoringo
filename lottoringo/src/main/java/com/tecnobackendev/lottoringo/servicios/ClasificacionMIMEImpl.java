package com.tecnobackendev.lottoringo.servicios;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class ClasificacionMIMEImpl implements ClasificacionMIME {

	private Map<String, String> mimeTipos = null;

		public ClasificacionMIMEImpl() {
			super();
			mimeTipos = new HashMap<>();
			  mimeTipos.put("aac", "audio/aac");
			  mimeTipos.put("ai", "application/postscript");
		      mimeTipos.put("aif", "audio/x-aiff");
		      mimeTipos.put("aifc", "audio/x-aiff");
		      mimeTipos.put("aiff", "audio/x-aiff");
		      mimeTipos.put("amr", "audio/AMR");
		      mimeTipos.put("asc", "text/plain");
		      mimeTipos.put("asf", "video/x.ms.asf");
		      mimeTipos.put("asx", "video/x.ms.asx");
		      mimeTipos.put("au", "audio/basic");
		      mimeTipos.put("avi", "video/x-msvideo");
		      mimeTipos.put("bcpio", "application/x-bcpio");
		      mimeTipos.put("bin", "application/octet-stream");
		      mimeTipos.put("cab", "application/x-cabinet");
		      mimeTipos.put("cdf", "application/x-netcdf");
		      mimeTipos.put("class", "application/java-vm");
		      mimeTipos.put("cpio", "application/x-cpio");
		      mimeTipos.put("cpt", "application/mac-compactpro");
		      mimeTipos.put("crt", "application/x-x509-ca-cert");
		      mimeTipos.put("csh", "application/x-csh");
		      mimeTipos.put("css", "text/css");
		      mimeTipos.put("csv", "text/comma-separated-values");
		      mimeTipos.put("dcr", "application/x-director");
		      mimeTipos.put("dir", "application/x-director");
		      mimeTipos.put("dll", "application/x-msdownload");
		      mimeTipos.put("dms", "application/octet-stream");
		      mimeTipos.put("doc", "application/msword");
		      mimeTipos.put("dtd", "application/xml-dtd");
		      mimeTipos.put("dvi", "application/x-dvi");
		      mimeTipos.put("dxr", "application/x-director");
		      mimeTipos.put("eps", "application/postscript");
		      mimeTipos.put("etx", "text/x-setext");
		      mimeTipos.put("exe", "application/octet-stream");
		      mimeTipos.put("ez", "application/andrew-inset");
		      mimeTipos.put("gif", "image/gif");
		      mimeTipos.put("gtar", "application/x-gtar");
		      mimeTipos.put("gz", "application/gzip");
		      mimeTipos.put("gzip", "application/gzip");
		      mimeTipos.put("hdf", "application/x-hdf");
		      mimeTipos.put("htc", "text/x-component");
		      mimeTipos.put("hqx", "application/mac-binhex40");
		      mimeTipos.put("html", "text/html");
		      mimeTipos.put("htm", "text/html");
		      mimeTipos.put("ice", "x-conference/x-cooltalk");
		      mimeTipos.put("ief", "image/ief");
		      mimeTipos.put("iges", "model/iges");
		      mimeTipos.put("igs", "model/iges");
		      mimeTipos.put("jar", "application/java-archive");
		      mimeTipos.put("java", "text/plain");
		      mimeTipos.put("jnlp", "application/x-java-jnlp-file");
		      mimeTipos.put("jpg", "image/jpeg");
		      mimeTipos.put("jpg", "image/jpeg");
		      mimeTipos.put("jpg", "image/jpeg");
		      mimeTipos.put("js", "application/x-javascript");
		      mimeTipos.put("jsp", "text/plain");
		      mimeTipos.put("kar", "audio/midi");
		      mimeTipos.put("latex", "application/x-latex");
		      mimeTipos.put("lha", "application/octet-stream");
		      mimeTipos.put("lzh", "application/octet-stream");
		      mimeTipos.put("man", "application/x-troff-man");
		      mimeTipos.put("mathml", "application/mathml+xml");
		      mimeTipos.put("me", "application/x-troff-me");
		      mimeTipos.put("mesh", "model/mesh");
		      mimeTipos.put("mid", "audio/midi");
		      mimeTipos.put("midi", "audio/midi");
		      mimeTipos.put("mif", "application/vnd.mif");
		      mimeTipos.put("mol", "chemical/x-mdl-molfile");
		      mimeTipos.put("movie", "video/x-sgi-movie");
		      mimeTipos.put("mov", "video/quicktime");
		      mimeTipos.put("mp2", "audio/mpeg");
		      //mimeTipos.put("mp3", "audio/mpeg");
		      mimeTipos.put("mp3", "audio/mp3");
		      mimeTipos.put("mp4", "video/mp4");
		      mimeTipos.put("mpeg", "video/mpeg");
		      mimeTipos.put("mpe", "video/mpeg");
		      mimeTipos.put("mpga", "audio/mpeg");
		      mimeTipos.put("mpg", "video/mpeg");
		      mimeTipos.put("ms", "application/x-troff-ms");
		      mimeTipos.put("msh", "model/mesh");
		      mimeTipos.put("msi", "application/octet-stream");
		      mimeTipos.put("nc", "application/x-netcdf");
		      mimeTipos.put("oda", "application/oda");
		      mimeTipos.put("ogg", "application/ogg");
		      mimeTipos.put("pbm", "image/x-portable-bitmap");
		      mimeTipos.put("pdb", "chemical/x-pdb");
		      mimeTipos.put("pdf", "application/pdf");
		      mimeTipos.put("pgm", "image/x-portable-graymap");
		      mimeTipos.put("pgn", "application/x-chess-pgn");
		      mimeTipos.put("png", "image/png");
		      mimeTipos.put("pnm", "image/x-portable-anymap");
		      mimeTipos.put("ppm", "image/x-portable-pixmap");
		      mimeTipos.put("ppt", "application/vnd.ms-powerpoint");
		      mimeTipos.put("ps", "application/postscript");
		      mimeTipos.put("qt", "video/quicktime");
		      mimeTipos.put("ra", "audio/x-pn-realaudio");
		      mimeTipos.put("ra", "audio/x-realaudio");
		      mimeTipos.put("ram", "audio/x-pn-realaudio");
		      mimeTipos.put("ras", "image/x-cmu-raster");
		      mimeTipos.put("rdf", "application/rdf+xml");
		      mimeTipos.put("rgb", "image/x-rgb");
		      mimeTipos.put("rm", "audio/x-pn-realaudio");
		      mimeTipos.put("roff", "application/x-troff");
		      mimeTipos.put("rpm", "application/x-rpm");
		      mimeTipos.put("rpm", "audio/x-pn-realaudio");
		      mimeTipos.put("rtf", "application/rtf");
		      mimeTipos.put("rtx", "text/richtext");
		      mimeTipos.put("ser", "application/java-serialized-object");
		      mimeTipos.put("sgml", "text/sgml");
		      mimeTipos.put("sgm", "text/sgml");
		      mimeTipos.put("sh", "application/x-sh");
		      mimeTipos.put("shar", "application/x-shar");
		      mimeTipos.put("silo", "model/mesh");
		      mimeTipos.put("sit", "application/x-stuffit");
		      mimeTipos.put("skd", "application/x-koan");
		      mimeTipos.put("skm", "application/x-koan");
		      mimeTipos.put("skp", "application/x-koan");
		      mimeTipos.put("skt", "application/x-koan");
		      mimeTipos.put("smi", "application/smil");
		      mimeTipos.put("smil", "application/smil");
		      mimeTipos.put("snd", "audio/basic");
		      mimeTipos.put("spl", "application/x-futuresplash");
		      mimeTipos.put("src", "application/x-wais-source");
		      mimeTipos.put("sv4cpio", "application/x-sv4cpio");
		      mimeTipos.put("sv4crc", "application/x-sv4crc");
		      mimeTipos.put("svg", "image/svg+xml");
		      mimeTipos.put("swf", "application/x-shockwave-flash");
		      mimeTipos.put("t", "application/x-troff");
		      mimeTipos.put("tar", "application/x-tar");
		      mimeTipos.put("tar.gz", "application/x-gtar");
		      mimeTipos.put("tcl", "application/x-tcl");
		      mimeTipos.put("tex", "application/x-tex");
		      mimeTipos.put("texi", "application/x-texinfo");
		      mimeTipos.put("texinfo", "application/x-texinfo");
		      mimeTipos.put("tgz", "application/x-gtar");
		      mimeTipos.put("tiff", "image/tiff");
		      mimeTipos.put("tif", "image/tiff");
		      mimeTipos.put("tr", "application/x-troff");
		      mimeTipos.put("tsv", "text/tab-separated-values");
		      mimeTipos.put("txt", "text/plain");
		      mimeTipos.put("ustar", "application/x-ustar");
		      mimeTipos.put("vcd", "application/x-cdlink");
		      mimeTipos.put("vrml", "model/vrml");
		      mimeTipos.put("vxml", "application/voicexml+xml");
		      mimeTipos.put("wav", "audio/x-wav");
		      mimeTipos.put("wbmp", "image/vnd.wap.wbmp");
		      mimeTipos.put("wmlc", "application/vnd.wap.wmlc");
		      mimeTipos.put("wmlsc", "application/vnd.wap.wmlscriptc");
		      mimeTipos.put("wmls", "text/vnd.wap.wmlscript");
		      mimeTipos.put("wml", "text/vnd.wap.wml");
		      mimeTipos.put("wrl", "model/vrml");
		      mimeTipos.put("wtls-ca-certificate", "application/vnd.wap.wtls-ca-certificate");
		      mimeTipos.put("xbm", "image/x-xbitmap");
		      mimeTipos.put("xht", "application/xhtml+xml");
		      mimeTipos.put("xhtml", "application/xhtml+xml");
		      mimeTipos.put("xls", "application/vnd.ms-excel");
		      mimeTipos.put("xml", "application/xml");
		      mimeTipos.put("xpm", "image/x-xpixmap");
		      mimeTipos.put("xpm", "image/x-xpixmap");
		      mimeTipos.put("xsl", "application/xml");
		      mimeTipos.put("xslt", "application/xslt+xml");
		      mimeTipos.put("xul", "application/vnd.mozilla.xul+xml");
		      mimeTipos.put("xwd", "image/x-xwindowdump");
		      mimeTipos.put("xyz", "chemical/x-xyz");
		      mimeTipos.put("z", "application/compress");
		      mimeTipos.put("zip", "application/zip");
			
		}

		/* (non-Javadoc)
		 * @see com.tecnobackendev.lottoringo.servicios.ClasificacionMIME#getMimeTipos()
		 */
		@Override
		public Map<String, String> getMimeTipos() {
			return mimeTipos;
		}

		/* (non-Javadoc)
		 * @see com.tecnobackendev.lottoringo.servicios.ClasificacionMIME#setMimeTipos(java.util.Map)
		 */
		@Override
		public void setMimeTipos(Map<String, String> mimeTipos) {
			this.mimeTipos = mimeTipos;
		}

	}
	
