package com.tecnobackendev.lottoringo.servicios;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tecnobackendev.lottoringo.entidades.Pines;
import com.tecnobackendev.lottoringo.repositorios.PinesRepository;

@Service
public class PinesServiceImpl implements PinesService {
	@Autowired
	private PinesRepository pinesRepository;
	@Autowired
	private CalendarioService calendarioService;
	
	@Override
	public List<Pines> listarPines() {
		// TODO Auto-generated method stub
		return pinesRepository.findAll();
	}

	@Override
	public Optional<Pines> buscarPinesPorId(int idPin) {
		// TODO Auto-generated method stub
		return pinesRepository.findById(idPin);
	}

	@Override
	public Optional<Pines> buscarPinesPorPin(int pin) {
		// TODO Auto-generated method stub
		return
			this.listarPines()
				.stream()
				.filter(entidad -> entidad.getPin() == pin)
				.findFirst();
	}

	@Override
	public Pines agregarNuevoPin() {
		// TODO Auto-generated method stub
		Date actual = calendarioService.tiempoActual();
		Date expiracion = calendarioService.agregarHorasTiempoActual(actual, 12);
		
		int min = 10000000;
	    int max = 99999999;
	    int valor = ThreadLocalRandom.current().nextInt(min, max + 1);
		
		Pines pines = new Pines();
		pines.setFechaCreacion(actual);
		pines.setFechaExpiracion(expiracion);
		pines.setPin(valor);
		
		return pinesRepository.save(pines);
	}

	@Override
	public Pines actualizarPin(Pines pines) {
		// TODO Auto-generated method stub
		return pinesRepository.save(pines);
	}

	@Override
	public void borrarPin(int idPin) {
		// TODO Auto-generated method stub
		pinesRepository.deleteById(idPin);

	}

}
