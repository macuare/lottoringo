package com.tecnobackendev.lottoringo.servicios;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.tecnobackendev.lottoringo.entidades.Boletos;
import com.tecnobackendev.lottoringo.entidades.Ganadores;
import com.tecnobackendev.lottoringo.repositorios.GanadoresRepository;

@Service
public class GanadoresServiceImpl implements GanadoresService{
	@Autowired
	private GanadoresRepository repositorio;

	@Override
	public List<Ganadores> listarGanadores() {
		// TODO Auto-generated method stub
		return repositorio.findAll();
	}
	
	@Override
	public List<Ganadores> listarGanadores(int indicePagina, int cantidadElementos){
		Pageable lote = PageRequest.of(indicePagina, cantidadElementos, Sort.by("idGanadores").descending());
		return repositorio.findAll(lote).getContent();
	}
	
	@Override
	public List<Ganadores> listarGanadores(List<Boletos> listaBoletos) {
		// TODO Auto-generated method stub
		//ExampleMatcher matcher = ExampleMatcher.matching().withIgnorePaths("idGanadores, idAciertos, acreditado, status");
		
		List<Integer> ids = 
			listaBoletos.stream()
						.filter(boleto -> {
							// Identificando de la lista de boletos ver si ya existen previamente en Ganadores.
							Optional<Ganadores> ganador = 
									Optional.ofNullable(boleto.getGanadores()); 
							
							return ganador.isPresent();
						}).map(entidad -> 
							   entidad.getGanadores().getIdGanadores()
							   )
						.collect(Collectors.toList());
		
		
		return repositorio.findAllById(ids);
	}

	@Override
	public Optional<Ganadores> buscarGanadorPorId(int id) {
		// TODO Auto-generated method stub
		return repositorio.findById(id);
	}

	@Override
	public Ganadores agregarNuevoGanador(Ganadores ganador) {
		// TODO Auto-generated method stub
		return repositorio.save(ganador);
	}

	@Override
	public Ganadores actualizarGanador(Ganadores ganador) {
		// TODO Auto-generated method stub
		return repositorio.save(ganador);
	}

	@Override
	public void borrarGanador(int idGanador) {
		// TODO Auto-generated method stub
		repositorio.deleteById(idGanador);
		
	}
	
	@Override
	public void borrarGanadores(List<Ganadores> entidades) {
		// TODO Auto-generated method stub
		repositorio.deleteInBatch(entidades);
		
	}
	
}
