package com.tecnobackendev.lottoringo.servicios;

import java.util.List;
import java.util.Optional;

import com.tecnobackendev.lottoringo.entidades.Usuarios;

public interface UsuariosService {
	public List<Usuarios> listarLosUsuarios();	
	public Optional<Usuarios> buscarPorToken(String tokenUsuario);
	public Optional<Usuarios> buscarPorUsuario(String nombreUsuario);
	public Optional<Usuarios> buscarPorCorreo(String correoUsuario);
	public Usuarios agregarNuevoUsuario(Usuarios usuario);
	public Usuarios actualizarUsuario(Usuarios usuario);
	public void borrarUsuario(String tokenUsuario);

}
