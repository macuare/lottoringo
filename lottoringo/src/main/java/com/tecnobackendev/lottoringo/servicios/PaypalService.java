package com.tecnobackendev.lottoringo.servicios;

import java.util.List;
import java.util.Optional;

import com.tecnobackendev.lottoringo.entidades.Paypal;

public interface PaypalService {
	public List<Paypal> listarPaypals();	
	public Optional<Paypal> buscarPaypalsPorCorreo(String correoElectronico);
	public Optional<Paypal> buscarPaypalPorUsuario(String tokenUsuario);
	public Paypal agregarNuevaCuentaPaypal(Paypal paypal);
	public Paypal actualizarPaypal(Paypal paypal);
	public void borrarPaypal(int idPaypal);

}
