package com.tecnobackendev.lottoringo.servicios;

import java.util.List;
import java.util.Optional;

import com.tecnobackendev.lottoringo.entidades.Boletos;
import com.tecnobackendev.lottoringo.entidades.Ganadores;


public interface GanadoresService {
	public List<Ganadores> listarGanadores();	
	public List<Ganadores> listarGanadores(int indicePagina, int cantidadElementos);
	public List<Ganadores> listarGanadores(List<Boletos> listaBoletos);
	public Optional<Ganadores> buscarGanadorPorId(int id);
	public Ganadores agregarNuevoGanador(Ganadores ganador);
	public Ganadores actualizarGanador(Ganadores ganador);
	public void borrarGanador(int idGanador);
	public void borrarGanadores(List<Ganadores> entidades);
}
