package com.tecnobackendev.lottoringo.servicios;

import java.util.List;
import java.util.Optional;

import com.tecnobackendev.lottoringo.entidades.Aciertos;

public interface AciertosService {
	public List<Aciertos> listarAciertos();	
	public Optional<Aciertos> buscarAciertoPorId(int id);
	public Aciertos agregarNuevoAcierto(Aciertos acierto);
	public Aciertos actualizarAcierto(Aciertos acierto);
	public void borrarAcierto(int id);
}
