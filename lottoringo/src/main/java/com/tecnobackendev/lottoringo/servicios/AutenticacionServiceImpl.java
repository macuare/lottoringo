package com.tecnobackendev.lottoringo.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tecnobackendev.lottoringo.entidades.Autenticacion;
import com.tecnobackendev.lottoringo.repositorios.AutenticacionRepository;

@Service
public class AutenticacionServiceImpl implements AutenticacionService {
	
	@Autowired
	private AutenticacionRepository autenticacionRepository;

	@Override
	public List<Autenticacion> listarAutenticacion() {
		// TODO Auto-generated method stub
		return autenticacionRepository.findAll();
	}

	@Override
	public Optional<Autenticacion> buscarAutenticacionPorId(int id) {
		// TODO Auto-generated method stub
		return autenticacionRepository.findById(id);
	}
	@Override
	public Optional<Autenticacion> buscarAutenticacionPorToken(String tokenUsuario) {
		// TODO Auto-generated method stub
		List<Autenticacion> listado = this.listarAutenticacion();
		return listado.stream()
				.filter(elemento -> elemento.getUsuarios().getIdUsuarios().equalsIgnoreCase(tokenUsuario))
				.findFirst();
		
	}

	@Override
	public Optional<Autenticacion> buscarAutenticacionPorUsuario(String nombreUsuario) {
		// TODO Auto-generated method stub
		List<Autenticacion> listado = this.listarAutenticacion();
		return listado.stream()
				.filter(elemento -> elemento.getUsuarios().getNombreUsuario().equalsIgnoreCase(nombreUsuario))
				.findFirst();
	}
	
	

	@Override
	public Optional<Autenticacion> buscarAutenticacionPorCorreo(String correoUsuario) {
		// TODO Auto-generated method stub
		List<Autenticacion> listado = this.listarAutenticacion();
		return listado.stream()
				.filter(elemento -> elemento.getUsuarios().getCorreo().equalsIgnoreCase(correoUsuario))
				.findFirst();
	}

	@Override
	public Autenticacion agregarNuevaAutenticacion(Autenticacion autenticacion) {
		// TODO Auto-generated method stub
		return autenticacionRepository.save(autenticacion);
	}

	@Override
	public Autenticacion actualizarAutenticacion(Autenticacion autenticacion) {
		// TODO Auto-generated method stub
		return autenticacionRepository.saveAndFlush(autenticacion);
	}

	@Override
	public void borrarAutenticacion(int idAutenticacion) {
		// TODO Auto-generated method stub
		autenticacionRepository.deleteById(idAutenticacion);
		
	}

}
