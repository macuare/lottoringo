package com.tecnobackendev.lottoringo.servicios;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import com.tecnobackendev.lottoringo.entidades.Seguridad;
import com.tecnobackendev.lottoringo.repositorios.SeguridadRepository;

@Service
public class SeguridadServiceImpl implements SeguridadService{

	@Autowired
	private SeguridadRepository seguridadRepository;
	@Autowired
	private Codificacion codificacion;
	@Autowired
	private CalendarioService calendarioService;
	
	@Override
	public List<Seguridad> listarSeguridad() {
		// TODO Auto-generated method stub
		return seguridadRepository.findAll();
	}

	@Override
	public Optional<Seguridad> buscarPorToken(String token) {
		// TODO Auto-generated method stub
		Seguridad seguridad = new Seguridad();
		seguridad.setToken(token);
		
		ExampleMatcher exampleMatcher = 
				ExampleMatcher.matching()
							  .withIgnorePaths("idSeguridad");
		
		Example<Seguridad> example = Example.of(seguridad, exampleMatcher);
		
		return seguridadRepository.findOne(example);
	}

	@Override
	public Optional<Seguridad> buscarPorFechaCreacion(Date fechaCreacion) {
		// TODO Auto-generated method stub
		Seguridad seguridad = new Seguridad();
		seguridad.setFechaCreacion(fechaCreacion);
		
		Example<Seguridad> example = Example.of(seguridad);
		
		return seguridadRepository.findOne(example);
	}

	@Override
	public Optional<Seguridad> buscarPorFechaExpiracion(Date fechaExpiracion) {
		// TODO Auto-generated method stub
		Seguridad seguridad = new Seguridad();
		seguridad.setFechaCreacion(fechaExpiracion);
		
		Example<Seguridad> example = Example.of(seguridad);
		
		return seguridadRepository.findOne(example);
	}

	
	
	@Override
	public Seguridad agregarNuevaSeguridad() {
		// TODO Auto-generated method stub
		long referencia = System.currentTimeMillis();
		Random aleatorio = new Random(referencia);
		int valor = aleatorio.nextInt(999999);
		
		Date fechaExpiracion = calendarioService.agregarHorasTiempoActual(new Date(referencia), 12);
		
		Seguridad seguridad = new Seguridad();
		seguridad.setToken(
				codificacion.codificar("lotto"+valor)
				);
		seguridad.setFechaExpiracion(fechaExpiracion);
		
		System.out.println(seguridad.toString());
		
		return seguridadRepository.saveAndFlush(seguridad);
	}

	@Override
	public Seguridad actualizarSeguridad(Seguridad seguridad) {
		// TODO Auto-generated method stub
		return seguridadRepository.saveAndFlush(seguridad);
	}

	@Override
	public void borrarSeguridad(String token) {
		// TODO Auto-generated method stub
		this.buscarPorToken(token).ifPresent(seguridad ->{
			seguridadRepository.delete(seguridad);
		});
		
		
	}

}
