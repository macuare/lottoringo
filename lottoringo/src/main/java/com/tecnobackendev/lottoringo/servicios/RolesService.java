package com.tecnobackendev.lottoringo.servicios;

import java.util.List;
import java.util.Optional;

import com.tecnobackendev.lottoringo.entidades.Roles;

public interface RolesService {
	public List<Roles> listarRoles();	
	public Optional<Roles> buscarRolesPorId(int idRoles);
}
