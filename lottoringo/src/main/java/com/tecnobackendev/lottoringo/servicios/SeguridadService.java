package com.tecnobackendev.lottoringo.servicios;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.tecnobackendev.lottoringo.entidades.Seguridad;

public interface SeguridadService {

	public List<Seguridad> listarSeguridad();	
	public Optional<Seguridad> buscarPorToken(String token);
	public Optional<Seguridad> buscarPorFechaCreacion(Date fechaCreacion);
	public Optional<Seguridad> buscarPorFechaExpiracion(Date fechaExpiracion);
	public Seguridad agregarNuevaSeguridad();
	public Seguridad actualizarSeguridad(Seguridad seguridad);
	public void borrarSeguridad(String token);
}
