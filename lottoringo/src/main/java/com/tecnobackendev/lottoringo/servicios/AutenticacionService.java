package com.tecnobackendev.lottoringo.servicios;

import java.util.List;
import java.util.Optional;

import com.tecnobackendev.lottoringo.entidades.Autenticacion;


public interface AutenticacionService {
	public List<Autenticacion> listarAutenticacion();	
	public Optional<Autenticacion> buscarAutenticacionPorId(int id);
	public Optional<Autenticacion> buscarAutenticacionPorToken(String tokenUsuario);
	public Optional<Autenticacion> buscarAutenticacionPorUsuario(String nombreUsuario);
	public Optional<Autenticacion> buscarAutenticacionPorCorreo(String correoUsuario);
	public Autenticacion agregarNuevaAutenticacion(Autenticacion autenticacion);
	public Autenticacion actualizarAutenticacion(Autenticacion autenticacion);
	public void borrarAutenticacion(int idAutenticacion);

}
