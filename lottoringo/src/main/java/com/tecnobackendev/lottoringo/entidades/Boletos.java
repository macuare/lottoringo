package com.tecnobackendev.lottoringo.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Boletos {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idBoletos;
	
	private int bola1;
	private int bola2;
	private int bola3;
	private int bola4;
	private int bola5;
	
	@Column(name = "bola_suerte")
	private int bolaSuerte;
	
	@Column(name = "fecha_creacion", updatable = false)
	@CreationTimestamp
	private Date fechaCreacion;
	
	@ManyToOne(fetch = FetchType.EAGER) 
	@JoinColumn(name = "idUsuarios")
	private Usuarios usuarios;
	
	@ManyToOne(fetch = FetchType.EAGER) 
	@JoinColumn(name = "idSorteos")
	private Sorteos sorteos;

	@OneToOne(mappedBy = "boletos", fetch = FetchType.EAGER)
	@JsonBackReference(value = "ganadores-boletos")
	private Ganadores ganadores;
	
	
	
	public Ganadores getGanadores() {
		return ganadores;
	}

	public void setGanadores(Ganadores ganadores) {
		this.ganadores = ganadores;
	}

	public int getIdBoletos() {
		return idBoletos;
	}

	public void setIdBoletos(int idBoletos) {
		this.idBoletos = idBoletos;
	}

	public int getBola1() {
		return bola1;
	}

	public void setBola1(int bola1) {
		this.bola1 = bola1;
	}

	public int getBola2() {
		return bola2;
	}

	public void setBola2(int bola2) {
		this.bola2 = bola2;
	}

	public int getBola3() {
		return bola3;
	}

	public void setBola3(int bola3) {
		this.bola3 = bola3;
	}

	public int getBola4() {
		return bola4;
	}

	public void setBola4(int bola4) {
		this.bola4 = bola4;
	}

	public int getBola5() {
		return bola5;
	}

	public void setBola5(int bola5) {
		this.bola5 = bola5;
	}
	
	public int getBolaSuerte() {
		return bolaSuerte;
	}

	public void setBolaSuerte(int bolaSuerte) {
		this.bolaSuerte = bolaSuerte;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Usuarios getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(Usuarios usuarios) {
		this.usuarios = usuarios;
	}

	public Sorteos getSorteos() {
		return sorteos;
	}

	public void setSorteos(Sorteos sorteos) {
		this.sorteos = sorteos;
	}

	@Override
	public String toString() {
		return "Boletos [idBoletos=" + idBoletos + ", bola1=" + bola1 + ", bola2=" + bola2 + ", bola3=" + bola3
				+ ", bola4=" + bola4 + ", bola5=" + bola5 + ", bolaSuerte=" + bolaSuerte + ", fechaCreacion="
				+ fechaCreacion + ", usuarios=" + usuarios.getIdUsuarios() + ", sorteos=" + sorteos.getIdSorteos() + "]";
	}
	
	
	
	
	
	
}
