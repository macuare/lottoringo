package com.tecnobackendev.lottoringo.entidades;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Pines {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idPines;
	private int pin;
	
	@Column(name = "fecha_creacion")
	@CreationTimestamp 
	private Date fechaCreacion;
	
	@Column(name = "fecha_expiracion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaExpiracion;

	public int getIdPines() {
		return idPines;
	}

	public int getPin() {
		return pin;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public Date getFechaExpiracion() {
		return fechaExpiracion;
	}

	public void setIdPines(int idPines) {
		this.idPines = idPines;
	}

	public void setPin(int pin) {
		this.pin = pin;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public void setFechaExpiracion(Date fechaExpiracion) {
		this.fechaExpiracion = fechaExpiracion;
	}

	@Override
	public String toString() {
		return "Pines [idPines=" + idPines + ", pin=" + pin + ", fechaCreacion=" + LocalDateTime.ofInstant(fechaCreacion.toInstant(), ZoneId.systemDefault()) + ", fechaExpiracion="
				+ fechaExpiracion + "]";
	}
	
	
	
}
