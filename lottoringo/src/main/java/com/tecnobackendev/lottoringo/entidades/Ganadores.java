package com.tecnobackendev.lottoringo.entidades;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.Digits;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Ganadores {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idGanadores;
	
	@ManyToOne(fetch = FetchType.EAGER) 
	@JoinColumn(name = "idAciertos")
	private Aciertos aciertos;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idBoletos")
	private Boletos boletos;

	@Digits(integer=3, fraction=3)
	private BigDecimal acreditado;
	
	private boolean status;
	
	public int getIdGanadores() {
		return idGanadores;
	}

	public void setIdGanadores(int idGanadores) {
		this.idGanadores = idGanadores;
	}

	public Aciertos getAciertos() {
		return aciertos;
	}

	public void setAciertos(Aciertos aciertos) {
		this.aciertos = aciertos;
	}

	public Boletos getBoletos() {
		return boletos;
	}

	public void setBoletos(Boletos boletos) {
		this.boletos = boletos;
	}
	
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public BigDecimal getAcreditado() {
		return acreditado;
	}

	public void setAcreditado(BigDecimal acreditado) {
		this.acreditado = acreditado;
	}

	@Override
	public String toString() {
		return "Ganadores [idGanadores=" + idGanadores + ", aciertos=" + aciertos + ", boletos=" + boletos
				+ ", acreditado=" + acreditado + ", status=" + status + "]";
	}

	
	
	
	
}
