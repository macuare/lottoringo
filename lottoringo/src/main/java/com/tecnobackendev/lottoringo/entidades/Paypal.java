package com.tecnobackendev.lottoringo.entidades;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Paypal {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idPaypal;
	
	@OneToOne(fetch = FetchType.EAGER) 
	@JoinColumn(name = "idUsuarios")
	private Usuarios usuarios;
	private String correo;
	
	public int getIdPaypal() {
		return idPaypal;
	}
	public void setIdPaypal(int idPaypal) {
		this.idPaypal = idPaypal;
	}
	
	public Usuarios getUsuarios() {
		return usuarios;
	}
	public void setUsuarios(Usuarios usuarios) {
		this.usuarios = usuarios;
	}
	
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	@Override
	public String toString() {
		return "Paypal [idPaypal=" + idPaypal + ", idUsuarios=" + usuarios.getIdUsuarios() + ", correo=" + correo + "]";
	}
	
	
	

}
