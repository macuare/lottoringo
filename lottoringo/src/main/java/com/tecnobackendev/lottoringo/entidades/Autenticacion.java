package com.tecnobackendev.lottoringo.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Autenticacion {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idAutenticacion; 
	
	private String password;
	
	@Column(name = "fecha_creacion", updatable = false)
	@CreationTimestamp //@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacion;
	
	@Column(name = "fecha_modificacion")
	@UpdateTimestamp //@Temporal(TemporalType.TIMESTAMP)
	private Date fechaModificacion;

	@OneToOne(fetch = FetchType.EAGER) 
	@JoinColumn(name = "idUsuarios")
	private Usuarios usuarios; 
	
	@ManyToOne(fetch = FetchType.EAGER) 
	@JoinColumn(name = "idRoles")
	private Roles roles;
	
	public int getIdAutenticacion() {
		return idAutenticacion;
	}

	public void setIdAutenticacion(int idAutenticacion) {
		this.idAutenticacion = idAutenticacion;
	}


	public Usuarios getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(Usuarios usuarios) {
		this.usuarios = usuarios;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	
	public Roles getRoles() {
		return roles;
	}

	public void setRoles(Roles roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "Autenticacion [idAutenticacion=" + idAutenticacion + ", password=" + password + ", fechaCreacion="
				+ fechaCreacion + ", fechaModificacion=" + fechaModificacion + ", usuarios=" + usuarios.getIdUsuarios() + ", roles="
				+ roles.getIdRoles() + "]";
	}

	
	
	

}
