package com.tecnobackendev.lottoringo.entidades;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Usuarios {
	@Id
	private String idUsuarios;
	@Column(name = "nombre_usuario")
	private String nombreUsuario;
	
	private String correo;
	
	private String telefono;
	
	private String imagen;
	
	@Column(name = "fecha_creacion", updatable = false)
	@CreationTimestamp //@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacion;
	
	
	@Column(name = "fecha_nacimiento")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaNacimiento;

	
	@OneToOne(mappedBy = "usuarios", fetch = FetchType.EAGER)
	@JsonBackReference(value = "paypal-usuarios")
	private Paypal paypal;
	
	@OneToOne(mappedBy = "usuarios", fetch = FetchType.EAGER)
	@JsonBackReference(value = "autenticacion-usuarios")
	private Autenticacion autenticacion;
	
	@OneToMany(mappedBy = "usuarios", fetch = FetchType.EAGER)
	@JsonBackReference(value = "boletos-usuario")
	private Set<Boletos> boletos;
	
	
	public Set<Boletos> getBoletos() {
		return boletos;
	}

	public void setBoletos(Set<Boletos> boletos) {
		this.boletos = boletos;
	}

	public String getIdUsuarios() {
		return idUsuarios;
	}

	public void setIdUsuarios(String idUsuarios) {
		this.idUsuarios = idUsuarios;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Paypal getPaypal() {
		return paypal;
	}

	public void setPaypal(Paypal paypal) {
		this.paypal = paypal;
	}

	public Autenticacion getAutenticacion() {
		return autenticacion;
	}

	public void setAutenticacion(Autenticacion autenticacion) {
		this.autenticacion = autenticacion;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	@Override
	public String toString() {
		return "Usuarios [idUsuarios=" + idUsuarios + ", nombreUsuario=" + nombreUsuario + ", correo=" + correo + ", telefono=" + telefono
				+ ", imagen=" + imagen + ", fechaCreacion=" + fechaCreacion + ", fechaNacimiento=" + fechaNacimiento
				+ "]";
	}

	
	
	

}
