package com.tecnobackendev.lottoringo.entidades;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Roles {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idRoles;
	private String tipo;
	
	@OneToMany(mappedBy = "roles", fetch = FetchType.EAGER)	
	@JsonBackReference(value = "autenticacion-roles")
	private Set<Autenticacion> autenticacion;
	
	public int getIdRoles() {
		return idRoles;
	}
	public void setIdRoles(int idRoles) {
		this.idRoles = idRoles;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	@Override
	public String toString() {
		return "Roles [idRoles=" + idRoles + ", tipo=" + tipo + "]";
	}
	public Set<Autenticacion> getAutenticacion() {
		return autenticacion;
	}
	public void setAutenticacion(Set<Autenticacion> autenticacion) {
		this.autenticacion = autenticacion;
	}
	
	
	
}
