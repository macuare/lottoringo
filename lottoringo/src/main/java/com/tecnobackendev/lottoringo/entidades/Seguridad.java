package com.tecnobackendev.lottoringo.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Seguridad {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idSeguridad;
	private String token; 
	
	@Column(name = "fecha_creacion", updatable = false)
	@CreationTimestamp
	private Date fechaCreacion;
	
	@Column(name = "fecha_expiracion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaExpiracion;
	
	public int getIdSeguridad() {
		return idSeguridad;
	}
	public void setIdSeguridad(int idSeguridad) {
		this.idSeguridad = idSeguridad;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Date getFechaExpiracion() {
		return fechaExpiracion;
	}
	public void setFechaExpiracion(Date fechaExpiracion) {
		this.fechaExpiracion = fechaExpiracion;
	}
	@Override
	public String toString() {
		return "Seguridad [idSeguridad=" + idSeguridad + ", token=" + token + ", fechaCreacion=" + fechaCreacion
				+ ", fechaExpiracion=" + fechaExpiracion + "]";
	}
	
	
	


}
