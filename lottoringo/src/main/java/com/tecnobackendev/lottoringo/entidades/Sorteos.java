package com.tecnobackendev.lottoringo.entidades;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Sorteos {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idSorteos;
	
	private int bola1;
	private int bola2;
	private int bola3;
	private int bola4;
	private int bola5;
	
	@Column(name = "bola_suerte")
	private int bolaSuerte;
	
	@Column(name = "fecha_creacion", updatable = false)
	@CreationTimestamp
	private Date fechaCreacion;
	
	@Column(name = "fecha_modificacion")
	@UpdateTimestamp
	private Date fechaModificacion;
	
	@Column(name = "fecha_sorteo")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaSorteo;
	
	@OneToMany(mappedBy = "sorteos", fetch = FetchType.EAGER)
	@JsonBackReference(value = "boletos-sorteos")
	private Set<Boletos> boletos;
	

	
	public Set<Boletos> getBoletos() {
		return boletos;
	}

	public void setBoletos(Set<Boletos> boletos) {
		this.boletos = boletos;
	}

	public int getIdSorteos() {
		return idSorteos;
	}

	public void setIdSorteos(int idSorteos) {
		this.idSorteos = idSorteos;
	}

	public int getBola1() {
		return bola1;
	}

	public void setBola1(int bola1) {
		this.bola1 = bola1;
	}

	public int getBola2() {
		return bola2;
	}

	public void setBola2(int bola2) {
		this.bola2 = bola2;
	}

	public int getBola3() {
		return bola3;
	}

	public void setBola3(int bola3) {
		this.bola3 = bola3;
	}

	public int getBola4() {
		return bola4;
	}

	public void setBola4(int bola4) {
		this.bola4 = bola4;
	}

	public int getBola5() {
		return bola5;
	}

	public void setBola5(int bola5) {
		this.bola5 = bola5;
	}

	public int getBolaSuerte() {
		return bolaSuerte;
	}

	public void setBolaSuerte(int bolaSuerte) {
		this.bolaSuerte = bolaSuerte;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Date getFechaSorteo() {
		return fechaSorteo;
	}

	public void setFechaSorteo(Date fechaSorteo) {
		this.fechaSorteo = fechaSorteo;
	}

	@Override
	public String toString() {
		return "Sorteos [idSorteos=" + idSorteos + ", bola1=" + bola1 + ", bola2=" + bola2 + ", bola3=" + bola3
				+ ", bola4=" + bola4 + ", bola5=" + bola5 + ", bolaSuerte=" + bolaSuerte + ", fechaCreacion="
				+ fechaCreacion + ", fechaModificacion=" + fechaModificacion + ", fechaSorteo=" + fechaSorteo + "]";
	}

	
	
	
}
