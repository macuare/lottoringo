package com.tecnobackendev.lottoringo.entidades;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Digits;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Aciertos {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idAciertos;
	private int bolas;
	
	@Column(name = "bola_suerte")
	private int bolaSuerte;
	
	@Digits(integer=4, fraction=2)
	@Column(name = "monto_usd")
	private BigDecimal montoUSD;
	
	@Column(name = "fecha_creacion", updatable = false)
	@CreationTimestamp
	private Date fechaCreacion;
	
	@Column(name = "fecha_modificacion")
	@UpdateTimestamp
	private Date fechaModificacion;

	@OneToMany(mappedBy = "aciertos", fetch = FetchType.EAGER)
	@JsonBackReference(value = "ganadores-aciertos")
	private Set<Ganadores> ganadores;
	
	
	
	public Set<Ganadores> getGanadores() {
		return ganadores;
	}

	public void setGanadores(Set<Ganadores> ganadores) {
		this.ganadores = ganadores;
	}

	public int getIdAciertos() {
		return idAciertos;
	}

	public void setIdAciertos(int idAciertos) {
		this.idAciertos = idAciertos;
	}

	public int getBolas() {
		return bolas;
	}

	public void setBolas(int bolas) {
		this.bolas = bolas;
	}

	public int getBolaSuerte() {
		return bolaSuerte;
	}

	public void setBolaSuerte(int bolaSuerte) {
		this.bolaSuerte = bolaSuerte;
	}

	public BigDecimal getMontoUSD() {
		return montoUSD;
	}

	public void setMontoUSD(BigDecimal montoUSD) {
		this.montoUSD = montoUSD;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Override
	public String toString() {
		return "Aciertos [idAciertos=" + idAciertos + ", bolas=" + bolas + ", bolaSuerte=" + bolaSuerte + ", montoUSD="
				+ montoUSD + ", fechaCreacion=" + fechaCreacion + ", fechaModificacion=" + fechaModificacion + "]";
	}
	
	
	

}
