package com.tecnobackendev.lottoringo.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tecnobackendev.lottoringo.entidades.Autenticacion;

@Repository
public interface AutenticacionRepository extends JpaRepository<Autenticacion, Integer>{

}
