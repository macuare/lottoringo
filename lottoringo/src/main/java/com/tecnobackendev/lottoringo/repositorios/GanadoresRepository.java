package com.tecnobackendev.lottoringo.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tecnobackendev.lottoringo.entidades.Ganadores;

@Repository
public interface GanadoresRepository extends JpaRepository<Ganadores, Integer>{

}
