package com.tecnobackendev.lottoringo.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tecnobackendev.lottoringo.entidades.Sorteos;

@Repository
public interface SorteosRepository extends JpaRepository<Sorteos, Integer>{

}
