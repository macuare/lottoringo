package com.tecnobackendev.lottoringo.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tecnobackendev.lottoringo.entidades.Boletos;

@Repository
public interface BoletosRepository extends JpaRepository<Boletos, Integer>{

}
