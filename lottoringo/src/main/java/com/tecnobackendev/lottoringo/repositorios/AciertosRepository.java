package com.tecnobackendev.lottoringo.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tecnobackendev.lottoringo.entidades.Aciertos;

@Repository
public interface AciertosRepository extends JpaRepository<Aciertos, Integer> {

}
