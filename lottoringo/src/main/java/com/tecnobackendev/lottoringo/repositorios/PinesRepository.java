package com.tecnobackendev.lottoringo.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tecnobackendev.lottoringo.entidades.Pines;

@Repository
public interface PinesRepository extends JpaRepository<Pines, Integer>{

}
