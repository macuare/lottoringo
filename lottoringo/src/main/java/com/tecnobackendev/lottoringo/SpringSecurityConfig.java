package com.tecnobackendev.lottoringo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.access.AccessDeniedHandler;

import com.tecnobackendev.lottoringo.servicios.AutentiSeguridadService;

@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter{
	@Autowired
	private AutentiSeguridadService autentiSeguridadService;
	
	
	@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//		auth
//        .inMemoryAuthentication()
//        .withUser("andyaacm@gmail.com")
//        .password("{noop}6fa97762927c7f7237c3f4b147ea934e2c0c9329d5e675144af5dbd11a2bb3a553e3edf6e7a056057d83df83a1be54bb6c67b38cbfc406834c4a9a17f0badbfd")
//        .roles("Jefe");
//		
		 auth.userDetailsService(autentiSeguridadService);		 
    }
	
	
	@Configuration
	@Order(1)
	public static class RestfulSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
		@Autowired
	    private AccessDeniedHandler accessDeniedHandler;

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			// TODO Auto-generated method stub
			
			System.out.println("---------<Configuracion RESTful>---------");
			
			
			 http.requiresChannel().antMatchers("/api/**").requiresSecure(); // solicitando SSL RESTful
			 
			// Esto es porque para estas pantallas las URL's no necesitan seguridad
			http.antMatcher("/api/**") // Necesario para interceptar la url en muktiples HttpSecurity
				.authorizeRequests()
				.antMatchers(HttpMethod.POST,"/api/servicios/autenticacion").permitAll()
				.antMatchers(HttpMethod.GET, "/api/servicios/autenticacion/verificar/usuario/**").permitAll()
				.antMatchers(HttpMethod.POST,"/api/servicios/autenticacion/actualizar/clave").permitAll()
				
				.antMatchers(HttpMethod.POST,"/api/servicios/pines/nuevo").permitAll()
				.antMatchers(HttpMethod.GET, "/api/servicios/pines/buscar/**").permitAll()
				
				.antMatchers(HttpMethod.POST,"/api/servicios/usuario/nuevo").permitAll()
				.antMatchers(HttpMethod.GET,"/api/servicios/usuario/informacion/basica/**").permitAll()
				
				.antMatchers(HttpMethod.GET,"/api/servicios/ganadores/recientes/**").permitAll()
				
				.antMatchers(HttpMethod.GET,"/api/servicios/paypal/usuario/**").permitAll()
				.antMatchers(HttpMethod.PUT,"/api/servicios/paypal/actualizar").permitAll()
				
				.antMatchers(HttpMethod.PUT,"/api/servicios/usuario/foto").permitAll()
				
				.antMatchers(HttpMethod.GET,"/api/servicios/sorteos/**").permitAll()
				
				.antMatchers(HttpMethod.GET,"/api/servicios/boletos/**").permitAll()
				.antMatchers(HttpMethod.POST,"/api/servicios/boletos/nuevo").permitAll()
				
				.antMatchers(HttpMethod.GET,"/api/servicios/loginout/**").permitAll()
				
				.antMatchers(HttpMethod.POST,"/api/servicios/firebase/suscripcion").permitAll()
				
				.antMatchers(HttpMethod.GET,"/api/servicios/jackpot/**").permitAll()
				
				
				.antMatchers(HttpMethod.GET, "/api/servicios/prueba").permitAll()
				.antMatchers(HttpMethod.GET, "/api/servicios/prueba2").permitAll()
				.antMatchers(HttpMethod.POST,"/api/servicios/admin").hasAuthority("ROLE_Administrador")
				.anyRequest().hasAnyRole("Administrador","Jefe")
				.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)//No se necesita que la sesion sea creada
				.and().exceptionHandling().accessDeniedHandler(accessDeniedHandler)
				.and().httpBasic()
				.and().csrf().disable()
				.formLogin().disable();
				
				
		}
		
	}
	
	@Configuration
	@Order(2)
	public static class WebLoginSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
		@Autowired
	    private AccessDeniedHandler accessDeniedHandler;
		
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			// TODO Auto-generated method stub
			System.out.println("--------------<Seguridad WEB>---------------");
			// Redireccionando a conexion segura SSL
			http.requiresChannel().anyRequest().requiresSecure();
			
			http
			 	.authorizeRequests()
				 	 .antMatchers("/","/403","/recovery","/new-user").permitAll()
				 	 .antMatchers("/css/**","/fonts/**","/img/**","/js/**","/scss/**","/vendors/**").permitAll()
				 	 .antMatchers("/templates/css/**","/templates/fonts/**","/templates/img/**","/templates/js/**","/templates/scss/**","/templates/vendors/**").permitAll()
				 	 
				 	 .antMatchers("/dashboard","/applications","/administrations", "/raffles", "/winners", "/notifications").hasAnyRole("Jefe","Administrador")
				 	 .anyRequest().authenticated()
			 	.and()
				 	.formLogin()
				 		.loginPage("/login").permitAll()																								
				 		.defaultSuccessUrl("/dashboard", true)
				.and()
					.logout().permitAll()																																																																											
						.invalidateHttpSession(true)												
					.deleteCookies("auth_code", "JSESSIONID")
					.logoutSuccessUrl("/")
				.and()
					.csrf()
				.and()
					.exceptionHandling().accessDeniedHandler(accessDeniedHandler); // solo cuando el usuario no tiene el rol o privilegio. Aun cuando la clave sea correcta
			
			 
		}
		
	}
	
	
	
	
	
	
	
}// fin de la clase
