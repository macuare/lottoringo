package com.tecnobackendev.lottoringo.controladores;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tecnobackendev.lottoringo.entidades.Autenticacion;
import com.tecnobackendev.lottoringo.entidades.Boletos;
import com.tecnobackendev.lottoringo.entidades.Paypal;
import com.tecnobackendev.lottoringo.entidades.Pines;
import com.tecnobackendev.lottoringo.entidades.Roles;
import com.tecnobackendev.lottoringo.entidades.Seguridad;
import com.tecnobackendev.lottoringo.entidades.Sorteos;
import com.tecnobackendev.lottoringo.entidades.Usuarios;
import com.tecnobackendev.lottoringo.modelo.FormatoComprarBoleto;
import com.tecnobackendev.lottoringo.modelo.FormatoGanadores;
import com.tecnobackendev.lottoringo.modelo.FormatoGenerarPin;
import com.tecnobackendev.lottoringo.modelo.FormatoImagenPerfil;
import com.tecnobackendev.lottoringo.modelo.FormatoLogin;
import com.tecnobackendev.lottoringo.modelo.FormatoNotificaciones;
import com.tecnobackendev.lottoringo.modelo.FormatoNuevaClave;
import com.tecnobackendev.lottoringo.modelo.FormatoNuevoUsuario;
import com.tecnobackendev.lottoringo.modelo.FormatoPaypal;
import com.tecnobackendev.lottoringo.servicios.AciertosService;
import com.tecnobackendev.lottoringo.servicios.ArchivosService;
import com.tecnobackendev.lottoringo.servicios.AutenticacionService;
import com.tecnobackendev.lottoringo.servicios.BoletosService;
import com.tecnobackendev.lottoringo.servicios.CalendarioService;
import com.tecnobackendev.lottoringo.servicios.Codificacion;
import com.tecnobackendev.lottoringo.servicios.EmailService;
import com.tecnobackendev.lottoringo.servicios.FireBaseService;
import com.tecnobackendev.lottoringo.servicios.GanadoresService;
import com.tecnobackendev.lottoringo.servicios.PaypalService;
import com.tecnobackendev.lottoringo.servicios.PinesService;
import com.tecnobackendev.lottoringo.servicios.SeguridadService;
import com.tecnobackendev.lottoringo.servicios.SorteosService;
import com.tecnobackendev.lottoringo.servicios.UsuariosService;

@RestController
@RequestMapping("/api/servicios")
public class MicroServiciosController {
	private ResponseEntity<String> reHttp;
	
	@Autowired
	private EmailService emailService;
	@Autowired
	private Codificacion codificacion;
	@Autowired
	private UsuariosService usuariosService;
	@Autowired
	private PinesService pinesService;
	@Autowired
	private SeguridadService seguridadService;
	@Autowired
	private CalendarioService calendarioService;
	@Autowired
	private AutenticacionService autenticacionService;
	@Autowired
	private PaypalService paypalService;
	@Autowired
	private ArchivosService archivosService;
	@Autowired
	private GanadoresService ganadoresService;
	@Autowired
	private SorteosService sorteosService;
	@Autowired
	private BoletosService boletosService;
	@Autowired
	private FireBaseService fireBaseService;
	@Autowired
	private AciertosService aciertosService;
	
	
	
	private void mensajeHTTP(String titulo, String mensaje, HttpStatus status){
		reHttp = ResponseEntity
				.status(status)
				.body("{\"Contexto\":\""+titulo+"\" , \"mensaje\":\""+mensaje+"\"}");
	}
	
	
	@PostMapping("/autenticacion")	
	public ResponseEntity<String> procesoAutenticacion(@RequestBody FormatoLogin formato) {
		reHttp = null;
	System.out.println("Autenticacion");
		if(formato != null && formato.getUsuario() != null) {
			Optional<Usuarios> usuario = 
					usuariosService.buscarPorUsuario(formato.getUsuario());
			
			if(usuario.isPresent()) {
				if(usuario.get().getAutenticacion().getRoles().getTipo().equalsIgnoreCase("Inactivo")) {
					this.mensajeHTTP("Autenticacion", 
					 		 "Usuario no autorizado. Contacte al Administrador.", 
					 		 HttpStatus.CONFLICT);
				}else {
					if(usuario.get().getAutenticacion().getPassword().equalsIgnoreCase(formato.getContraseña())) {
						// Creando el token.
						Optional<Seguridad> seguridad = 
								Optional.ofNullable(seguridadService.agregarNuevaSeguridad());
						
						if(seguridad.isPresent()) {
							this.mensajeHTTP("Autenticacion", 
											 seguridad.get().getToken() +
											 "\", \"tokenUsuario\":\"" + usuario.get().getIdUsuarios()
											 +"\", \"nombreUsuario\":\"" + usuario.get().getNombreUsuario()
											 +"\", \"foto\":\"" + archivosService.convertirImagenBase64(usuario.get().getImagen()), 
											 HttpStatus.CREATED);
						}else {
							this.mensajeHTTP("Autenticacion", 
									 		 "No se pudo crear el token para este usuario", 
									 		 HttpStatus.CONFLICT);
						}
						
						
					}else {
						this.mensajeHTTP("Autenticacion", 
										 "Contraseña invalida", 
										 HttpStatus.CONFLICT);
					}
				}
				
			}else {
				this.mensajeHTTP("Autenticacion", 
								 "El usuario no existe", 
								 HttpStatus.NOT_FOUND);
			}
		
		}else {
			this.mensajeHTTP("Autenticacion", 
					 "Estructura incorrecta. Valores nulos encontrado", 
					 HttpStatus.BAD_REQUEST);
		}
	
		return reHttp;
		
	}
	
	@GetMapping("/autenticacion/verificar/usuario/{nombreUsuario}")
	public ResponseEntity<String> buscarUsuario(@PathVariable(value = "nombreUsuario") String nombreUsuario){
		reHttp = null;
		
		if(nombreUsuario != null) {
			Optional<Usuarios> usuario = 
					usuariosService.buscarPorUsuario(nombreUsuario);
			
			if(usuario.isPresent()) {
				this.mensajeHTTP("Verificacion Usuario", 
								 "Existe", 
								 HttpStatus.OK);
			}else {
				this.mensajeHTTP("Verificacion Usuario", 
								 "El usuario no existe", 
								 HttpStatus.NOT_FOUND);
			}
		
		}else {
			this.mensajeHTTP("Autenticacion", 
					 "Estructura incorrecta. Valores nulos encontrado", 
					 HttpStatus.BAD_REQUEST);
		}
	
		return reHttp;
		
	}
	
	@PostMapping("/pines/nuevo")
	public ResponseEntity<String> generarPin(@RequestBody FormatoGenerarPin formato){
		reHttp = null;
		Pines pines = null;
		
		//System.out.println("--PIN para-->"+formato.toString());
		
		if(formato.getCorreo().contains("@")) {
			pines = pinesService.agregarNuevoPin();
			
			if(pines != null && pines.getPin() > 0) {
				// Enviando el correo con el pin
				emailService.enviarCorreoHTML(
						formato.getCorreo(), 
						"LottoRingo.",
						"This is your temporal pin. -> "+pines.getPin()+" valid until "+pines.getFechaExpiracion());
				
				this.mensajeHTTP("Nuevo Pin", 
						 "Pin generado correctamente", 
						 HttpStatus.OK);
				
			}else {
				this.mensajeHTTP("Nuevo Pin", 
								 "El pin no cumple con los requisitos.", 
								 HttpStatus.CONFLICT);
			}
			
			
		}else {
			this.mensajeHTTP("Nuevo Pin", 
					 "Debe ser un correo valido.", 
					 HttpStatus.CONFLICT);
			
		}
		
		
		
		return reHttp;
		
	}
	
	@GetMapping("/pines/buscar/{numeroPin}")
	public ResponseEntity<String> buscarPin(@PathVariable(value = "numeroPin") int numeroPin){
		reHttp = null;
		String titulo = "Gestionar Pin";
		
		if(String.valueOf(numeroPin).length() == 8) { // solo 8 digitos de longitud
			Optional<Pines> pin = 
					pinesService.buscarPinesPorPin(numeroPin);
			
			if(pin.isPresent() && pin.get().getPin() == numeroPin) {
				
				if(!calendarioService.verificarValidezTiempoExpiracion(pin.get().getFechaExpiracion())) {
					this.mensajeHTTP(titulo, 
									 String.valueOf(pin.get().getPin()), 
									 HttpStatus.OK);
				}else {
					pinesService.borrarPin(pin.get().getIdPines());
					
					this.mensajeHTTP(titulo, 
									 "Pin expiro.", 
									 HttpStatus.GONE);
				}
				
			}else {
				this.mensajeHTTP(titulo, 
						 		 "El pin("+numeroPin+") no existe.", 
						 		  HttpStatus.NOT_FOUND);
			}
			
			
		}else {
			this.mensajeHTTP(titulo, 
					 		 "El pin("+numeroPin+") no cumple con los requisitos.", 
					 		  HttpStatus.CONFLICT);
		}
	
		return reHttp;
		
	}
	
	@PostMapping("/autenticacion/actualizar/clave")
	public ResponseEntity<String> actualizarClave(@RequestBody FormatoNuevaClave formato){
		reHttp = null;
		String titulo = "Recuperar Contraseña";
		
		Optional<Pines> pines = pinesService.buscarPinesPorPin(formato.getCodigoPin());
		
		if(pines.isPresent()) {
			// Buscando por usuario o correo
			Optional<Autenticacion> autenticacion = 
					formato.getUsuario().contains("@") ? 
							autenticacionService.buscarAutenticacionPorCorreo(formato.getUsuario()) :
								autenticacionService.buscarAutenticacionPorUsuario(formato.getUsuario());
			
			// Si el usuario existe ya tiene clave previa				
			if(autenticacion.isPresent()) {
				// Estableciendo el nuevo password	
				autenticacion.get().setPassword(formato.getContraseña());
				
				// Actualizando con la nueva informacion y recibiendo la misma
				Autenticacion actualizado = 
						autenticacionService.actualizarAutenticacion(autenticacion.get());	
				
				// Verificando que se halla realizado los cambios.
				if(actualizado != null && formato.getContraseña().equalsIgnoreCase(actualizado.getPassword())) {
					
					pinesService.borrarPin(pines.get().getIdPines()); // Se borra el pin utilizado 
					
					this.mensajeHTTP(titulo, 
									 "Contraseña actualizada", 
									 HttpStatus.OK);
					
				}else {
					this.mensajeHTTP(titulo, 
									 "No se pudo actualizar la informacion", 
									 HttpStatus.CONFLICT);
				}
			}else {
				this.mensajeHTTP(titulo, 
								 "Este usuario no tiene una contraseña en los registros de Autenticacion", 
								 HttpStatus.CONFLICT);
			}			
			
			
		}else {
			this.mensajeHTTP(titulo, 
							 "El pin no existe.", 
							 HttpStatus.NOT_FOUND);
		}
		
		return reHttp;
		
	}
	
	@PostMapping("/usuario/nuevo")
	public ResponseEntity<String> registrarNuevoUsuario(@RequestBody FormatoNuevoUsuario formato){
		reHttp = null;
		String titulo = "Nuevo Usuario";
		
		// Verificar que el usuario no existe
		if(!usuariosService.buscarPorCorreo(formato.getCorreo()).isPresent()) {
			Optional<Pines> pines = pinesService.buscarPinesPorPin(formato.getNumeroPin());
			
			if(pines.isPresent()) {
				
				// Verificando validez del pin
				if(!calendarioService.verificarValidezTiempoExpiracion(pines.get().getFechaExpiracion())) {
					
					// Almacenando la imagen de perfil
					String imagenPerfil = 
						archivosService.guardarArchivoImagen(
							"Foto"+codificacion.codificarSHA1("lotto"+System.currentTimeMillis()),
							formato.getImagenB64()
						);
					if(imagenPerfil != null) {
						
						// Guardando nuevo registro
						Usuarios usuario = new Usuarios();
						usuario.setIdUsuarios(codificacion.codificar(String.valueOf(System.currentTimeMillis())));
						usuario.setNombreUsuario(formato.getPrimerNombre()+" "+formato.getSegundoNombre());
						usuario.setCorreo(formato.getCorreo());
						usuario.setFechaNacimiento(formato.getFechaNacimiento());
						usuario.setImagen(imagenPerfil);
						usuario.setTelefono(formato.getTelefono());	
						
						usuario = usuariosService.agregarNuevoUsuario(usuario); // Guardando y retornando valor almacenado
						
						
						if(usuario != null) {
							Roles rol = new Roles();
							rol.setIdRoles(3); // Participante
							
							Autenticacion autenticacion = new Autenticacion();
							autenticacion.setUsuarios(usuario);
							autenticacion.setPassword(formato.getClave());
							autenticacion.setRoles(rol);
							
							autenticacion =	autenticacionService.agregarNuevaAutenticacion(autenticacion); // Guardando y retornando valor almacenado
							pinesService.borrarPin(pines.get().getIdPines()); // Borrando pin utilizado
							
							if(autenticacion != null && autenticacion.getIdAutenticacion() > 0) {
								Paypal paypal = new Paypal();
								paypal.setUsuarios(usuario);
								
								if(formato.getCuentaPaypal() != null && formato.getCuentaPaypal().contains("@")) {									
									paypal.setCorreo(formato.getCuentaPaypal());
									
									paypalService.agregarNuevaCuentaPaypal(paypal); // Guardando y retornando valor almacenado
									
									this.mensajeHTTP(titulo, 
											 "Registros de Usuario almacenado con exito", 
											 HttpStatus.OK);
								}else {
									paypal.setCorreo("none");
									
									paypalService.agregarNuevaCuentaPaypal(paypal); // Guardando paypal generico
									
									this.mensajeHTTP(titulo, 
											 "Registro de Usuario almacenado con exito, su cuenta Paypal se puede hacer luego", 
											 HttpStatus.ACCEPTED);
								}
								
								
							
							}else {
								usuariosService.borrarUsuario(usuario.getIdUsuarios()); // se borra el usuario xq no se pudo crear su clave
								this.mensajeHTTP(titulo, 
										 "No se pudo agregar la clave del nuevo usuario", 
										 HttpStatus.CONFLICT);
							}
						}else {
							this.mensajeHTTP(titulo, 
											 "No se pudo agregar el nuevo usuario", 
											 HttpStatus.CONFLICT);
						}
					}else {
						this.mensajeHTTP(titulo, 
								 "No se pudo almacenar la imagen de perfil. Proceso cancelado", 
								 HttpStatus.CONFLICT);
					}	
					
					
				}else {
					pinesService.borrarPin(pines.get().getIdPines());
					
					this.mensajeHTTP(titulo, 
									 "Pin expiro.", 
									 HttpStatus.GONE);
				}
				
			}else {
				this.mensajeHTTP(titulo, 
						 "El pin no existe.", 
						 HttpStatus.NOT_FOUND);
			}
			
		}else {
			this.mensajeHTTP(titulo, 
					 "Este usuario ya existe", 
					 HttpStatus.CONFLICT);
		}
		
			
			
		return reHttp;
		
	}
		
	//=============================================
	
	/**Este metodo permite validar la vigencia del token usado y su eliminacion automatica */
	private ResponseEntity<String> validarSolicitud(String token){
		reHttp = null;
		String titulo = "Centinela";
		
		Optional<Seguridad> seguridad = 
				seguridadService.buscarPorToken(token);
		
		if(seguridad.isPresent()) {
			System.out.println("seguridad: "+seguridad.get().getIdSeguridad()+" - token:"+seguridad.get().getToken());
			
			if(!calendarioService.verificarValidezTiempoExpiracion(seguridad.get().getFechaExpiracion())) {
				this.mensajeHTTP(titulo, 
								 "Valido", 
								 HttpStatus.OK);
			}else {
				seguridadService.borrarSeguridad(token);
				
				this.mensajeHTTP(titulo, 
								 "Token expiro.", 
								 HttpStatus.GONE);
			}
		}else {
			this.mensajeHTTP(titulo, 
					 "Token no existe", 
					 HttpStatus.NOT_FOUND);
		}
		
		return reHttp;
	}
	
	@GetMapping("/ganadores/recientes/{pagina}/{cantidad}/{token}")
	public ResponseEntity<?> listaGanadores(@PathVariable(value = "pagina") int pagina, 
										    @PathVariable(value = "cantidad") int cantidad,
										    @PathVariable(value = "token") String token){
	
		
		ResponseEntity<String> validacion = 
					this.validarSolicitud(token);
		
		if(validacion.getStatusCode().is2xxSuccessful()) {
			
			List<FormatoGanadores> listado = 
					ganadoresService.listarGanadores(pagina, cantidad)
									.stream()
									.map(entidad ->{
										FormatoGanadores formato = new FormatoGanadores();
										formato.setId(entidad.getIdGanadores());
										formato.setMonto(String.valueOf(entidad.getAciertos().getMontoUSD()));
										formato.setImagenB64(
												archivosService.convertirImagenBase64(
														entidad.getBoletos().getUsuarios().getImagen()
												)
										);
										formato.setNombreUsuario(entidad.getBoletos().getUsuarios().getNombreUsuario());
										
										formato.setFechaGanador(
												calendarioService.fechaTexto(entidad.getBoletos().getSorteos().getFechaSorteo())
										);
										
										return formato;
									}).collect(Collectors.toList());
		
			// Respuesta en funcion a elementos encontrados
			HttpStatus tipoRespuesta = listado.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK;
				
			return ResponseEntity
					.status(tipoRespuesta)
					.body(listado);
			
		}else {
			return validacion;
		}
		
	
	}
	
	@GetMapping("/paypal/usuario/{tokenUsuario}/{token}")
	public ResponseEntity<String> buscarCuentaPaypalUsuario(@PathVariable(value = "tokenUsuario") String tokenUsuario,
													   		@PathVariable(value = "token") String token){
		String titulo = "Paypal";
		
		ResponseEntity<String> validacion = 
					this.validarSolicitud(token);
		
		if(validacion.getStatusCode().is2xxSuccessful()) {
			Optional<Usuarios> usuario = 
					usuariosService.buscarPorToken(tokenUsuario);
		
			if(usuario.isPresent()) {
				this.mensajeHTTP(titulo, 
						 usuario.get().getPaypal().getCorreo(), 
						 HttpStatus.OK);
			}else {
				this.mensajeHTTP(titulo, 
						 "El usuario no existe", 
						 HttpStatus.CONFLICT);
			}
				
		}else {
			reHttp = validacion;
		}
		
		return reHttp;
	}
	
	@PutMapping("/paypal/actualizar")
	public ResponseEntity<String> registrarNuevoUsuario(@RequestBody FormatoPaypal formato){
		reHttp = null;
		String titulo = "Paypal";
		
		ResponseEntity<String> validacion = 
				this.validarSolicitud(formato.getToken());
	
		if(validacion.getStatusCode().is2xxSuccessful()) {
			Optional<Paypal> paypal = 
					paypalService.buscarPaypalPorUsuario(formato.getTokenUsuario());
			
			if(paypal.isPresent()) {
				paypal.get().setCorreo(formato.getCuentaPaypal());
				paypalService.actualizarPaypal(paypal.get());
				
				this.mensajeHTTP(titulo, 
						 "Cuenta paypal actualizada.", 
						 HttpStatus.OK);
				
			}else {
				this.mensajeHTTP(titulo, 
						 "Este usuario tiene problemas con su cuenta paypal.", 
						 HttpStatus.NOT_FOUND);
			}
			
			
		}else {
			reHttp = validacion;
		}
		
		return reHttp;
	} 
	
	@GetMapping("/usuario/informacion/basica/{tokenUsuario}/{token}")
	public ResponseEntity<?> buscarInformacionUsuario(@PathVariable(value = "tokenUsuario") String tokenUsuario,
													  @PathVariable(value = "token") String token){
		String titulo = "Informacion Basica Usuario";
		
		ResponseEntity<String> validacion = 
					this.validarSolicitud(token);
		
		if(validacion.getStatusCode().is2xxSuccessful()) {
			Optional<Usuarios> usuario = 
					usuariosService.buscarPorToken(tokenUsuario);
		
			if(usuario.isPresent()) {
				// Dinamicamente convirtiendo la imagen en base64
				usuario.get().setImagen(archivosService.convertirImagenBase64(usuario.get().getImagen()));
				
				return ResponseEntity
						.status(HttpStatus.OK)
						.body(
							usuario.get()
						);
				
			}else {
				this.mensajeHTTP(titulo, 
						 "El usuario no existe", 
						 HttpStatus.CONFLICT);
				return reHttp;
			}
				
		}else {
			reHttp = validacion;
			return reHttp;
		}
		
		
	}
	
	@PutMapping("/usuario/foto")
	public ResponseEntity<String> actualizarFotoPerfil(@RequestBody FormatoImagenPerfil formato){
		reHttp = null;
		String titulo = "Imagen de Perfil";
		
		ResponseEntity<String> validacion = 
				this.validarSolicitud(formato.getToken());
	
		if(validacion.getStatusCode().is2xxSuccessful()) {
			Optional<Usuarios> usuario = 
					usuariosService.buscarPorToken(formato.getTokenUsuario());
			
			if(usuario.isPresent()) {
				// Almacenando la imagen de perfil
				String imagenPerfil = 
					archivosService.guardarArchivoImagen(
						"Foto"+codificacion.codificarSHA1("lotto"+System.currentTimeMillis()),
						formato.getImagenB64()
					);
				
				if(imagenPerfil != null) {
					// Actualizando la imagen de perfil
					usuario.get().setImagen(imagenPerfil);
					usuariosService.actualizarUsuario(usuario.get());
					
					this.mensajeHTTP(titulo, 
							 "Imagen Actualizada", 
							 HttpStatus.OK);
				}else {
					this.mensajeHTTP(titulo, 
							 "No se pudo almacenar la imagen de perfil. Proceso cancelado", 
							 HttpStatus.CONFLICT);
				}
				
				
			}else {
				this.mensajeHTTP(titulo, 
						 "Este usuario no existe.", 
						 HttpStatus.NOT_FOUND);
			}
			
			
			
		}else {
			reHttp = validacion;
		}
		
		return reHttp;
	}
	
	@GetMapping("/sorteos/recientes/{token}")
	public ResponseEntity<?> buscarSorteosRecientes(@PathVariable(value = "token") String token){
		String titulo = "sorteos";
		
		ResponseEntity<String> validacion = 
					this.validarSolicitud(token);
		
		if(validacion.getStatusCode().is2xxSuccessful()) {
			// Respuesta en funcion a elementos encontrados
		   return ResponseEntity
					.status(HttpStatus.OK)
					.body(
					  sorteosService.listarSorteos(0, 10)
					);
			
		}else {
			return validacion;
		}
		
	}
	
	@GetMapping("/sorteos/{idSorteo}/{token}")
	public ResponseEntity<?> buscarSorteosPorId(@PathVariable(value = "token") String token,
												@PathVariable(value = "idSorteo") int idSorteo){
		String titulo = "sorteos";
		
		ResponseEntity<String> validacion = 
					this.validarSolicitud(token);
		
		if(validacion.getStatusCode().is2xxSuccessful()) {
			
			Optional<Sorteos> sorteo = 
					sorteosService.buscarSorteoPorId(idSorteo);
			
			if(sorteo.isPresent()) {
				return ResponseEntity
						.status(HttpStatus.OK)
						.body(
						 sorteo.get()
						);
			}else {
				this.mensajeHTTP(titulo, 
						 "Este sorteo no existe", 
						 HttpStatus.CONFLICT);
				
				return reHttp;
			}
			
		}else {
			return validacion;
		}
		
	}
	
	@GetMapping("/boletos/{idSorteo}/{tokenUsuario}/{token}")
	public ResponseEntity<?> buscarBoletosUsuarioPorSorteo(@PathVariable(value = "token") String token,
														   @PathVariable(value = "idSorteo") int idSorteo,
														   @PathVariable(value = "tokenUsuario") String tokenUsuario){
		String titulo = "boletos";
		
		ResponseEntity<String> validacion = 
				this.validarSolicitud(token);
	
		if(validacion.getStatusCode().is2xxSuccessful()) {
			
			Optional<Usuarios> usuario = 
					usuariosService.buscarPorToken(tokenUsuario);
			
			if(usuario.isPresent()) {
				Optional<Sorteos> sorteo = 
						sorteosService.buscarSorteoPorId(idSorteo);
				
				if(sorteo.isPresent()) {
					List<Boletos> lista = 
							boletosService.buscarBoletosPorSorteo(sorteo.get())
										  .stream()
										  .filter(entidad -> entidad.getUsuarios().getIdUsuarios().equalsIgnoreCase(tokenUsuario))
										  .collect(Collectors.toList());
					
					return ResponseEntity
							.status(HttpStatus.OK)
							.body(
								lista
							);
					
				}else {
					this.mensajeHTTP(titulo, 
							 "Este sorteo no existe", 
							 HttpStatus.NOT_FOUND);
					return reHttp;
				}
				
			}else {
				this.mensajeHTTP(titulo, 
						 "El usuario no existe", 
						 HttpStatus.NOT_FOUND);
				return reHttp;
			}
			
		}else {
			reHttp = validacion;
			return reHttp;
		}
		
	}
	
	@PostMapping("/boletos/nuevo")
	public ResponseEntity<String> guardarNuevoBoleto(@RequestBody FormatoComprarBoleto formato){
		String titulo = "boletos";
		
		ResponseEntity<String> validacion = 
					this.validarSolicitud(formato.getToken());
		
		if(validacion.getStatusCode().is2xxSuccessful()) {
			
			Optional<Usuarios> usuario =
					usuariosService.buscarPorToken(formato.getTokenUsuario());
			
			if(usuario.isPresent()) {
				
				Optional<Sorteos> sorteo = 
						sorteosService.buscarSorteoPorId(formato.getIdSorteo());
				
				if(sorteo.isPresent()) {
					Boletos nuevoBoleto = new Boletos();
					nuevoBoleto.setBola1(formato.getBola1());
					nuevoBoleto.setBola2(formato.getBola2());
					nuevoBoleto.setBola3(formato.getBola3());
					nuevoBoleto.setBola4(formato.getBola4());
					nuevoBoleto.setBola5(formato.getBola5());
					nuevoBoleto.setBolaSuerte(formato.getBolaSuerte());
					nuevoBoleto.setSorteos(sorteo.get());
					nuevoBoleto.setUsuarios(usuario.get());
					
					
					if(boletosService.agregarNuevoBoleto(nuevoBoleto) != null) {
						this.mensajeHTTP(titulo, 
								 formato.getBola1()+"-"+formato.getBola2()+"-"+formato.getBola3()+"-"+formato.getBola4()+"-"+formato.getBola5()+"-"+formato.getBolaSuerte(), 
								 HttpStatus.OK);
					}else {
						this.mensajeHTTP(titulo, 
								 "Este boleto no se pudo agregar", 
								 HttpStatus.CONFLICT);
					}
					
				}else {
					this.mensajeHTTP(titulo, 
							 "Este sorteo no existe", 
							 HttpStatus.NOT_FOUND);
				}
				
			}else {
				this.mensajeHTTP(titulo, 
						 "El usuario no existe", 
						 HttpStatus.NOT_FOUND);
			}
			
		}else {
			reHttp = validacion;
		}
		
		return reHttp;
	}
	
	@GetMapping("/loginout/{token}")
	public ResponseEntity<String> loginout(@PathVariable(value = "token") String token){
		String titulo = "Login Out";
		
		ResponseEntity<String> validacion = 
					this.validarSolicitud(token);
		
		if(validacion.getStatusCode().is2xxSuccessful()) {
			
			seguridadService.borrarSeguridad(token);
			
			this.mensajeHTTP(titulo, 
					 "Hasta pronto...!!!", 
					 HttpStatus.OK);
			
		}else {
			return validacion;
		}
		
		return reHttp;
	}
	
	
	@PostMapping("/firebase/suscripcion")
	public ResponseEntity<String> firebaseToken(@RequestBody FormatoNotificaciones formato){
		String titulo = "Notificaciones";
		
		ResponseEntity<String> validacion = 
					this.validarSolicitud(formato.getToken());
		
		if(validacion.getStatusCode().is2xxSuccessful()) {
			fireBaseService.inicializarServicio();
			fireBaseService.suscripcionTopico(formato.getTokenFirebase());
			
			this.mensajeHTTP(titulo, 
					 "Suscripcion realizada", 
					 HttpStatus.OK);
			
		}else {
			reHttp = validacion;
		}
		
		return reHttp;
		
	}
	
	@GetMapping("/jackpot/{token}")
	public ResponseEntity<String> jackpot(@PathVariable(value = "token") String token){
		String titulo = "jackpot";
		
		ResponseEntity<String> validacion = 
					this.validarSolicitud(token);
		
		if(validacion.getStatusCode().is2xxSuccessful()) {
			
			int pote = 
					aciertosService.listarAciertos()
								   .stream()
								   .filter(s -> s.getBolas() == 5 && s.getBolaSuerte() == 1)
								   .findFirst()
								   .get()
								   .getMontoUSD()
								   .intValue();
			
			DecimalFormat df = new DecimalFormat("0.00");
			
			this.mensajeHTTP(titulo, 
					 df.format(pote)+" $", 
					 HttpStatus.OK);
			
		}else {
			return validacion;
		}
		
		return reHttp;
	}
	
	
	
	//-----------------------------------------
	
	@GetMapping("/prueba")	
	public ResponseEntity<String> prueba(){
		ResponseEntity<String> reHttp = null;
		System.out.println("ruta prueba");
		
		reHttp = ResponseEntity
				.ok("{\"Mensaje\":\"Solcitud valida\"}");
		
		return reHttp;
		
	}

}
