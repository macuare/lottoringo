package com.tecnobackendev.lottoringo.controladores;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.tecnobackendev.lottoringo.entidades.Aciertos;
import com.tecnobackendev.lottoringo.entidades.Autenticacion;
import com.tecnobackendev.lottoringo.entidades.Boletos;
import com.tecnobackendev.lottoringo.entidades.Ganadores;
import com.tecnobackendev.lottoringo.entidades.Paypal;
import com.tecnobackendev.lottoringo.entidades.Roles;
import com.tecnobackendev.lottoringo.entidades.Sorteos;
import com.tecnobackendev.lottoringo.entidades.Usuarios;
import com.tecnobackendev.lottoringo.modelo.FormatoNuevaClave;
import com.tecnobackendev.lottoringo.modelo.FormatoSorteosStatus;
import com.tecnobackendev.lottoringo.repositorios.AciertosRepository;
import com.tecnobackendev.lottoringo.servicios.AciertosService;
import com.tecnobackendev.lottoringo.servicios.ArchivosService;
import com.tecnobackendev.lottoringo.servicios.AutenticacionService;
import com.tecnobackendev.lottoringo.servicios.BoletosService;
import com.tecnobackendev.lottoringo.servicios.CalendarioService;
import com.tecnobackendev.lottoringo.servicios.Codificacion;
import com.tecnobackendev.lottoringo.servicios.EmailService;
import com.tecnobackendev.lottoringo.servicios.FireBaseService;
import com.tecnobackendev.lottoringo.servicios.GanadoresService;
import com.tecnobackendev.lottoringo.servicios.PaypalService;
import com.tecnobackendev.lottoringo.servicios.SorteosService;
import com.tecnobackendev.lottoringo.servicios.UsuariosService;

@Controller
public class WebController {
	@Autowired
	private AutenticacionService autenticacionService;
	@Autowired
	private EmailService emailService;
	@Autowired
	private Codificacion codificacion;
	@Autowired
	private ArchivosService archivosService;
	@Autowired
	private SorteosService sorteosService;
	@Autowired
	private GanadoresService ganadoresService;
	@Autowired
	private UsuariosService usuariosService;
	@Autowired
	private PaypalService paypalService;
	@Autowired
	private CalendarioService calendarioService;
	@Autowired
	private BoletosService boletosService;
	@Autowired
	private AciertosService aciertosService;
	@Autowired
	private FireBaseService fireBaseService;
	
	
	
	
	private void plantillaPerfil(Model modelo, Authentication sesionActual) {
		
		
		// Obteniendo Datos del Jefe/Administrador
		Autenticacion autenticacion = 
						autenticacionService.buscarAutenticacionPorUsuario(sesionActual.getName()).get();
		
		List<Ganadores> listadoGanadores =
						  ganadoresService.listarGanadores()
										  .stream()
										  .filter(ganadores -> ganadores.isStatus() == false)
										  .collect(Collectors.toList());
		
		int cantidadGanadoresPagosPendiente = 
				listadoGanadores.size();
		int sinCuentaPaypal = 
				(int) listadoGanadores.stream()
									  .filter(entidad -> entidad.getBoletos().getUsuarios().getPaypal().getCorreo().equalsIgnoreCase("none"))
									  .count();
		
		Date fechaActual = new Date(System.currentTimeMillis());
		
		modelo.addAttribute("FotoPefil", archivosService.convertirImagenBase64(autenticacion.getUsuarios().getImagen()));
		modelo.addAttribute("Usuario", sesionActual.getName());
		modelo.addAttribute("RolUsuario", sesionActual.getAuthorities().toArray()[0].toString()
																			 .replace("ROLE_", ""));
		modelo.addAttribute("fechaActual",fechaActual);
		modelo.addAttribute("cantidadGanadoresPagosPendiente",cantidadGanadoresPagosPendiente);
		modelo.addAttribute("sinCuentaPaypal",sinCuentaPaypal);
	}
	
	
	@RequestMapping("favicon.ico")
    String favicon() {
		return "forward:/favicon.ico";
	}
	
	@GetMapping("/")
    public String home() {
		System.out.println("Primera pagina -> Login");
		return "login";
    }
	
	@GetMapping("/login")
    public String login() {
		System.out.println("Pagina Login");
		
		return "login";
		
    }
	
	@GetMapping("/recovery")
    public String recuperarClave() {
		System.out.println("Pagina recuperacion contraseña");
		
		return "forgot-password";
		
    }
	
	@PostMapping("/recovery")
    public String claveTemporalCorreo(@RequestParam(name="correo", defaultValue="ninguno@gmail.com") String correoElectronico) {
		System.out.println("Enviando nueva contraseña");
		String reWeb = null;
		
		Optional<Autenticacion> autenticacion = null;
		
		if(correoElectronico.contains("@")) 
			autenticacion = 
					autenticacionService.buscarAutenticacionPorCorreo(correoElectronico); 
		else 
			autenticacion = 
					autenticacionService.buscarAutenticacionPorUsuario(correoElectronico);
		
		if(autenticacion.isPresent() && 
		   (autenticacion.get().getRoles().getTipo().equalsIgnoreCase("Jefe") ||
			autenticacion.get().getRoles().getTipo().equalsIgnoreCase("Administrador"))
		   ) {
			
			// Modificando la clave del usuario a una temporal
			String nueva = 
					codificacion.codificarSHA1(String.valueOf(System.currentTimeMillis()));
			
			String temporal = 
					codificacion.codificar(nueva);
			
			// Estableciendo el nuevo pass y actualizando la base de datos.
			autenticacion.get().setPassword(temporal);
			autenticacionService.actualizarAutenticacion(autenticacion.get());
			
			// Enviando mensaje y clave al correo
			emailService.enviarCorreoHTML(
					autenticacion.get().getUsuarios().getCorreo(), 
					"LottoRingo. Password Recovery",
					"This is your temporal pass. -> "+nueva+" (Copy and Paste)");
			
			reWeb = "redirect:/login";
		}else {
			reWeb = "redirect:/recovery?error=true";
		}
		
		
		return reWeb;
		
    }
	
	@GetMapping("/reset")
    public String resetearClave() {
		System.out.println("Pagina recuperacion contraseña");
		
		return "update-pass";
		
    }
	
	@PostMapping("/update/password")
    public String actualizandoClave(@RequestParam(name="clave1", defaultValue="1") String clave1,
    							 	@RequestParam(name="clave2", defaultValue="2") String clave2,
    							 	Authentication sesionActual) {
		System.out.println("Actualizando contraseña");
		String reWeb = null;
		
		if(clave1.equals(clave2)) {
			Optional<Autenticacion> autenticacion = null;
			
			if(sesionActual.getName().contains("@")) 
				autenticacion = 
						autenticacionService.buscarAutenticacionPorCorreo(sesionActual.getName()); 
			else 
				autenticacion = 
						autenticacionService.buscarAutenticacionPorUsuario(sesionActual.getName());
			
			if(autenticacion.isPresent() && 
			   (autenticacion.get().getRoles().getTipo().equalsIgnoreCase("Jefe") ||
				autenticacion.get().getRoles().getTipo().equalsIgnoreCase("Administrador"))
			   ) {
				
				// Modificando la clave del usuario
				String nueva = 
						codificacion.codificar(clave1);
				
				// Estableciendo el nuevo pass y actualizando la base de datos.
				autenticacion.get().setPassword(nueva);
				autenticacionService.actualizarAutenticacion(autenticacion.get());
				
				// Enviando mensaje y clave al correo
				emailService.enviarCorreoHTML(
						autenticacion.get().getUsuarios().getCorreo(), 
						"LottoRingo. Password Update",
						"Your password has been updated...!!!");
				
				sesionActual.setAuthenticated(false); // terminando la sesion
				reWeb = "redirect:/login";
			}else {
				sesionActual.setAuthenticated(false); // terminando la sesion
				reWeb = "redirect:/login";
			}
			
		}else {
			reWeb = "redirect:/reset?error=true&mensaje=Password does not match...!!!";
		}
		
		reWeb = reWeb.replaceAll(" ", "+"); // Quita los espacios en blanco de la Url por +(Concatenacion)
		return reWeb;
		
    }
	
	
	
	@GetMapping("/new-user")
    public String nuevoUsuario() {
		System.out.println("Pagina recuperacion contraseña");
		
		return "register";
		
    }
	
	
	
	@GetMapping("/dashboard")
    public String dashboard(Model modelo, Authentication sesionActual) {
		System.out.println("pagina para dashboard");
		
		// Obteniendo Datos del Jefe/Administrador
//		Autenticacion autenticacion = 
//				autenticacionService.buscarAutenticacionPorUsuario(sesionActual.getName()).get();
		this.plantillaPerfil(modelo, sesionActual);
		
		
		// Usuarios Registrados
		long participantesRegistrados =
			    autenticacionService.listarAutenticacion()
									.stream()
									.filter(s -> s.getRoles().getTipo().equalsIgnoreCase("Participante"))
									.count();
		
		// Contando los sorteos totales entre los realizados y propuestos.
		int totalSorteos = 
				sorteosService.listarSorteos().size();
		
		// Contando los total de ganadores
		int totalGanadores = 
				ganadoresService.listarGanadores().size();
		
		
//		modelo.addAttribute("FotoPefil", archivosService.convertirImagenBase64(autenticacion.getUsuarios().getImagen()));
//		modelo.addAttribute("Usuario", sesionActual.getName());
//		modelo.addAttribute("RolUsuario", sesionActual.getAuthorities().toArray()[0].toString()
//																			 .replace("ROLE_", ""));
		modelo.addAttribute("Participantes", participantesRegistrados);
		modelo.addAttribute("TotalSorteos", totalSorteos);
		modelo.addAttribute("TotalGanadores", totalGanadores);
		
		return "index";
		
    }
	
	@GetMapping("/applications")
    public String usersApp(Model modelo, Authentication sesionActual) {
		System.out.println("users-app");
		this.plantillaPerfil(modelo, sesionActual);
		
		List<Autenticacion> listaUsuarios = 
				autenticacionService.listarAutenticacion()
									.stream()
									.filter(entidad -> 
											entidad.getRoles().getTipo().equalsIgnoreCase("Participante") ||
											entidad.getRoles().getTipo().equalsIgnoreCase("Inactivo")
											)
									.map(entidad ->{
										entidad.getUsuarios().setImagen(archivosService.convertirImagenBase64(entidad.getUsuarios().getImagen()));
										
										return entidad;
									})
									.collect(Collectors.toList());
		
		modelo.addAttribute("listaUsuarios", listaUsuarios);
		
		
		
		return "users-app";
		
    }
	
	@PostMapping("/applications")
	public String ActualizarStatusUsuario(@RequestParam(name="identificador", defaultValue="0") int identificador,
										  @RequestParam(name="statusUsuario", defaultValue="0") int statusUsuario) {
		
		Optional<Autenticacion> autenticacion = 
				autenticacionService.buscarAutenticacionPorId(identificador);
		
		if(autenticacion.isPresent()) {
			statusUsuario = statusUsuario == 1 ? 3 : 4; 
		
			Roles rol = new Roles();
			rol.setIdRoles(statusUsuario);
			autenticacion.get().setRoles(rol);
			autenticacionService.actualizarAutenticacion(autenticacion.get());
		}
		
		
		return "users-app";
	}
	
	@GetMapping("/administrations")
    public String usersAdmin(Model modelo, Authentication sesionActual) {
		System.out.println("users-admin");
		
		this.plantillaPerfil(modelo, sesionActual);
		
		List<Autenticacion> listaAdministradores = 
				autenticacionService.listarAutenticacion()
									.stream()
									.filter(elemento -> elemento.getRoles().getTipo().equalsIgnoreCase("Jefe") || 
														elemento.getRoles().getTipo().equalsIgnoreCase("Administrador") )
									.map(entidad -> {
										Autenticacion autenticacion = entidad;
										autenticacion.getUsuarios().setImagen( // Estableciendo imagen codificada
												archivosService.convertirImagenBase64(autenticacion.getUsuarios().getImagen())
												);
										return autenticacion;
									})
									.collect(Collectors.toList());
		
		modelo.addAttribute("listaAdministradores", listaAdministradores);
		
		
		return "users-admin";
		
    }
	
	@PostMapping("/administrations")
	public String agregarNuevoAdministrador(@RequestParam(name="nombre", defaultValue="x") String nombre,
										    @RequestParam(name="segundoNombre", defaultValue="x") String segundoNombre,
										    @RequestParam(name="telefono", defaultValue="x") String telefono,
										    @RequestParam(name="correo", defaultValue="x") String correo,
										    @RequestParam(name="clave", defaultValue="x") String clave) {
		
		System.out.println("Creando nuevo administrador");
		
		Optional<Usuarios> usuarioActual = 
				usuariosService.buscarPorCorreo(correo);
		
		if(usuarioActual.isPresent()) { // Existe previamente y necesita solo activarse como admin
			System.out.println("Habilitando Admin existente");
			
			Roles rol = new Roles();
			rol.setIdRoles(2); // Como administrador
			
			Autenticacion autenticacion = usuarioActual.get().getAutenticacion();
			autenticacion.setRoles(rol);
			autenticacion.setPassword(clave);
			
			autenticacionService.actualizarAutenticacion(autenticacion);
			
		}else { // Sino se crea un nuevo usuario
			System.out.println("Creando nuevo Admin");
			
			Usuarios usuario = new Usuarios();
			usuario.setIdUsuarios(codificacion.codificar(String.valueOf(System.currentTimeMillis())));
			usuario.setNombreUsuario(nombre+" "+segundoNombre);
			usuario.setCorreo(correo);
			usuario.setTelefono(telefono);
			usuario.setFechaNacimiento(new Date(System.currentTimeMillis()));
			usuario.setImagen("generica.png");
			
			// Almancenando y obteniendo sus datos
			usuario = 
					usuariosService.agregarNuevoUsuario(usuario);
			
			// Estableciendo rol del nuevo usuario
			Roles rol = new Roles();
			rol.setIdRoles(2); // Administrador
			
			// Creando autenticacion
			Autenticacion autenticacion = new Autenticacion();
			autenticacion.setUsuarios(usuario);
			autenticacion.setPassword(clave);
			autenticacion.setRoles(rol);
			
			// Almacenando la autenticacion
			autenticacionService.agregarNuevaAutenticacion(autenticacion);
			
			// Creando Paypal generico
			Paypal paypal = new Paypal();
			paypal.setUsuarios(usuario);
			paypal.setCorreo("none");
			
			paypalService.agregarNuevaCuentaPaypal(paypal);
		}
		
		return "users-admin";
	}
	
	@PutMapping("/administrations")
	public String actualizarAdministrador(@RequestParam(name="nombre", defaultValue="x") String nombre,
									      @RequestParam(name="segundoNombre", defaultValue="x") String segundoNombre,
									      @RequestParam(name="correoElectronico", defaultValue="x") String correo,
									      Authentication sesionActual) {
		
		System.out.println("Actualizando administrador");
		
		if(nombre.equalsIgnoreCase("x")) {
			System.out.println("Proceso de actualizacion abortada");
		}else {

			Optional<Usuarios> usuario =
					usuariosService.buscarPorCorreo(correo);
			
			String nombreUsuarioOld = null;
			
			if(usuario.isPresent()) { // Actualizando registros del usuario de interes.
				nombreUsuarioOld = usuario.get().getNombreUsuario(); // Nombre anterior antes de cambiarlo
				usuario.get().setNombreUsuario(nombre+" "+segundoNombre);
				usuariosService.actualizarUsuario(usuario.get());
			}
			
			// Desloguea al mismo usuario que se actualizo
			if(sesionActual.getName().equalsIgnoreCase(nombreUsuarioOld)) {
				System.out.println("Mismo usuario haciendo cambios. Reiniciando conexion");
				sesionActual.setAuthenticated(false);
			}
		}
		
		return "users-admin";
	}
	
	@DeleteMapping("/administrations")
	public String eliminarAdministrador(@RequestParam(name="correoElectronico", defaultValue="x") String correo) {
		
		System.out.println("Borrando Administrador");
		
		Optional<Usuarios> usuario =
				usuariosService.buscarPorCorreo(correo);
		
		if(usuario.isPresent()) {
			Roles rol = new Roles();
			rol.setIdRoles(4); // Inactivando usuario Administrador
			
			Autenticacion autenticacion = 
					usuario.get().getAutenticacion();
			
			autenticacion.setRoles(rol);
			
			autenticacionService.actualizarAutenticacion(autenticacion);
			
		}
		
		return "users-admin";
	}
	
	@GetMapping("/raffles")
    public String raffles(Model modelo, Authentication sesionActual) {
		System.out.println("raffles");
		
		this.plantillaPerfil(modelo, sesionActual);
		
		List<FormatoSorteosStatus> listaSorteos = 
						sorteosService.listarSorteos(0, 10)
									  .stream()
									  .map(mapper -> {
										  Sorteos sorteo = mapper;
										  boolean vigente = calendarioService.verificarValidezTiempoExpiracion(mapper.getFechaSorteo());
										  
										  FormatoSorteosStatus fss = new FormatoSorteosStatus();
										  fss.setSorteos(sorteo);
										  fss.setVigente(vigente);
										  
										  return fss;
									  }).collect(Collectors.toList());
	
		modelo.addAttribute("listaSorteos", listaSorteos);
		
		return "raffles";
    }
	
	@PostMapping("/raffles")
    public String nuevoSorteo(@RequestParam(name="fechaSorteo", defaultValue="01/25/2019") String fecha,
    						  @RequestParam(name="jackpotSorteo", defaultValue="0.00") String jackpot         ) {
		System.out.println("Nuevo sorteo -> "+fecha);
		
		LocalTime lt = LocalTime.of(23, 59, 59);
		LocalDate ld = LocalDate.parse(fecha, DateTimeFormatter.ofPattern("MM/d/yyyy"));
		LocalDateTime ldt = LocalDateTime.of(ld, lt);
		
		if(!calendarioService.verificarValidezTiempoExpiracion(Date.from(ldt.toInstant(ZoneOffset.UTC)))) {
			// Agregando nuevo sorteo
			Sorteos sorteo = new Sorteos();
			sorteo.setBola1(0);
			sorteo.setBola2(0);
			sorteo.setBola3(0);
			sorteo.setBola4(0);
			sorteo.setBola5(0);
			sorteo.setBolaSuerte(0);
			sorteo.setFechaSorteo(Date.from(ldt.toInstant(ZoneOffset.UTC)));
			
			sorteosService.agregarNuevoSorteo(sorteo);
			
			// Agregando el jackpot
			Aciertos acierto = 
				aciertosService.listarAciertos()
							   .stream()
							   .filter(s -> s.getBolas() == 5 && s.getBolaSuerte() == 1)
							   .findFirst()
							   .get();
			
			acierto.setMontoUSD(new BigDecimal(jackpot)); // Modificando el monto del sorteo
			
			aciertosService.actualizarAcierto(acierto); 
			
		}else {
			System.out.println("Fecha sugerida es menor al de hoy.");
		}
		
		
		
		return "raffles";
    }
	
	@PutMapping("/raffles")
	public String actualizarSorteo(@RequestParam(name="id", defaultValue="0") int id,
								   @RequestParam(name="bola", defaultValue="0") int bola,
								   @RequestParam(name="valor", defaultValue="0") int valor) {
		
		System.out.println("Actualizando Sorteo -> id:"+id+" | bola:"+bola+" | valor"+valor);
		
		if(valor >= 0 && valor <= 75) {
			Optional<Sorteos> sorteo = 
					sorteosService.buscarSorteoPorId(id);
			
			
				if(sorteo.isPresent()) {
					if(!calendarioService.verificarValidezTiempoExpiracion(sorteo.get().getFechaSorteo())) {
						switch(bola) {
							case 1: 
								sorteo.get().setBola1(valor);
								sorteosService.actualizarSorteo(sorteo.get());
								break;
							case 2: 
								sorteo.get().setBola2(valor);
								sorteosService.actualizarSorteo(sorteo.get());
								break;
							case 3: 
								sorteo.get().setBola3(valor);
								sorteosService.actualizarSorteo(sorteo.get());
								break;
							case 4: 
								sorteo.get().setBola4(valor);
								sorteosService.actualizarSorteo(sorteo.get());
								break;
							case 5: 
								sorteo.get().setBola5(valor);
								sorteosService.actualizarSorteo(sorteo.get());
								break;
							default:
								sorteo.get().setBolaSuerte(valor);
								sorteosService.actualizarSorteo(sorteo.get());
								break;
						}
					}else {
						System.out.println("fecha de sorteo ya paso, no se pueden modificar los valores");
					}
				}else {
					System.out.println("No se encontro el sorteo por su ID");
				}
			
		}else {
			System.out.println("Valor esta fuera de rango");
		}
		
		return "raffles";
	}
	
	@DeleteMapping("/raffles")
	public String eliminarSorteo(@RequestParam(name="id", defaultValue="0") int id) {
		
		System.out.println("Borrando Sorteo ->"+id);
		
		Optional<Sorteos> sorteo = 
				sorteosService.buscarSorteoPorId(id);
		
		if(sorteo.isPresent()) {
			if(!calendarioService.verificarValidezTiempoExpiracion(sorteo.get().getFechaSorteo())) {
				sorteosService.borrarSorteo(id);
			}else {
				System.out.println("Este sorteo ya paso y no se puede eliminar");
			}
		}else {
			System.out.println("No se encontro el sorteo por su ID:"+id);
		}
		
		
		return "raffles";
	}
	
	
	
	
	@GetMapping("/winners")
    public String ganadores(Model modelo, Authentication sesionActual) {
		System.out.println("winners");
		
		this.plantillaPerfil(modelo, sesionActual);
		
		List<Ganadores> listaParcial = 
				ganadoresService.listarGanadores(0, 30)
								.stream()
								.filter(entidad -> entidad.isStatus() == false)
								//.sorted(Comparator.comparing(Ganadores::isStatus))
								.map(entidad -> {
									Usuarios usuario = entidad.getBoletos().getUsuarios();
									usuario.setImagen(archivosService.convertirImagenBase64(usuario.getImagen())); // Convirtiendo la Imagen a b64
									
									return entidad;
								})
								.collect(Collectors.toList());
		
		modelo.addAttribute("listaGanadores", listaParcial);
		
		return "winners";
		
    }
	
	@PostMapping("/winners")
    public String nuevosGanadores(@RequestParam(name="id", defaultValue="0") int id) {
		System.out.println("nuevos winners");
		// Buscar el sorteo por id y los nuevos ganadores de ese sorteo. comparar por fecha y crear la lista de los nuevos ganadores
		Optional<Sorteos> sorteo = 
					sorteosService.buscarSorteoPorId(id);
		
		if(sorteo.isPresent()) {
			
			// Buscando todos los boletos de los usuarios que coinciden con el sorteo de interes
			List<Boletos> boletosValidos = 
					boletosService.buscarBoletosPorSorteo(sorteo.get());
			
			// Borrando todos los resultados asociados al sorteo de interes. Evitar duplicidad para recalculos.
			List<Ganadores> listaGanadoresPrevios = 
					ganadoresService.listarGanadores(boletosValidos);
			
			ganadoresService.borrarGanadores(listaGanadoresPrevios);
			
			// Cargando Aciertos validos
			List<Aciertos> tipoAciertos = 
					aciertosService.listarAciertos();
			
			// Buscando ganadores nuevos
			List<Ganadores> listaGanadoresComprobados =
				boletosValidos.stream()
							  .map(boleto ->{
									AtomicInteger nroBolasCoincidentes = new AtomicInteger(0);
									AtomicInteger bolaSuerte = new AtomicInteger(0);
									
									// Coincidencias
									if(sorteo.get().getBola1() == boleto.getBola1()) nroBolasCoincidentes.incrementAndGet();
									if(sorteo.get().getBola2() == boleto.getBola2()) nroBolasCoincidentes.incrementAndGet();
									if(sorteo.get().getBola3() == boleto.getBola3()) nroBolasCoincidentes.incrementAndGet();
									if(sorteo.get().getBola4() == boleto.getBola4()) nroBolasCoincidentes.incrementAndGet();
									if(sorteo.get().getBola5() == boleto.getBola5()) nroBolasCoincidentes.incrementAndGet();
									if(sorteo.get().getBolaSuerte() == boleto.getBolaSuerte()) bolaSuerte.set(1);
									
									// Acierto valido
									Optional<Aciertos> aciertoActual = 
															tipoAciertos.stream()
																		.filter(z -> (z.getBolas() == nroBolasCoincidentes.get() && 
																					  z.getBolaSuerte() == bolaSuerte.get())
																				)
																		.findFirst();
									
									Ganadores ganador = null;
									// Se genera un boleto ganador solo cuando existe un acierto			
									if(aciertoActual.isPresent()) {
										// Creando boleto ganador
										ganador = new Ganadores();
										ganador.setBoletos(boleto);
										ganador.setAciertos(aciertoActual.get());
										ganador.setStatus(false);
										ganador.setAcreditado(new BigDecimal("0.000"));
										
									}
								
									return ganador;
					
							  })
							  .filter(entidad -> entidad != null)
							  .collect(Collectors.toList());
			
//			System.out.println("LISTA GANADORES COMPROBADOS");
//			listaGanadoresComprobados.forEach(System.out::println);
			
			// dividir el pote por ganadores
			Map<Aciertos, List<Ganadores>> resultado =
					listaGanadoresComprobados.stream()
											 .collect(Collectors.groupingBy(Ganadores::getAciertos));
			
//			System.out.println("=============GANADORES POR TIPO==============");
			resultado.entrySet().forEach(entidad ->{
				//System.out.println(entidad.getKey()+" - "+entidad.getValue().toString());
				entidad.getValue().forEach(nuevo -> {
					nuevo.setAcreditado(entidad.getKey().getMontoUSD().divide(new BigDecimal(entidad.getValue().size())));
					// Guardando nuevo ganador con la cantidad de dinero que le corresponde pote/nroGanadores
					ganadoresService.agregarNuevoGanador(nuevo);
				});
			});
			
			
		}else {
			System.out.println("Sorteo no encontrado");
		}
		
		
		return "winners";
		
    }
	
	@PutMapping("/winners")
	public String pagarGanador(@RequestParam(name="identificador", defaultValue="0") int id) {
		
		ganadoresService.buscarGanadorPorId(id)
						.ifPresent(entidad -> {	
							// Pagar al ganador por paypal
							
							// Establecer que el ganador ya se le pago
							entidad.setStatus(true);
							ganadoresService.actualizarGanador(entidad);
						});
		
		return "winners";
	}
	
	
	
	
	@GetMapping("/notifications")
    public String notificaciones(Model modelo, Authentication sesionActual) {
		System.out.println("notifications");
		
		this.plantillaPerfil(modelo, sesionActual);
	
		return "send-noti";
		
    }
	
	@PostMapping("/notifications")
    public String enviarNotificaciones(@RequestParam(name="mensaje", defaultValue="Saludos...!!!") String mensaje,
    								   @RequestParam(name="titulo", defaultValue="LottoRingo 2019") String titulo,
    								   Model modelo, Authentication sesionActual) {
		System.out.println("notifications");
		this.plantillaPerfil(modelo, sesionActual);
		
		// Incializando servicio de firebase
		fireBaseService.inicializarServicio();
		
		fireBaseService.enviarMensaje(titulo, mensaje);
	
		return "send-noti";
    }
	
	
	
	@GetMapping("/403")
    public String error403() {
        return "403";
    }
	
}
