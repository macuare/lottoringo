package com.tecnobackendev.lottoringo;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.PathResourceResolver;

@Configuration
public class WebConfig implements WebMvcConfigurer{
	//Ejm: The CSS static resources are located in the src/main/resources/static/css folder.
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		// TODO Auto-generated method stub
		registry.addResourceHandler(
						"/img/**",
		                "/css/**",
		                "/js/**",
		                "/scss/**",
		                "/vendor/**",
		                "/lib/**")
				.addResourceLocations(
                        "classpath:/templates/img/",
                        "classpath:/templates/css/",
                        "classpath:/templates/js/",
                        "classpath:/templates/scss/",
                        "classpath:/templates/vendor/",
                        "classpath:/templates/lib/")
				.resourceChain(true)
				.addResolver(new PathResourceResolver());
		
		//WebMvcConfigurer.super.addResourceHandlers(registry);
	}

	
}
