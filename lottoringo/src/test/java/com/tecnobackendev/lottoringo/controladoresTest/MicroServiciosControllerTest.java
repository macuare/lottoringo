package com.tecnobackendev.lottoringo.controladoresTest;

import static org.junit.Assert.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.tecnobackendev.lottoringo.LottoringoApplication;
import com.tecnobackendev.lottoringo.entidades.Pines;
import com.tecnobackendev.lottoringo.modelo.FormatoGenerarPin;
import com.tecnobackendev.lottoringo.modelo.FormatoImagenPerfil;
import com.tecnobackendev.lottoringo.modelo.FormatoLogin;
import com.tecnobackendev.lottoringo.modelo.FormatoNuevaClave;
import com.tecnobackendev.lottoringo.modelo.FormatoNuevoUsuario;
import com.tecnobackendev.lottoringo.modelo.FormatoPaypal;
import com.tecnobackendev.lottoringo.servicios.CalendarioService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LottoringoApplication.class,
webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MicroServiciosControllerTest {
	@Autowired
	private TestRestTemplate testRestTemplate;
	@LocalServerPort
	private int puerto;
	
	@Autowired
	CalendarioService calendarioService;
	
	private String getRootUrl() {
        return "http://localhost:" + puerto;
    }
	
	@Before
	public void setUp() throws Exception {
		System.out.println("-------------------PRUEBAS DE INTEGRACION MICROSERVICIOS-----------------");
	}

	@Ignore("Listo")
	@Test
	public void test() {
		HttpHeaders cabecera = new HttpHeaders();
		HttpEntity<String> entidad = new HttpEntity<String>(null, cabecera);
		
		//final ResponseEntity<Person> response = restTemplate.getForEntity(getBaseUrl() + "/person/{id}", Person.class, 1L);
//		ResponseEntity<String> respuesta = 
//				testRestTemplate.exchange(getRootUrl()+"/api/servicios/prueba",
//						  HttpMethod.GET,
//						  entidad,
//						  String.class
//						  ResponseEntity<String> respuesta = 
//							testRestTemplate.exchange(getRootUrl()+"/api/servicios/prueba/otros/{nombre}",
//									  HttpMethod.GET,
//									  entidad,
//									  String.class,"andy cusatti"
		
		ResponseEntity<String> respuesta = 
				testRestTemplate.exchange(getRootUrl()+"/api/servicios/pin/validar/{digito}",
						  HttpMethod.GET,
						  entidad,
						  String.class,"15196976"
		
		
									  
//				testRestTemplate.exchange(getRootUrl()+"/pin/validar/{numeroPin}",
//										  HttpMethod.GET,
//										  entidad,
//										  String.class,
//										  15485
				);
		
		
		System.out.println("xxx=>"+respuesta);
		
		assertNotNull(respuesta.getBody());
		//assertEquals(200, respuesta.getStatusCodeValue());
	}

	@Ignore("Listo")
	@Test
	public void solicitudAutenticacionNormal() throws URISyntaxException {
		
		URI uri = new URI(getRootUrl()+"/api/servicios/autenticacion");
		
		HttpHeaders cabecera = new HttpHeaders();
		cabecera.setContentType(MediaType.APPLICATION_JSON);
		
		FormatoLogin formato = 
				new FormatoLogin(
				"andyaacm@gmail.com", 
				"6fa97762927c7f7237c3f4b147ea934e2c0c9329d5e675144af5dbd11a2bb3a553e3edf6e7a056057d83df83a1be54bb6c67b38cbfc406834c4a9a17f0badbfd"
				);
		
		HttpEntity<FormatoLogin> request = new HttpEntity<>(formato, cabecera);
		
		ResponseEntity<String> respuesta = 
				testRestTemplate.postForEntity(
							uri, 
							formato, 
							String.class	
				);
		
		System.out.println(respuesta);
		assertEquals(HttpStatus.CREATED, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void solicitudAutenticacionUsuarioNoExiste() throws URISyntaxException {
		
		URI uri = new URI(getRootUrl()+"/api/servicios/autenticacion");
		
		HttpHeaders cabecera = new HttpHeaders();
		cabecera.setContentType(MediaType.APPLICATION_JSON);
		
		FormatoLogin formato = new FormatoLogin("ramon", "555");
		
		HttpEntity<FormatoLogin> request = new HttpEntity<>(formato, cabecera);
		
		ResponseEntity<String> respuesta = 
				testRestTemplate.postForEntity(
							uri, 
							formato, 
							String.class	
				);
		
		System.out.println(respuesta);
		assertEquals(HttpStatus.NOT_FOUND, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void solicitudAutenticacionUsuarioClaveInvalida() throws URISyntaxException {
		
		URI uri = new URI(getRootUrl()+"/api/servicios/autenticacion");
		
		HttpHeaders cabecera = new HttpHeaders();
		cabecera.setContentType(MediaType.APPLICATION_JSON);
		
		FormatoLogin formato = new FormatoLogin("andy", "859425");
		
		HttpEntity<FormatoLogin> request = new HttpEntity<>(formato, cabecera);
		
		ResponseEntity<String> respuesta = 
				testRestTemplate.postForEntity(
							uri, 
							formato, 
							String.class	
				);
		
		System.out.println(respuesta);
		assertEquals(HttpStatus.CONFLICT, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void solicitudAutenticacionUsuarioBodySinData() throws URISyntaxException {
		
		URI uri = new URI(getRootUrl()+"/api/servicios/autenticacion");
		
		HttpHeaders cabecera = new HttpHeaders();
		cabecera.setContentType(MediaType.APPLICATION_JSON);
		
		FormatoLogin formato = new FormatoLogin(null, null);
		
		HttpEntity<FormatoLogin> request = new HttpEntity<>(formato, cabecera);
		
		ResponseEntity<String> respuesta = 
				testRestTemplate.postForEntity(
							uri, 
							request, 
							String.class	
				);
		

		System.out.println(respuesta);
		assertEquals(HttpStatus.BAD_REQUEST, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void verificarNombreUsuarioNormal() throws URISyntaxException {
		
		HttpHeaders cabecera = new HttpHeaders();
		
		HttpEntity<String> request = new HttpEntity<>(null, cabecera);
		
		String nombreUsuario = "andy";
		
		ResponseEntity<String> respuesta = 
				testRestTemplate.exchange(
						  getRootUrl()+
						  "/api/servicios/autenticacion/verificar/usuario/{nombreUsuario}",
						  HttpMethod.GET,
						  request,
						  String.class,
						  nombreUsuario
						 );
		
		System.out.println(respuesta);
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void verificarNombreUsuarioUsuarioNoExiste() throws URISyntaxException {
		
		HttpHeaders cabecera = new HttpHeaders();
		
		HttpEntity<String> request = new HttpEntity<>(null, cabecera);
		
		String nombreUsuario = "rosa";
		
		ResponseEntity<String> respuesta = 
				testRestTemplate.exchange(
						  getRootUrl()+
						  "/api/servicios/autenticacion/verificar/usuario/{nombreUsuario}",
						  HttpMethod.GET,
						  request,
						  String.class,
						  nombreUsuario
						 );
		
		System.out.println(respuesta);
		assertEquals(HttpStatus.NOT_FOUND, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void verificarNombreUsuarioUsuarioNulo() throws URISyntaxException {
		
		HttpHeaders cabecera = new HttpHeaders();
		
		HttpEntity<String> request = new HttpEntity<>(null, cabecera);
		
		String nombreUsuario = null;
		
		ResponseEntity<String> respuesta = 
				testRestTemplate.exchange(
						  getRootUrl()+
						  "/api/servicios/autenticacion/verificar/usuario/{nombreUsuario}",
						  HttpMethod.GET,
						  request,
						  String.class,
						  nombreUsuario
						 );
		
		System.out.println(respuesta);
		assertEquals(HttpStatus.NOT_FOUND, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void solicitudNuevoPin() throws URISyntaxException {
		
		URI uri = new URI(getRootUrl()+"/api/servicios/pines/nuevo");
		
		HttpHeaders cabecera = new HttpHeaders();
		cabecera.setContentType(MediaType.APPLICATION_JSON);
		
		FormatoGenerarPin fgp = new FormatoGenerarPin();
		fgp.setCorreo("andyaacm@gmail.com");
		
		HttpEntity<FormatoGenerarPin> request = new HttpEntity<>(fgp, cabecera);
		
		ResponseEntity<String> respuesta = 
				testRestTemplate.postForEntity(
							uri, 
							request, 
							String.class	
				);
		

		System.out.println(respuesta);
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void verificarPin() throws URISyntaxException {
		
		HttpHeaders cabecera = new HttpHeaders();
		
		HttpEntity<String> request = new HttpEntity<>(null, cabecera);
		
		String numeroPin = "22648499";
	
		ResponseEntity<String> respuesta = 
				testRestTemplate.exchange(
						  getRootUrl()+
						  "/api/servicios/pines/buscar/{numeroPin}",
						  HttpMethod.GET,
						  request,
						  String.class,
						  numeroPin
						 );
		
		System.out.println(respuesta);
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void verificarCaducidadPin() throws URISyntaxException {
		
		HttpHeaders cabecera = new HttpHeaders();
		
		HttpEntity<String> request = new HttpEntity<>(null, cabecera);
		
		String numeroPin = "22648499";
	
		ResponseEntity<String> respuesta = 
				testRestTemplate.exchange(
						  getRootUrl()+
						  "/api/servicios/pines/buscar/{numeroPin}",
						  HttpMethod.GET,
						  request,
						  String.class,
						  numeroPin
						 );
		
		System.out.println(respuesta);
		assertEquals(HttpStatus.GONE, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void verificarPinNoExiste() throws URISyntaxException {
		
		HttpHeaders cabecera = new HttpHeaders();
		
		HttpEntity<String> request = new HttpEntity<>(null, cabecera);
		
		String numeroPin = "22648490";
	
		ResponseEntity<String> respuesta = 
				testRestTemplate.exchange(
						  getRootUrl()+
						  "/api/servicios/pines/buscar/{numeroPin}",
						  HttpMethod.GET,
						  request,
						  String.class,
						  numeroPin
						 );
		
		System.out.println(respuesta);
		assertEquals(HttpStatus.NOT_FOUND, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void verificarPinLongitudCorta() throws URISyntaxException {
		
		HttpHeaders cabecera = new HttpHeaders();
		
		HttpEntity<String> request = new HttpEntity<>(null, cabecera);
		
		String numeroPin = "2264849";
	
		ResponseEntity<String> respuesta = 
				testRestTemplate.exchange(
						  getRootUrl()+
						  "/api/servicios/pines/buscar/{numeroPin}",
						  HttpMethod.GET,
						  request,
						  String.class,
						  numeroPin
						 );
		
		System.out.println(respuesta);
		assertEquals(HttpStatus.CONFLICT, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void actualizarContraseña() throws URISyntaxException {
		
		URI uri = new URI(getRootUrl()+"/api/servicios/autenticacion/actualizar/clave");
		
		HttpHeaders cabecera = new HttpHeaders();
		cabecera.setContentType(MediaType.APPLICATION_JSON);
		
		FormatoNuevaClave formato = new FormatoNuevaClave();
		formato.setCodigoPin(85269741);
		formato.setContraseña("nuevaclave");
		formato.setUsuario("profesor");
	
		HttpEntity<FormatoNuevaClave> request = new HttpEntity<>(formato, cabecera);
		
		ResponseEntity<String> respuesta = 
				testRestTemplate.postForEntity(
							uri, 
							request, 
							String.class	
				);
		

		System.out.println(respuesta);
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void actualizarContraseñaUsuarioSinRegistroAutenticacion() throws URISyntaxException {
		
		URI uri = new URI(getRootUrl()+"/api/servicios/autenticacion/actualizar/clave");
		
		HttpHeaders cabecera = new HttpHeaders();
		cabecera.setContentType(MediaType.APPLICATION_JSON);
		
		FormatoNuevaClave formato = new FormatoNuevaClave();
		formato.setCodigoPin(12345678);
		formato.setContraseña("nuevaclaveTina");
		formato.setUsuario("tina");
	
		HttpEntity<FormatoNuevaClave> request = new HttpEntity<>(formato, cabecera);
		
		ResponseEntity<String> respuesta = 
				testRestTemplate.postForEntity(
							uri, 
							request, 
							String.class	
				);
		

		System.out.println(respuesta);
		assertEquals(HttpStatus.CONFLICT, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void actualizarContraseñaUsuarioPinErrado() throws URISyntaxException {
		
		URI uri = new URI(getRootUrl()+"/api/servicios/autenticacion/actualizar/clave");
		
		HttpHeaders cabecera = new HttpHeaders();
		cabecera.setContentType(MediaType.APPLICATION_JSON);
		
		FormatoNuevaClave formato = new FormatoNuevaClave();
		formato.setCodigoPin(12345677);
		formato.setContraseña("nuevaclaveTina");
		formato.setUsuario("tina");
	
		HttpEntity<FormatoNuevaClave> request = new HttpEntity<>(formato, cabecera);
		
		ResponseEntity<String> respuesta = 
				testRestTemplate.postForEntity(
							uri, 
							request, 
							String.class	
				);
		

		System.out.println(respuesta);
		assertEquals(HttpStatus.NOT_FOUND, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void crearNuevoUsuario() throws URISyntaxException {
		System.out.println("test nuevo usuario");
		URI uri = new URI(getRootUrl()+"/api/servicios/usuario/nuevo");
		
		HttpHeaders cabecera = new HttpHeaders();
		cabecera.setContentType(MediaType.APPLICATION_JSON);
		
		
		String imagenCodificada = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACA"
								+ "gwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAFicSURBVHja7H15nBxVvf25Vb13z75mZrJN9okhBAiEPcEEIYIPcQnwA/Upiw/Ehac+FBFFxC"
								+ "egQEARQfk8ASVh87G5hEcgrAGzQ8iezCQzmX3vvaru74/untT01HJr6ZnOTJU2menprl7qnvM933O/93sJpRSjfRBC4By5/5pVfjZyUJWfnSMHx1hg0"
								+ "ZUvH16SpAkETQJi26lINtAzN/zmN7/xTZo0yVtZWen1eDw8ACQSCc2X9ng8NP04sb29PX706NH4DTfcEJORAJWTAbVp1NLUySbMEOA4Lj+GYr4oAIcA"
								+ "DIOeZIGek/+8YcOGaXV1dbWBQGCy1+ut43k+KEkSYRycVBTFcDwePxKJRA4fPnz4yLnnntsIQJIRgJRFBtQKGTgEMDYKwCGA44AAsgDPZUV6DgD32mu"
								+ "vTZszZ86phYWFi91udwPP89Ucx5VyHFdiQekJkiR1S5LUI4piazKZ3Nnf3//B7t273z/vvPMOpUlAylIGkhlCcAjAIQCHAEbK+mygk5qaGn7p0qXeVat"
								+ "WVS5evPjcwsLCJR6PZzHP8xWEkEJCiD/HeWqUUtovimJHIpH4oL+//70PPvjgjTVr1rS//vrr8ZaWFlFBIVC9dMEhAIcAJjwByCL9sOh+5513Fpx77rl"
								+ "VkydP/kRRUdG5fr//HLfbPSsd2cfaUaUAhGQyuTcajW7o6+t74/Dhwx++8cYbbT/84Q8HlFSCEhE4BOAQwIQkAIVoz6Vv/L/+9a+51dXVnygtLf28z+d"
								+ "bRggJHQ8fj1I6GIvF1nd3dz/T2tr64SmnnLILgKhABkOqwCEAhwAmHAHIIn4mr+cB8C0tLZcUFhZ+yufzncnzfP3x/FFFUTwQi8Xe7u/v/0dNTc1f00Q"
								+ "wLE0YkgQOATgEMEEuvlzm82kCcDU3N68qLS1d5fV6z8p1Lj8GqiAaj8ff6u7uXlNbW7sGgJAmgAwZUEmSqEMAo/w+4ByjCnwudeW5NPDdS5cuDTQ1NV0"
								+ "Wi8X+PmnSpAd9Pt+K8Qb+NOn7fT7fikmTJj0Yi8X+3tTUdNnSpUsDANwZEkwfTpWYowDGddTPgJ/fsmXLqbNnz/7PQCDwaYy9mTfqoiASiby8Z8+eXy1"
								+ "atOh9WWogTQQ14KQAE4QAFOQ+//Qzz0xdtnTpl0tLS79JCAlM5AhEKY10d3evXv/66//zhc9/vjHLIxi3ROAQwAQggKyo7wLg2rNnzwVTpkz5gcfjOck"
								+ "RoMeORCKxuamp6RezZ8/+e9ofEMazGnA8gAmQ78uA77766quLuru776yvr/+DA/6Rh8fjOam+vv4P3d3dd1599dVFaW/AlfYGHF/AUQDHba7veuGFF2Y"
								+ "uX778Xp/Pt8wZcvpHLBZb/+qrr37nM5/5zL60Ehh33oCTAoxDApCBn0/f3I2NjV+qqam5hef5SQ602Q9RFI+2tLT8fOrUqX8CkJR7A+OBBJwUYHxLftc"
								+ "tt9xS1t7efvvkyZMfdMBv/OB5ftLkyZMfbG9vv/2WW24py3yvTkrgKIC8l/wffPDBqfPmzfthIBBY7gwx60ckEnn1448/vnPx4sXvj5eUwEkBxgEBKLn"
								+ "8O3fu/NSsWbMeGIuoT4b+M+wHGw+a+f+YpAR79+69saGh4R8YB7MEDgEc5wSgAH73rl27Pjtr1qzfEkKCo/ddDgd7XJAQTkpoGUziYG8cm49G8X8H+3G"
								+ "oN5l+WAbBdAjT6dE37LzTSjz45LRCnDQpgBklXlSH3Ah6eHhd3DBCGM3hQykN79279/q5c+c+n/YFjlsScAjgOCYABfB79u7de3l9ff29o1XGK/8Ojww"
								+ "kcKRfwN7uGJ7b3YeX9/cjmZTkX+7wKD70D83u6SMfGfKRCbeLw6dnl+DSucWYVeZDXZEHdYXeUR+8lNLogQMHvj1r1qynACSOVxJwCOA4JQAl8O/bt+/"
								+ "K6dOn3zMa4M98dwlRwquHwtjaGsFfdvbiwyPhFGh5DuDJcECPuMYsKoCOvF+kgCACIvCJaQW4fH4pTqwJYvmMInh4btQGMaU0cvDgwe/NnDnzieOVBBw"
								+ "COA4JQAn8O3fu/Ozs2bNXcxxXOBpSv6k/gUe2dmNbexQv7hsAwknAy48EPeTgVUjchx6X9TdKdX5OP16QgFgSCHpw8ZwSLKwO4JpTqjCl2Dsqg1mSpP4"
								+ "9e/Z8s6Gh4fnjkQQcAjjOCEAJ/Nu3b7+woaHhdxzHFefke5L9p3Uwidvebsfrhwaxpy0KSBTwcABHskCsBmgNoCtFe9X7s84jSUBcBAjB7EkBLK0vxk/"
								+ "Pq0N1gWfoeTR3JNC7c+fOr59wwgl/O95IwCGA45MAMgU+nvfee++ck08++X94ni/L5XfUMpDEne91Ys3OHnRGhJQMd3MKUZ0Oz+t1VYBB0lAjBrmPIIg"
								+ "ACMqDLqw6sQI/PLcONYWenA5uURS7Nm3a9OUlS5ZsSJOACEB0COA4IwBRzF8C4HlOPs/v+fa3v13+i1/84jmv17soV1K/Oybi+T39+O7rreiNpBfHcVA"
								+ "HrWEVYEU1aJwn4xVQiuKAG/dcOA2fnV+GUr8LuZo1iMfjW37wgx9cet9993XKSEASxfwlAZ53COC4IIA0+Ela9rtqamoCO3bsuLekpORKW40tAFz6e3m"
								+ "1MYy73m3Huv2DqWhPVKK4lrnHAlw1A9BQGqChRCQACREr5pbg+8smY/mMVKYkUWp7lUJPT88TCxYs+E5LS0sEx+oEaL6SgEMAxw8BZDr4uAF49+8/8B9T"
								+ "p079KSHEba8kJIgJEn72bifue78LkUgS8PHKIB76x4y5Z0EFGPINZH+PCggEXPj2OXW49bzJ8Lk42K3QKaXJxsbG22bMqH8IQBypOgFJFPMzt3QIYGQul"
								+ "2cXiB9h+r3yyiuLV6xY8TzHcQV2Gn2E49DYl8SXXmnGhsZw6g8uouLoZ4FMHsVViYJFBVhIA/T8AaT9AQqcM7MEf/rCbEwt8YJKkq0GoSRJA+vWrfvsyp"
								+ "UrP8g2BUVRpHk2vvKCAJzFQPr45AG4br/99klnn332z+wEP0cICEfw8sEBnP6XQynw8yQFfpZ3piSkDZUCMz6GGPzKlB7v4gCew4a93Tj9d1vx8q4uEI7"
								+ "AznU9HMcVnH322T+7/fbbJ6VJm8fEa7XmKACbGHqovh+Ar729/e6ysrKv2in5KSW4d0s3btnQjlhCUp7LZ1EBtpiBjOmEUR9A6XUECT6vCz//1DR858xa"
								+ "EEJtTQm6urr+WFlZ+T0AsYwfIOZZjukogDw+0uAfcv23bNlyqf3gB378Tge+u74dMUFii/pmIjQxdqpROVwcYgkB333pAH78z0ND34ldR1lZ2Ve3bNlyK"
								+ "QBP+hpyvFLS7RwOAWjk/jwA9yOPPDJ97ty5t9inflI1PNf/XxvueKcjxfrZc8JyhZStloZtJKRHCEYkPrGXeFTZJf0vz4FSijtebcR/PL8XEiWKytDsMX"
								+ "fu3FseeeSR6TjWdpykr61zOATABH4XAPfKlSuv8Xg80+wCPyEE177ait9t7k3V7XNEH1C6wDALxlyBnfGlOAJwwO/easG1z+1Ofz/2XEuPxzNt5cqV1+B"
								+ "Yb0GHBBwCYBquQ9N+77777vkVFRVX2PJFEwIKgu9u6MQftvelJD+xAnozOCZj8HUyvCeewx82HsV3XzkACjJUD2H1qKiouOLdd989P00CmevqEIBDAJrR"
								+ "nwPgWrZsWbChoeHrPM8X2SL7QXD3ph7ct7lXAxuMkZbkY2JvdRQS3LfhCO7ecAQSYEs6wPN8UUNDw3XLli0LplUA56gAhwC0kJKp+PM88MADXwyFQufYc"
								+ "lLC4a/7B3HH212pgqdhfhTJAUZzJNlzeRKOQBQk3PH3g/jrR50ghNgiWEKh0DkPPPDAF5EyBF2y6+wc+UQAdAxv2bn/9ddfXz59+vRrbYECR7CvN47vrO"
								+ "/AYFI21UeIPniMRnoyBsC1kzhcPAZjAr7z3F7s64yAEFuGJ5k+ffq1119/fXm2FzCWY84hgPzM/V0A3Ndee+1FXp9vrtWLzBFgICHhyn+0o6k3mVq+O14"
								+ "idq7SDC+Ppu4ornxqFwbiQto7sXYdvD7f3GuvvfYiyDYbcVRAvhHAGFGxKyv3P/fccwtmzpr1NXtoheDW93qwsTkKeEgOqH+cjmGvCxsP9OLWfxyyzbic"
								+ "OWvWV88999wCOQG4eJ5MdAngKIBj3wMPwHXfffdd4vP6GqxeYJ7j8PLBCB7e0af+LVsZ3GScBzCe4PfvtuDlj7vAc5xlwPm8vvn33XffJbI0wBn7+fUlj"
								+ "D4Nu1wjFvy46+vrr7B6XkIoOqMCbn63G7GYNP7BmpORSRCNJnHz3w6gM5xIf4XWrkvq2g5PA1JjYOJKAIcFZdF/7dq1J/l8vjkWVT84jsPvdvTh49aYdo"
								+ "mvldpvSsf/lXFz+PhwPx56rxkcx1nmUZ/PN2ft2rUnOSrAIYBs848H4F68ePHFPM9bWu3HcRw2Ho3h11v7Ieac7cc5CRACkQL3vdmM95v6wHG8tayC5ws"
								+ "WL158MY6VB094M3DCEoDL5Rq2pdcll1xSVFpaeoaVAcGRVGOTx3cPoKc3eWzKjwXIdCw5gI7x8zUOD4/uzgj+tLkNoihZrRIkpaWlZ1xyySVFcgJIjwWHA"
								+ "Cay/P/Wt761xOfzzbA0ujiC7V0JPLytN+X6m5LtFphgLAVBLonI78LDbzdje+sgiMVVgz6fb8a3vvWtJU4a4BDAsA09a2pqTuJ5PmjlZJIIPPLhAIQkg45"
								+ "gIYNRz/PzIKWgyqmAkBTxyMZWiJJkSbPzPB+sqanJ9gEcBTBB5T8BwJ9++unB4uLi+Za+SAIcGhDw8Ef9gJuMBDJL003WKK5LDAptv463lCL7FG4ev9/Yj"
								+ "MaeuOXeAcXFxfNPP/30II51DJqwacBEVgBDTT9WrVpVFwwGZ8IiA/zyXz2QEtIxwGcDXwm81IpCMIqr41tRiEkRv3y9yXK8DgaDM1etWlWHY/s8cBMZBHlxjPIMrLz2n581e/ZUr883zcqMbkygeOFAePimmroApGw5vKGInwtgjqVJSYclWi981IVYkgLEUmnw1BkzZ07NSgGIsxZg4pEfD4CfNnXqAisn4nkOz+4Noy+9RZYp8LMCnoUM8jLvtyEt4IC+aBLP7egAb21KkNRPn75ARgCOAphgEiDD+tzSpUsLysrKTzB7rlTAJ3j84z5Eo8IxeaqVBii28zYKeMqANWoAm6OlCiwQASGIhpN4fFNr6lcLY6CsrPyEpUuXFsh8AM5ZCzABGMDtHj7/f8opp5QUFhYsNHs+ngcO9sSxr1dIRX9FwMtBQ4cPAsq4O++w3yk7EPU291QVJTS3mLYAgr1dMRzsCqc31zB33QoLCxaecsopJfI0IDU2nFLg8X4MI4CFCxfWut3uKvNn4/DMvgEc7I6rFP7ILjpVut8K6KjF6M/4GKP+glllwPI8N4+DrYN4ZnuHpTUWbre7auHChbXZPoCTAkys/J+bP3/+IitMAgAHegVICVFZkyqSPtXHH1P0B1v0tysSG5pFyFFhEgGkuIgD3bFh18DMkb72HCbwTMCEVgA+n89dXV19snnzDxiICTg0kEwVAugqvSxSQJZfwAo0avAPauRhWpLnWBXoqRie4FBPDAPRBKy096uurj7Z5/O5HQUwQQ632y0vAOKmTp3qLS4uPtk8lXD4V3sMG5sjOht7qADfalSlJgDHkiJQU2jNXeI/oiiIw/sHerHpyABgoW1YcXHxyVOnTvXKCSA9RhwCGMfRf2j677rrrpvqdrvLrZxwf28SPT3JY1t4K95UgK8k81Xvo8ppxAjQq4GfKj5FG5hqG4BajPTUgDJReh5P0N0Zxb7OmKWg7Xa7y6+77rpMPcCELAueyCkAv2jRorlmw1LGf+qNSca+RbVZAr37qBbQTBQR5broyNRUpIHHE4LeWBKAZMULpOkxwE/UFMCVL2+Eji4BcAC4isrKemqSBDkACUHC4UGB7QyUWrifaoNf94ukFtMFu8jAxhHBExzpSyAhSHARApMbjXPlFRXTkbVpCJ1ABDBhFIAnK/8HwBcUFEwzzSKEoC8uYVd3Qkf+U+vg14uOan83WkRk9H2OBhmoTXfyBLvaw+iLCpY6BRUWFEzPkv/EM4F8gLxRAKNEu3IC4ELB0Eyzr0sAdEZEfHA0mrXRhwU1oAkqypAKGP1uTcwhUpsuJrW4StHF4f3GPnQOJlER8phe6FRQUDgzSwGQ0ZakDgGM3jc+BP7y8nK32+0qhQUGGEyK6OlLAF7OhlJ3ajDH1yAHqjVVQIdHVT2TjykVYTEerTxG4fEcQU9PDINxQVZ/YQIALr60vLzc3dnZmbV34MRggIk4C0AAcCeddFLI6ucXJTt4j+qDX5MsqXburyXTzfgCLMqBMnxmlmDAsEZBtL7EmZxyyilBTNBaANcEA/8QCdTX1weIxR0oRVu7+qpIfJb7tGr4reCDWikcyqVBKL8GFgcFIdyUKVMCWeAnE0UCTCQCkCsAUllZGSSEWPr8CclgvKA60ZxF9g97nBF332r0tzhbADsIY+RjEiK1uMCQuKqrq0PyseEogAlAAOXl5UFCiPlF5RSIC5LO6j8YADyDnNcjCEqhbxZSA94DS56u8nymOgM9haGTahCCmGAtDyOE8OXl5UGHACYYEfj9fq/Viy0MDVKalWVoDHBqlBSMgF899TcekI3OAlDtz6bnKehOSCg/QBRpXowFhwCOHw8AANDR0REGIFo5m0++qxQxAngDUT9b8isCmkFVKKYBRqM/64ezWE+g+6dMeTWFz23ZxxbTY0F1rDgEMArHaDsuTU1NA4Ioii632/Q5hgjA8AegDB/cBPhhoO2YmeivSnBmzAE9T4Ht/fpcZKg/oCkVJ4rigQMHBsZ6PDoKYJSPbdu2DVJKzSsAELg4AJCgP5to1Jmn+rN/uhHc5GwBZe02RNWjsyE0UUs9DVwWW4RTSsVt27YNTFQcTKRKwGGvsGnTpigoBCvdblP4J7DW/MKEKagLUqoDdhVW0JxC1Co8UrrfiKE5goU0yGu40cgRYq3LFoWwbdu22Ih7nUrAcX3QRCIhiaIQtzJy3BxSW4AZnQ40C3wz4Afj/gJGW5ZTg+qGGlzHwJJ6ePjUNbDAAKIoxBOJhISJtQYoHwmAjtaLDN2SyaR56UcpAm4OlSE32geTjP3pLKzco9REmkBNEIhu3sFYdQgTpqKBdEGiqCzyIODhLQ2d9BhQ6OPklAKP2+if+be/v7/JAv5R5uexeJIPECQwdYJVbRem1jjUSN5MoVsibCS/Zy0aYlIFVlqbqxyChMWTC1AWcINaqMhMjwFTVq5DAMcv+CUA0pEjR3aZvegipSjx8Tix0p8qCDDVCTor4KhF/mGdcxgrBDXBz6oKKGNNgdXob8IvECScWFOAkqDLypoMmh4DUvo24UiAm2Dgp7KLLW3dunUnpdTU8JEowLs41Ib41C8jMwwFBaDSKkytXdiwXv5UWTmo7TOgFflVW33pRX6Nqj+W6K9oKmqlGBpkJEmoLfKC53lIJhUApVTaunXrTvmYQP7t3uUQQK6I4IknntgviGLM/NYOFKU+PtUQlErW94JQWhloqECHtdUYS0rACkytlICh16CeklDkHgq4eZQGXCy0q3oTRDH2xBNP7J+IwM87AhilfViGqYADBw6EBwYGGk2/aYliTpkX9ZV+IGm24y3VBj7rMl+98uARUZt1yk8j8hs1/0xla0rOnYj6mhDmVAZSxGvyGBgYaDxw4EBYKfo7m4OOsyMWj9NsAohEIkJXV9cus+cUJWBGsQcN5R72dam6ewZSfc+AwmCFIIPppwc8avB5dkR/tbRCoGioDmJmmd/SWoCurq6PI5GIkE0A6bHiKIBxJQHo0KtkLrSYSCSEXR/v2mr2fIJEUeDjMSXkTvkAlJroDcg6+6TmAZgAv1aXHz01oAdeU4U/BlKBtPKaUuxFyO+BIFLT42HXx7u2JhIJAak1IcfGhrM56Lj3AKT0RRffeeftXZIkCeZJi+K8aSEUFbnTLYJYpgNVjEDVqcHsKjw6clNRFg9BbUaB6hDMCKePQnOGgGlWQadqUM1vECQUlfpw3owS030AUx6iJLz11pu7MuNAFhgcD2AcS4DsmQBx9+7dXQMD/YfMnlMQRFw8M4TZZV4L04EaMl8JgKyFQqqgZFH+lHFtEWU3Fi0VEKXPLUiYXRXExfPLIAiC6bEwMNB/aO/evd0y8EtQLdhwdgcerwpA2Lx5c29nZ+dHVnwAj4fDnBJvan9Ay+QH7Xl+lh2GoDA1CFaTT0/CU3P9BXSLlFgMRwA8wZwKPzweHqJkHkjt7e0fbd68uReA4CiAiWIExmIjFMDRo0fjTU1NuywxiijhmhNLEQzwxlp+KxUCqW0hzhT19fJ9lSSeGgC85qYkjP0FYJQw0ueWKIIBF645rQbUYkfWw4cPf3z06NF4tgJIjxGHAMa5AhhGAvv37z8kSVLS7AmTAsWSWj8qAy6NEl3KCHqtNMAm8FOT4Dfl+mupBqoR/ZW5qjLkxpKphUhaaAUmSVJy//79hxSiv6MAxrMDkAW7jPkjbNu27cjA4GCT2XNKAFwc8O3FZbKNPFVMPkVLQuU5SlJf1eg71iVHudW4SsWg0mPUDECqkpJQA9V8WuDPNiSzD0Lx7XOmwMURSBbGQP/AQNP27dubZeAX5Z9kAk0CTEgFABnjiwCEtU8/3dzT03PAygkFieKK+cWoLvTISoP1KI5hVGg2HaUMI4qqpxVglPZaUToXfoES+UkU1UU+XHFSJQTJGoR6e3sPrH366WaZAsjw+IQ7JhwBRIf7ACIAoa+3N3Ho4KGtoObngKkElHp53HJmZbpfuA7Y9YCrB3wluU+ZVu7o1/jrpglg2IWIWn8N+ZEQcMvyaSj1u0AlS0Y9PXTw0Na+3t5EdgoQnWD5f34RAMVo5gHDPAAAwmuvvbY5Go12WHn7hBCcPy2EmjIvkJTM6T5W4Ouu9GOsGNTcf5ChzRgDl2jv9EP1zT9BQm1VEOfPLgEBsSSho9Fox2uvvbY5C/yjVwDkFALlFeUM+QB33fXLPb29vfutnDAhSJhd7sMNJ5cBcckY4M0Cn7KsC6DqMwyaS4h1wG94zp+RKLLPH03ihjPrMLsygIRgWf7vv+uuX+6REYA4Ec2/PCSAUaXfEQoAgLB37573KZVE09oSFBKVcFlDMeZPCQJxEcrLgFkBr2besa4j0OgfQNUMOJkhmO0ZUDXi0akY1DP+lN5D5vxxAfNnlmDVwgpIlIJasP8olcS9e/e8n7neIxTABJQAE1IBRKPRET4AgORTTz21wVKbMACJpIT6ci++d0Yl/D4+s3uIMfJjLvhhAD5Lz0CWXYSYioqohqo3solpxqqV4Pe78L3zpqC+MoBEUrR03ZPJ5MBTTz21AUByRP6fGhOOAphgKYCUAT+A5GOPPdbY3t6+1eqJ43ERVy4swfmzC1VWCbK2CoOJWQAto4/q1/pTdnwyzRZopRR6LcsFivMbynDlKdWIxwXLF7y9vX3rY4891pi53ulrP2EbgjoEMDwNSAJIPvvss2tMLw6SY1aguG95LWpKPSMXCempQSOegC4hqJGC3hQdY1sx1o1KtQwApedJFDUVftz3b7NABQlWdwGXJEl49tln18jAn70GwCEARwUgcfPNN29pbml512qGl5QophS6cevZk441C2GZ79cDPZOhx6ASWBqA6FUVMs0WsE5Fyg4CICHg1uXTMKXYi6Rk3aBvbml59+abb94CIOFE/zwkADrKt8gxH0CUKYAEgPjatWufslIaPOQHiBKuWVSKr5xaAUSEkUBn7RNAVVYHUo1egUzLiFUeo2v46VQMDhFGNvGpmI/DSIYCkSS+cvZkXHPaJCRE6/U5kiQl165d+xSAuIwAhmYAItEoHe3x5xDAWDMAHfaqGSMwASBx3733bm8+0vy+1fPT9KC/Z3kNLmwoAWIC+5dgun04a+MQjTp/1iXEakk8U22QyrqFqICVC6twz0UzUndL1q9z85Hmjffde+/2zPXF8ApAOkZjzyGAPDmGzQQASHR2dkZefvmlZ616AQCQECjKAjx+saIWc2uC6SpBBjMQGnm8Xrsw6OT6LODXzeVVZL6VtmMJEXMnF+LOi2egLOhCQrAl+gsvv/zSc52dnZEs+Z/xACb0MaEJIBIZMR2YTMvE2O233/7BwYMH19vxOtGEhIXVPtx9fi1CIR4QRDYzUG9qUE22qxIC1c7XWVuJa073Kf1drxlJqtovFPLg7s/MwMLaIKIJe7B58ODB9bfffvsHAGLpays3AGl6DDgE4KiAoeKQBIB4b29v5I9//MNT0Wikxw7NF4mJuGhOIe4/fzIIRzSaiLIqAjXwahGCyuOUzEjogJkZ/Ap/zwa/REF4gvsvmYGLPlGGSFSAHd95NBrp+eMf//BUb29vRJb/CzIDcMIfE7QSUAbMSIRmkcCQCrj33nt3btiw4XFJkkSrn44QIBoV8dWTy/D7z0yDmycKS4epfo8ARfNPjTQUTETVxUkMnYWg8ZqKxJNlGCr1MZQo3C6C339xDr66pAbRiMC2zaK+9Bc3bNjw+L333rszK/oPgT917SkwgU0ARwEcG7LDpgPTgyZ21VVXPd/S0rLJllchQCwu4upTyvE/n52GgJtjqBRkMQUNqgCAfTGR4u/Z56H6kV5p0CclBLw8/ueKebj6jBrE4oKJXZaVj5aWlk1XXXXV85nrCGf6T/HIm92BRSm3iowo/JT5cTAcpqFgkCiogPjg4GD0x7fd9pvVq1dPDQaDVXYwTSwu4PKFpfC6OFzzQiO6+xNI73NtwJQzss8e1c6/oZcqQLs8WNfnU3j9hIDSYh8e+eIcXHpCOWIxAZJNkAyHw20/vu223wwODkYz1zE7+g+Gw3RoSzGF9zpR2MFRACPDqZilBJI7tm/vbGxs3GTXC0kUiMREXDq/GI9fOh0zqvxALClLCVjSAB2loAd+rf4BukuI9R6j4SFQANEkZtSE8PgVc3HpCeWI2Ah+AGhsbNy0Y/v2Tvk1RPbUn3M4BKAxrIdKhO+66+4ZL7388o8aGhpW2v1i4aiIlbMK8fTn6/GZhRVAVEwNUStpAFRMPtbOQYbKg/VKjLP+LlEgmsRnFlfj6avmY+W8UoSjgu0XsaGhYeVLL7/8o7vuunsGhpf8wgF/ljKmdPS/D6Lg8gwMDo5ZCgAA6RSAS6dFHgD+zZs3XzNt2vTrvV5vTS4ZJ+jmEBYo7nm7FXe+eRSJuJjecFSLo6C/OajujkEGQK12DsriGaTyfY+Xww8/ORXfXToZQRdBOCHZYvipHfF4vOXQoYO/Pemkkx4BEMWxWQBpMBymWiSXa1QUhEIjr8QYYHHCK4BQMEhCwSCX/i54AK4bbrih5OjR1vtmz57z01yCP8NBkaQELwfctnQSXr5iFhZMCmQVDBlQAUrR3lD/AKhPFbJO7SmAf0FdAV7+2gLcdv5UeLnUZ84l+AHA6/XWzJ4956dHj7bed8MNN5SkyZ0HwIWCQS5N+o4CmKgKIBQKkvQdGfC7N258f9ns2bN/5vF4Fo7a9wHA4yLgeQ5JQcIT27tx09+a0BtJAjzREK4Mu+owOfwa97F4Alp1A6KE4oAbv/63GbhyUSXcbg6iQJEQpFHV4olEYtuePXtuPe20U9cjqxhocDBMJ6oCmLAEkAV+FwDXjh07Lp06depdPM9XjRbw/R4O4Aj2d8ax9WgEj2/vxv/u7Eq/TaJjVRgFPkverpZmsBYXQbHQBwT4twXluOqkKpxYE8KMCj9AKaJxcdSIQBTFtsbGxu8vWLDgOQwvCJKRgEMA454AZODn0zfP7t27r6mtrf0ZIcSfy/eR+baDbg5wcXh9Xz9eO9CPl/b0Y8v+3tR0oJszB3ozwNfM97V8BUbwy5+YkICkiEWzSnHRvFKcN6sYS+eUAkkR4aQELcqz7funNNrc3HzrnDlzHkl7AkOrAlMk4BDAuCaAUCiUifocAFdRUZF3+44dt5aWln6TEJLzugieAD4Pj42Hw3hwYxvW7etHW3s0BXoPpz70tPoI6NGNEbnPEvVZ1MawnoJZPyYEICGhqiqIFXNK8Y2zanHa1ELEEiLEURiPlFKhu7t79QkLFvysr68vLlMC0uDgIHUIYJwSQBr8JCP5AXiaW1ruLioqujq3nxdwkVQ7633dCdz2WjPeauxHW3csxQguTl8y2AV83YgPDeOP0TvQAr/8PSQlQJRQVe7HWfXF+OkF0zGzzAdCUhut5Hpo9vX1PVpbU/M9DF8jQAcHB6kzCzDODhn4eQCuL3zhiwXNLS0P5hr8Xp7A5+LQ2JfA9S824tTf7cCzOzrR1p8AvLwy+PX6AxptH85SJMQCfrXXMgN+IKV6fDzaemN4dlMrTv3V+7j+mT1o7InD5+LhdeV2eBYVFV3d3NLy4Be+8MUCHJshIOmxMiGOCaEACrIi/ze+cWPJj370o9WhUOjSXEb9gNeF1v4E1nzYjdvfaEZ3bxxw88MTXaamwQaa7DPJfSO5PmO+r5Tz6yqIrJ4CFEBSRGmRDz8+fzouW1SFqmIPIjEhp2pgcHDwuTvuuOObDz74QI9cCQyk0wEnBTiOCUAF/L8NhUKfyZXJ53cR8ByHNR914+GN7Vj/cXcq2nOMgcXQFuMwsCbAZNSnDIRiFfzyuyUKxJL45IIqXHNmLVYtqoIoSojmsHZgcHDwhTvuuOP60SIBhwBGgQAKhht+7jPOOCP4zDPP3ldYWHhFrj5b0MPhQE8CP13fjL9s6UAyLqbAbwu1gK03gJ6RYDjis0R9g7MF2e9D6bUiSbj9Lly+uAa3XVCP+jIfwnExZ+Oyv7//z5///Oe+/c4774SRqhWQAEi5IAGHAHJMAAXDc343AE9TU9PPSktLv5GLz+TiCAiAdQcHcf0Lh9DUHklPMnLmwQ69VIGy/10NhGblPiv4WaYYFV8//a8oAQLFlKogfvvFeVgxuzjdQCg347a7u/vBKVOm3IrhzUNtVwIOAeSQALLA7wLg3bdv33erq6t/CJunmgkAv5tDW1jEr94+ivvfaUUyKR2r4LMV8CaivWngs+b6BiW/EfBn7qIARBFuN49vnTsV/7l8CqpCHkQTOSkioq2trXfMnDnz10gtIxZyQQIOAeSIAGTg5zKR/6OdO6+dPHnyHYQQj52fgycEfg+HD5rDuPF/D2Fj4wDgJgocQw3j33i0NxDxmfJ6ylhoZDTfZ/EhVIqK0ibhafUleODyeVg8pRDRhAjRZjVAKU0cPnz4R/MbGn4vUwKSnSTgTAPmkF9wrLzXvXnLlkvr6upuywX4fW4OT2zrwsWP7cLG/b1p8MvDllr7LTD2+2PpD6jwB6oT9fX6ArJNTYCp+YjW76zgz/xCAHg4bNzTiYsf3IQnPmiFz82B5+x1Bgkhnrq6uts2b9lyaTqIuNJjatxND44rApCZfjwA9yOPPjqzvr7+vzmOC9o7QACOI/jBuiO49tkDaOuLA34XA8gZNhJQ6g2o+XyM7O2nuQ2Z2mYjGhuNDKs/0GhDptd7ULV9GYbXNyj2FJQ9KeBGW08M1z7xIX7wwj5whNg+O8BxXLC+vv6/H3n00ZlpEuABcAXjrEZg3GwMku34r1q1qujTKz99B8/xlXb2cnRzBAkRuHLtfvxy/RFEE0KqhJdlbxnDfSJ1HqDUoFM1uBtpH46RoNRMQ1jyfQM5v5YKyZzKyyMaT+KXf9+PK/9nBxIChZsjtvbt5Dm+8tMrP33HqlWritIkwA2RwDjZGCR/PIAB8x5AQcFwxz8UCvm3bdv+o8rKym/b+b4Dbg5HBgT8v6f24q19aclva+gx6hUYnC2grPeDEfgGzD4z4GdZk0BTi4zOmluGJ7+6EHVFXkQS9k4Vtre337dw4Ql3pHsMHpsZGDDvBxQUOB5ArvJ+z5tvvnW5neAnAEIeDgd6k7jsiV14a093KuqbBr+BsEDVIr1Wbq8Q3Zl3EtYAP/SUg0Hwq91Fwd6hiBDAy+GtnR247OEtONAVQ8jrsjVZr6ys/Pabb751OVKdosaVH3DcE0A6+g9183n88Scapk2b9gM7X8Pn5vBRewyXPbkL7+7vBfx87vQf1QK9vv/HtIuwWclvqHOQWmRnNPzUpYmyOvC78O6uDlz2yCZ81BqGz23v0J42bdoPHn/8iQbIugqlx55DAPaIX+P/k4GfA+C+6aabqpavWP5z3sXXUNjzPzdPsPloBJ/7825sOtiXMvts2LBSfZdgAzMIiiYi9HcRVjX4AO2ZByWTMPs9Ujazj9XwG2Feqr02BUJubNrbg8/9fhM2HxmAmyewaxzwLr5m+YrlP7/pppuqhvkBBSFi5nyOB5B19A8MGDpHYUFBdqWfb8/evT+urq7+lm2R38Vhc2sUVz61B/taBm0q6bVuBRiqAGReTmyySEjNuGPO9zV8BLNrCeJJzKwrwhP/fiJOmlyAWNI+T6C1tfX+2bNm3Y7UZiNDfkD/wAA1OH4dD8Cm988DcK979dWzKysrv2bXiYMeHof6Erju6b3Y1zwIePjcgd1oXYBeTYDeuajO3LyprkEMkn80wA8KeF3Y19SP6x7fjkM9MQRtJO7Kysqv/XPdurMgmxp0UoBRPtLRf0j6f+PGG6sWLlz4M47jQnacP+Dm0BkRcfWavdjW2H8s8o/aFnE6U4haRoAW6BVzfKptIjL1ENDwCfTyfSNko+URZD/Mx2Pbvi5c/dg2dIYFBGwicI7jQgsXLvzZN268cVgqkB6Tx92RPylAP3sKUFhYkIn8HgC+PXv23mqX9HfzBAkJ+PyTu7FuawcQTBf45OryUpMP1G0RQE2cw8YlxGYkP2BhFaGKKhhM4PzFtXj62pPg4YCkaM94b21tvX/27Fk/w7F9B8X+/gHJwBh2UgBT0b+wYFi137p1ry6rqqq61h5iAiQJ+Nqz+7BuRwcQcA3f0mpUN4nVq+jTifTqoVkd5Cz7DWjuFZAr8IMN/ErnC3rwz01H8bXHt6WaE9tE5FVVVdeuW/fqMnkqkB6bTgowWuC/6qqrShcuXPgTuzr5el0cfv7GETyzpT0t+0erbIuaB7yqvNcAPlMloJIvoDUtaBf49XwIlrUFWT/7eDyzsRl3/n2/bW3GCCH+hQsX/uSyyy4vOZ5JgDvOwD9EAKFQyHPrrT++0efzzbfj/AU+F17a1YsH32xWSK1zvFe82VJRTdBTzbv0awVYJT/Uc33WfJ8l56caaYCqMkj/QlLy7oFXD+ClHR0o8NnT/Nnn882//fbbvxkKhTwyQ5AcTyRwvKUAQ9H/t799aEF5ebnlzj6UUoS8HHZ1RPHdF/ajf1AY2borx/g38GaNRXvA4C7Cap2DtCQ/1SAjhsiv+jjKbvjp9hdIjZz+gTi+u+ZD7G6NIOR32ZJzl5eXX/Hb3z60AMfprMBxUwgki/6uCy64IHTGmWd8hXfxlVYLPLxuDm3hJK55di8OHA0fW9gzBoaManGQWqEOS8HQiJWGUFldiJFFSkrnHfEcOjLqK606HEEYeo/Lej+Kz8HI96n0OpmfvTwOHOnH1U9sRWt/HF43Z0eBUOUZZ57xlQsuuCAEWZnw8VIIdFywVVFh4bDc/5prr11YXl6+yvKHJwSCRPHA20fx1s6uVN5PKTsgjRCF6fMw1P9rTg/q/E0v2muZfHpyn0nyw2BLMj1VoOMrBFx4a3srfvPaIQgSBWeDK1heXr7qmmuvXShXAekx6ygAm8HvuvDCCwtOPfXU7xBCfFbP7eEJ3j88iF+92phe3GNSm9tFEvqo1lT7+nUDWt6AXiWgxhbh0MvhWZch64GdwfDTIoQhJeDCPS/txvuH+uCxwRQkhPhOPfXU71x44YXy/QWOCxI4XvKVoZLf733/+8sKCwuX2/HBe2MibvzrfsQlqtKy2+5F3SYNAaZIr1PQo9kujNHdzwa/0mO1ioE0gW4A7Cw5v5aBSAjikoQb/7IDvZGkLSAoLCxc/r3vf18+LegoAKvYkEX/oRZf8+bOu8Gy6U6BkNeFe99sxkfNgwDHmcR3Dtw+o41CVB+rt2xYT+6rqByt1EBrio+1wIcJ7AbAr5gWUIDn8FFTH+599SBCfjeoDQu85s2ddwOGtxBLqYA8bgiS7wpg2KYef//7Pz7l9/sXWj1p0MvjlV3d+PUbh42baqMyI2CgXZgWayitPNRrFab2mqoRnyqfQ3clIcuKPyUzj8Xwy1olCLVUh+LX/9iLv33YhqDPeqmw3+9f+Pe//+NTOLb3JMl3JZC3BFBUNJQ/ZVp7e+bNm7fKanNPF0cQSUi46/UjSITTU36G+vflUPIYqgtgMAYNTQ2qRHuttIB1as9IyzDVFMBgbYJexSAFwBEkwgn88u/7EElIcFlsLkoI8cybN28VjjUO4bPGskMAJt4fD8D14osvnl1QUHC61RMGfC489kEb3trdndqc0qiytzPUW1ogBB1pTxn4QsHcowxpgd4CIdZuPhT6aQjVATq01IOeOQnAw+Ot7W147O0mBPxuywO2oKDg9BdffPFsuRnoKACmeKg678+XlZV5Z8+ZcxHHcyXW5vwJDnZF8ZfNbRBFMf3pLXbsGY11AIbahen7f7rmHqBdQAQVNcA6FaiJT5ZOwhoGnx7JZP9KCERJwl/eO4yDHWHLtQEcz5XMnjPnorKyMq+MAIhTB2DgKC4qGjb1d9fddzeUlpZ+ypo8S134tds6sPHjTsDjMrYcN2cXzeRMgOForxHxKUM3Yc3af4XX0VppqJnPQ2cWgcJYeTBDauB1YeP2Nqz9oOXYWLFwlJaWfuqXd901T64C0mPaUQCM4B9GAAsXLjzb7XZXWc39D/bEsXr94VRbL5Ze/Eb7+tu1DkDLvGN5LpORqJKGqLUYU4v4as+BkilIVUw6HfOPeZ8BlfOOuE/hnAEXVv9tDw52RuDircHC7XZXnXjiiedkpQEkH0kgX/OTobLfK6+6qrSiomKZ1ROKFHj0vVa0dEU1tum20NPfdBpgoSaA2RikyhLfiJzXrDOAMbnPYuzpblrEOOfPqhR4gpbOQTz6ZpMtW41VVFQsu/Kqq0qR512E868OIMv8++R5n5xRECo41QrICIDeiIiH321OtfYypfDHYCWQ7sOpPuh1gQ8Yqhw05QtoRG+wmIZ6YGcuC1Q3GSkArxsPv3YAvWEhhVZLG9UUnPrJ8z45Y4QZ6NQBMEV/AoD3+/3uRYsWreA4zmeFVwJuDg+/dxSDsaSs3HeMU3/Tr6unEDRAz9winHGXIaaoz3IfZQSuzgpBvSZIWmoh/fzBWBIPv3EIAY/L0qXnOM63aNGiFX6/X14Z6KQAmvl/cZG80y+/ZMmSUHV11QVWzskToD8u4U8fHAUEyhY9Ryv4m1IeUI/0TNFez1nXA5WVzkEM8/t6LcA0eYDqG35aqQKhgCDhT+80YiAmGNvhXeGorq66YMmSJaHMeAZA0mPcIQCVISkv/eUvu+zy2R6vb5YVbBX43Xjw7Ra090RTbMDcjz9fbtBfZMRiBiqZnXq1CYrPV5EVWvsCaN2nal5qPJfFNKR6VYQqKYmLoL0zggf/7wAKAh5LV87j9c267LLLZ8tSgPSUoEMAeikAD8B9yuLFnySEmK7R5DmC7oiIv+3sQjwqqKeIaiAbdRY0sJKQ1RfQUwFWo72piM+wSIiypgCsqQODKkgf8UgCr+xoQ3ckaWnrcUIIf8rixZ9EHi8QyhsCKCkeOf1XWVlxtpXoWeDl8NJH7djc1De8r7/RBptWl/racR7m0mBWctCbDaAMJcNa8lzHiQd09g8wWDEI6NcB6JYHp3/2ubD5YDde3tKCAp+1beBSY3j4dGBJHqUB+aYAhvL/m2++eXIgEJhn5UQSBTYfGUSiP55V9Wez+WdbLwAj74V13wCNqG1mGlDN3dcy+VhzfWYQa5EJ9GU/NMAPpNYI9ESxqbE31UnYwoAOBALzbr755slyH8BRAOqYHVr6u+L888+w0vTD5+bw4dEI/rEzvZOvZpGMBfPPDnCbKg+G8elBluagZuW+oQIeA0ShNQ3ICna935UOL49/bG/Dh0cG4HObXylICPGtOP/8MyBbIpxPJJCPBMADcNXV1p1FQHiz6stFOGxrGcSug5lFP1oFPWYMQRMzBKxPUFQSMFBEZMQU1HhvSt+RqtnGCnydakBV80/tu2c1DdVIRqkUmgIeHrt2d2Lb4T64eGKhBoXwdbV1Z2WnAQ4BjAT/EAHU19d7Q6HQHNPmHyHojiTxz13dgIsYTabtA7LZPgPMb8qEMWhbxyAjKsCA3Gep5zeTKii+Z5334+bwzx2t6B5MgrewQCAUCs2pr6/3ZhEAcQhARQHcdNN/znO73WVW3P/WgQSe+qDl2KIfK8AyI+UtpQ02twpjXRykBXAYvN/MRiOqMwGspb0M036s4KcAfDyeevsQWvvi4C0UBbjd7rKbbvrPeY4CYMv/ufnz5y9wuVzFZk8mgeJwTwJCQjQc/PURbtdh4jUMKQWzpqDBxqFGGooa8QrA8HrUpnwf6mpCSEo43B2FleUBLpereP78+QvkY9xRAOopgKuisrIBhLjNBlhBpHhmW5u28w/YGJ1zIAPMpgZmpgCp3WmA1vdupuRXgxSMLgfOjv56VYUcwTMfHIEgSuavNCHuisrKBgxfGOQQgFL+f9ppS4KhUGiGlRMmJeCv29uzmn2yGH3IqwJANoOQ0RhUMj6ZDD+q8TwwVPJlnU/vPj1lQlnNP41qSq3zZr9/jsNfNzUjaXGFYCgUmnHaaUuC+eYD5F0KcPnll03zej2TzSKHI0BjTwyDccFE9B8N6W9AUdixQEjLgQe0m4voNQExvZMwZYjsDKkBSwqglO8rSgR1xTIYS6KxK5JeRW5uXHq9nsmXX37ZNCcF0FEAM2bOnOH1emvMnszDE/x1ezuSoiT7ii103cn1BqF21QSo/kmvPTgL+BgWCDH5AoylwcylwCbzfYOdg5KChL/+q9nSJiJer7dmxsyZMxwFoG0A8pWVldOtvC+Pi8NLH7an8gArTTZzlfrnYkaAMkR6MBADk+HHYjgaqQRkMCmZm3zo5PusJqG8RwAAJEW8tKUFHmudgrj02ObzSQXkFQHU1dV5Q8FQvZXGH5EERTQp6fTAHyvnP0czAqbSAJhIAzQeY4QQdN+HhmdoRB1QneXBusRy7LNGkyLCCXHk7nEGbqFgqL6urs7rEMBw8A+RwEUXXVRWUFBg2gB0cxz2d0YRjqXlv5p8Z620y2UvQEPmI8PyZdZ+glpmH1g+v4aZqNonEDrVfGrnMdJTEMpmn5pJqFVVSLPIgxCE4yIOdkTgtpAGhEKh+osuuqgsC/xkIhMA5PJ/xsxZ5X6/f7rpPMvF4b3GXnSF4zjW0wkW5v7zwPW3QwVQIypA506qQQisKsCoL8C64tCGfF+5UQhBV38M7+3tgtcCAQQCgfoZM2eVZ6UBE14BDJmAtbW11RzPF5rFkMvN4a193YgNxGWNP80666OJfrP9ARnNQEOLg2DcG4Ce429yGTFLxWB25GfK9xXOp/X5OCDWH8Obe9rhcptengKO5wtra2urs0zACa8AhmYAJk+ePNvKSSQJ6IuKgAT2ZbTA2KX+RjjHkhmoH9itTQMyFAlpAdhq/0BqomJQ8TW0PYO+iABJopYQW1dXNwt5VBKcLwqAq6iodBcVFU0z/UEI0BdNIpwQAY5lySyD7M6nAiDmiQyLm4Uwm4J6aQBskPwMU3zURMVg9uOoZm4wdITjAvqiAjgLXYKKi4unVVRUupEnU4GufAA/AG7atKlen89bbTb08hyHjoEk+iJC6rQUI7uvUTUPUunuMezcRg3/gfEhVOduvY1EKfvzNOW6BRXACnxqEvhq5+GAvmgCXQNxVBd5TO8d4PN5J02bNtXb0dHOZZHAmAy4vFEAhYWFHp/PZ3r3HxdPcLQvhq5w4tjeTmajv21z+CYtBbMbhdi5i7AhFWAg2hve+Yc1188V+NPvgSPo6o/jaG8k1R/A5OHz+aoKCws9CgqATDQCGGYABgIBj8fjqTZNABxBY3cUbX0x5U9l+yYgsNnpN2kGGnmA1QpBVuCzVgJqKhPGqG+oa5AB8Gf/zBG09UbR2BGGizMPG4/HU11UVOTJFyNwLFOAYSZgTU1twO1yl5gVQi6O4HB3FLH+GOB3Z11Iwi6VmS9FjhUbtfHBNIdpgBm5b4fkh5ZJCE0zj+k7pCNNplhvDE2dEbg4Yvryu13uklAo5EGelAK78gH8AMgJJyyotnYmkqoAFCSFr5WyI50yvONRB7hVH4CaIwbK8Jq5Bj6r0cfSOYgyAF3rvYgS4oJkeftgQohS9B8THyAfCAAASGlpWZmVk1CJpowZohQtFC6Y2e0ZqJ2kTW18GrVGCNTouahNZMAY7Y0QB/OGoSy+AbWd/0VRzJs6gLyZBfD6vH4r9CemdnViRABRvCsvpD/zy9kg+1mjfE6Ab+a7pPrfAQv4mc9FtbMNe4LfhE4Bhr4Mj9vjs/JVSjTVCUjRZ2JJCfQiOxltgBscatTA86hNRMJU5msA+IYjv8YbZt0/QPU+IyamsUOSaF5E/7zyANweCwQAQJQoBElS2P2XNdrrAWHMGGD00gCrEd+M3LdD8mffZSg10Jf95hWL2seledMUdKxTAJkCcAfMn4hCpBQJgSoAlUH+M0X447AwiBp8/1ZMQbNy31bwU7aZAMPgz6ftPMcHAWQXPxC3x+0z+0UTQiBIGYdWXihiwPyjY52qUZueYiZlsDI1yCj/tQDPJPU1cnQj78Us8BWXV4+y6hunHgCxBTtEHgHsjP55crGoDe/NEOjNRnobon0u5L4pyZ/997zb3HdcEAAAIB6LR82nwxRuwsHn4nRAoYJ+y0VBoyEC7PAKLMwGmFUDRqO+loJgdfhZc33WyK90fuoQgK1HNBqJWMGNmyepZg2SXBFYRN2YeDXU5qdbmA2wWihEDaoMIyTDmuvDaNSn+u/fUQD2H+FwZNDK83mOg8cl6wOWDV4yBmAcE77Ig7JgXblvNNdnjeYG1ITel6MIfmnc+YGufBnC4XA4bKU6lpDUpqCG5n+1WIHkE6gNEpIpFUAtEIZJlUENkAyr3DeV6zNE/XTfSGpDaKAOAYz4LmhfX9+glZNwBOA5AFQyUOWnkfwbvUrEzqtNLT48xyrAisxnPrfNEV93CTEDuY3DjMA1xsAf+r2ruytq5YTHyqoU1lQo5fK2Ov8kNwYe86moPQRhqEJQL+qb8QxkPzB3FzIBfjOfcYgnqEMAOSAD2nr0aD8sMgCnWlxpoPQ3X7wCq4Ri2P1nPW8OIr4h196A5Lcq+7OJhVBwHBwCsBP4mVtzc3NEEIRBl8sVMnMyQZRQW+yDO+RBUqSyrsAwVvrL6vyTXAA7z9IAQwDR810YInkugW9V9ksS3IV+1JYGUiXnJg9BEAYjkYjgEEDWJenu6UlEo5HmgoKCOWZOlBAkzKkOYVKpH01HBwAPrzH2tFICmhN82q8EbFQBpsBuBfCMoLcV+AwbhlKN5iICxaSqIObUFCIhiKYHQDQaae7s6IgjT6oJuDwAvwRA6mhvj0cikRazJ0uKEupK/Kgq8KWWBhrtBZirPoDMGgg56A9oZO9Dqp/Xm9lFWBGIGjv0qL4ezPUKVCvpZYn68kOiqC70oa4sgKRgfiBEIpGWtra2BPLEUsybWYAjR44kuru6mqoqzfUFFUWKygIvSgJ8aoOATDWQ5RkBkynCaMgIS2sBbFIQevPs2ecwe06rct+Q5FfqBkRRHHSjssCPRFIwfel6unsaDx8+nMDY70Ax5gog801LAKREIiE2NR3eY/ZEEgVCPg5BD6/dgNNyZB/VrYG1X9JwQ1GqH+n1oj2gvVOQlipg3rCEcS2+btSn+uqGMmweQgFQCUEvj5CPh2RhJuDQoUN7EomEqPDljgkR5IsJKAEQDx06eFiUJIHjOFPvKylIaKgpwAt+F0Rq1PzLheM3SkrAaGQ3lbMbjPQs+b2ZaG9HxM8Gvp6yoBR8wIOGumIkRcn01ZIkSWhsPHQkE/TyQQXkw6TGkArYvXt3ZyQSaTJ7omhCwPL5lSgt9AKipLCjbi6T9VFQAloRnfUzGt4+jHGT0uxIn4v24XZFfMrQNYgOl5elRX4sP6EG0YR5Az8SiTTt3r27U4EAJqwJSGVfhrh+/fr2np7uvWZPmBApTqgrRJHfncoJ7ABMvhxW3rup7cOM7EycDXwwAt/Ajr92Rn3NtEJBxYgURX4XTphahIRgfgqwp6d77/r169sBiLJxj7EkgrzxAACIR44cibW1tu21ghG/i2BaqZ9dtSsBa6zIwa73Yssuwgb8AZbnMwGfMeqrAZgl6kMn11faZIQA0yqC8Ls5S8OirbVt75EjR2JZBDDhTcBhJNDc0nxIkiTTOisuSPjSmVPBu3nrKlsLkLm42ZWd2LWLsJ4yMCT1bZD7akrBaPoDA5uMUArew+NLy2YinjQf/SVJEppbmg8pgH9CE4BcBYgAhG3bth2MRqNHzZ4sKUo4Z04Z3DxJLQwailJKN4zenL8tdoMSabBuW6b1HLXXlz1e87WVnq/wWop/UyFctXOy5PqK11mjfoFmf2/DzXm3i8M58yqRFM0TQDQaPbpt27aDAAQZCUzoQqARKQAA4c9PPtk4MNB/xIqpFvJyuPjESanNAqhZtI2C92d4/0CbZL9RaW/EEwCjvDYU7Q1EfGpkybGampEdIsXFp9Qh5OMtXdiBgf4jf37yycYsApjwKUA2CUjd3d3xvt7eRisn5AnwlbOnpGYC1Ob+rYVijNmsAHNNgFmfwAZPgBX4WuaeUnqiW63IaCoa6RokiPjK0pngLc4E9/X2NnZ3d8flY91RAMOLIKQMOzY3t+yWRClhFj+UUswqD+AT00vT2wUZLP3NB0o0rBAMnlfr+zDrCRgFvq6zb8DdpyzlwlDuEqxGCEkRn5hZiVnVQfVsi+EmiVKiublld0blZoHfUQBZrCi88cbrOxKJhOnlwYJEUVPsxbVLpwORpEZBUB5Jf0PbiNswG2Bm3YCeccg0DWhQ7htx96Hx2kY7B1EKRJK49vxZqCmxtgIwkUj0v/HG6ztk4HcUgMKlFzO3hx566GA4HD5i5YyUAmfNLsG8WeVATGQyu/MD/SYlvJJJCBUzjDK8HmU19NTOqWLI6Zp7DLMjShFf1VhUMg81DMXMeRMC5s2twllzK1JNQCxANRwOH3nooYcOysd4vujNfCIAKpNIyZajR7dbgU80KWJBbSFWnVEHiGK6MMiu9QD5kAowqhez/oLeLIEeo2oadVCfx2eV+tAzFg3K/ezXSUpYdfZ0LJhSjGhCtETlLUePbgOQlBmA1CGA4cewmQAAwjtvv/2mKIpxK9gJxwVcefoULJpbCcQF/Rx6FKp3bZ0RsNsMtKNYKBfAN2rwWV1GHEti0fxqXLm0HuF40tLlFkUx/s7bb7+VGdcYWQXoEIASCdx664+2DvT3H7RywrggobbYiy+fNQXwcKllwobX3VNth9u0smd1NE2oADtADxOgNwp8PXNPNeobyPM1fQGlZb8i4HHhy8tmorbEb6n4BwAG+vsP3nrrj7bmI/jziQCy6wGSAIQDBw+8afXE/dEkrj53GlaeWKM+I2CoIYhNOX6u6wJYzUCjDj8LiFiBb1Tu60l+GJD8VOX9CBJWnjoFVy+fif5IwvLATo9hIT2m82b+Px8JQO4BCAASa55as04QhLDVk8fiAn7+hfmorAjqdIhlmmO0qezXhnyDtWJQN32g2kaekpmnBlBFUxBsZc/Zf1erAMw2+KhKtaGWIalENBJFZUUBfn7FIsTi1tv2CYIQXvPUmnUAElkpAHUIQF0FDBmBTz75xKHDhw9vsHpikVLMrAzgnstPAASaJyuxbfAOwJgG6CoZmJP2LKYga6Q3Gu0tNw5ReE1Rwj1fXoyZVSGINiwGa2pqeuPJJ584lGUASvk06rg8HOKZgqBkMpmMv/jiC3+1sjjomK8j4pJFk/DDz80HYglZ27AcGn52GoOWegMaNAcNdQ0yCXxFGW4E+CalvqL7RIFoAj+8/CRcclodYknr0V+SJOGll17832QyGXcIwJgRKMpypsTaNWt2Hz7c9KbVnFuSKARRwDeWT8cVy2emZgUkRvk95oU/RtcBwDiLGYn2qqW/lB34VKupqJqHZ2Ipsd5rSxSIJnHlBfNw44WzIQgiJMn6RT18uOnNtWvW7E7L/wwB5BX484oA2trbRygAAIm9e/f2/e2VV54VBCFm9TWSAkXQzeEnl8zFeSfXAXExjwqDLCgFU4VE1AToLUR7VSnOaEpa7hykcJ8kAbEkzjt9Gn78xRMQ8HBICtYNekEQYn975ZVn9+7d26dEAOmx7hCAgosr9wGS6S8vvnr16m2NhxpftwNj0YSESUVerL7yBCw9qQZICOpLbUe9ANCsccgIcs3qOjBU9xnoY6D7fapU7rGYe4aWEmvclxSxdPEUrP73UzCp2IdoXLKF8xsPNb6+evXqbQDiMgI4Jv/zSAPkVQrQ1taebQQmAMS7urrCd99992ORSKTd6msQAoRjIqaU+fHbL5+IC06tO0YCYxb9bTAbLPcHZJwBYH0vmhKeZfrRQrTXAz6lQELEBUum4bfXLsGU8gDCMQHEht6vkUik/e67736sq6srLCOAofw/PcYdAmCYDRBkKiD2/PPPNW18//01EqXUMswIEEmImFTsw8NfWYRvXjQPfFJM1wnk+2yAxW5CdlX1GXHzjbYPZzH3WHP87McKEnhBwrc/txAPX7cEk0p8iCTEoS0krNwkSunG999f8/zzzzUBiOV7/p+vBICsNCCevsUuW/XFp5uamt6060XiSREBD4+ff2E+fvW1U1DsdwOx5Ng3C7WzbZihIiBqLtrruvks237Z0UdA57GxJIr9bvzq62fiZ5ctRMDLI54UbbtsTU1Nb1626otPp8GfGbdy+Q+HAHTGaesxiSRXATEAUQCxX91z98N2pAKZQ5QoBuNJfP286Xj0+tPQUF8GDMbTZcN0bG5Wv0RD/QHNqAi9aM8Q8a00CKUGHX9KgcE4GmZV4tHvnIuvf2oWBuNJiJJ9RB+JRNp/dc/dD8vHalb0R2tbO8230pN8VQDy0uChNABAdO3atQefeebpBwRBiNj1YgRAz2ACK0+oxOPXn4avXzI/9Q7S0nBMGdGW/oAmyYay9lJkXTOhNcVqwn/QquyjNHXt0v7O1z+3EI9/62ysXDQJPQMxWy+rIAiRZ555+oG1a9celIE/I//zqvR3xNinYyB3iYLb0to2PKhXV1WSNEG5AHgB+AGEABQAKHjxpZevW7x48f+z+735PTwkCry1uwu/+N+P8c6O1tROwxwZIxYwkT5YTT+Y3k6udxGmxt8jzVrvHxdwxol1+MHnF+CsuZXgCEU0Idp+lT744IMnL77o0w8DGEjfBtNEEM8ogNYs86+6qlLh7VOHALK+JC5NAm4APgCBDAksOOGE6j//+S93VlRULLD7/fEcgcfFYTAm4om3m3DPy7vQ0RNN9Rh0cxhpF5PcgDnXYLcb9ADjaj2dc1FqjhDSU3vgeVSWBvC9SxbginOmIeR1ISGItkr+zNHR0bHjiisu/+GO7dtbZeCPyFIAqbWtXVIY23lBAPmaAmSnAnJDMAogumP79s77779vdTgcbrP7RUUpFSncLoKrl07Dm7d9El+/YDbmTilOdRoOJ7IG5Chv/2XHxiWsNQFGFyUx9Qg02i6ModYgnABEirlTSvH1lQ3Y8PML8dXz6uHmCaIJISfgD4fDbffff9/qHdu3d2bGpYLxl9erTfJaASikAp50KhBMpwKF//3Lu5ZeddVVP+J53pszliRAgd+Nlt44/vj6Abz5URs27GhLKQKfC+A5Y8AbdRq1aStw09HeZqmf+Vu6jBc8h3MW1uDsEybhq+fNQk2JDwPRpKVdfHWDhCjGH3/88Ttu/q/vvw6gPx39w2kSSKhJfycFUCUA9UBeXVXFZfkBARkJFLzw4ovXnHrqqVfm+n27OILikAdNXTG8sqUF7+7pwjMbDyPeGwF8bsDF5Se4jQDSLDlYkfdG8nsgVa8RTcJbGsDnT5+G0+eWY+XJdZhSFkBvOAFBzP2M2/vvv//EZy6++BFZ3h9OS39Z3t8maYxphwBYCUBGAnzaD/DK/YDq6urSP//lL99oaGi4eDTev5snCPrc6BxIYF/rIA50hPH4m4ewfkdrauyTYR/WnpkEM/sDWvEgDG8LTs1/Bq1OQvJuvOnvdtkJNbhq6QzUV4Uwc1IBygs9CEcFSzv3GDl27tz54hWXX/5ga2trd1bePyT/tcDvEIA5AsikAnw6FZCbgqHi4uLi5557/lsNDQ0rR+tzZMxCiQL9kQR6oiLW7+zEY6/vQ0tnGFGJIhxJpowpQlI7lmSnC3Z8/9QG05H1HEaJxWjEF6WUzyJJgJtHMOCBnyOoqQjh3z85C8s+UYWSgAuFAQ84AiQEKSf5vQb4X7n00s/e39vb25sGvtz0S2Ry/9a2NuoQgP0EAAU/YIgEysrKitesWXvD/PnzP00IGdV5O46QoWDPcQQDcQnbD/fiuY2H8dHhXvRFRRzuCiPSE00Nbi5NBm7GKUZq+o/2qATDswEMaYAopeR8BvQcQaDEj8nlIRT5XZg/tQSXLpmGE6YWo8DLQZLo0AyfNMrjllJKP/roo5dXrfrib7q6urLBPyzvT49nhwDsJAAZCRAtEigoKChcvfqBzyxdunSV3+8vwxgcNG0cujgOfg8HcDwaOyN4e3cH/rW/G+FYEj1RAUc6I9hxuBdiTzT1LI4cIwYXp2Au5kItjALoM0AX0u3ZpVQxEV8SwIKpJagrC6DE70bQ78IpM8px5rxKTC0PApAQTYgQRAkSHbuarGg02vX666+v+eY3b3xhYGCgXwf8VA/8DgGYJAAFEnDLSCAzOxACEPzmN7+18NrrrruurKxsNvLgcPMEPjcPn4eHKAG9EQHN3RHsPNKH1r4YogkRvZEkjvZEcbB9EB8f6Udv50DmG0t94iGJQVI/ZwhDE5/UPkJQAnsG0JLC5iPphxRXBDFvcgmmV4YwqdiP4qAbfg+P6mIfGiYXo7Y0gOKgBzxJdW6KJQQkxfyYPevq6trz+4cffnj16vu3pY2+QRxz+zPgTxoBv0MAFgggiwR4HJsZ8MlIIAAgsGLFiil3/PzO/5gyZcoS5NGRSRNcPIHXxYPnCASJIi5IiMQF9EWS6BlMYCAmICml9jfojwrojSTQ1Z9AW28Uzd0RHOoYRFNHWNbEwug2WayEQUZGeErhdvOYUhHCtMoQaksDqCr2o6zAi+KgG4U+N4I+F9wcUOB3oSTkRVHAjYDXBa+Lg4vnIIoS4oIIQaRD8j6fjqampvd+dMsPH1q3bl1TOtpHZODPLPgZavTJCn6HACwSQBYJuLKMwUxKEAAQqKioKHrgwd984cwzz/y8y+XyI0+PoQBPCDhCwHMEHEfSwZRCojSVKksUoiRBECmSopSe8iKgJB2IJQpRokiKFAlBgiClwCWKNLUnlZg6l0Tp0HSZi+fAEZLKPngOPACeT70PF58yOt38sffEDVEChYvn4OY5uNJ/5zkOPJfxRFK+SOY9SZSCUgqLO23l/BAEIfr2228/c+M3bni6o6OjTwb+CIbX+mf6VhgCv0MANhCAihLIlAz75CQAIPCTn/z09Msvv/yawqKiWhynB5EFY5L+gZCxfU/Hyu+P1eFTHL9Hf19f81/+8pdHfvKT297NAn7G6Zev8hPNgN8hAJsIQMMYVFID/uXLV0z+r//6r1WfWLDgQjiHc2QdH+7Y8be77vrlU+vWrTuSjvRKUd+w4ecQQA4JQIEEeJk56JUZhIHMz/evXr1i+fIVny0tLa13hr1zdHd3H3j11XXPf+ub31wnM/ciGF7fP6K3n1nwOwRgMwHIvtRMybBcDcgNwqHbOeecU/udm25aedJJJ6/0eDwFDgwm3hGPx/u3bNn8t3t//etXNmzY0CwDfBTDjT551Jf0qvwcAhgjAgAFqquHKgY5mS/gkXkDciLwrVp12Yzvff/7V1dXV8/ned7jwGL8H6IoJlpbWz+8886fP/Lcs88exLEuPnLgK/X0k1pb26gdBQkOAeSWAJDlCyh5A/J/vff86tfLli1b9qmKiooZbrc76MBk/B3JZDLc2dmx77XX1v/zu/950/p0dJe38FLK9Yc19HQIIM8JYOgLrh7mC8jVgHymIPvm+elPbz/ttCWnnTJ79pwz/H5/sQOb4/+IRqO9e/bsfmfjexv/ddttP96IYy3mlG7J7KgPgLa2yvJ9hwDynwAyP04anhLwWWmBVwZ+r+zm+fp//MfcpUuXnXTiiQvPKSoqnuLA6Pg7+vp6m7Zu3bbh9dfXb/7dQw/tSgM/LrvFsvL87D38pKNp4A8bsQ4BHD8EICOBbCJwZxHBMALI/Hvu0qVVJ598ct3y5cuXzJ49+7RgMFjhQCt/j3A43LFnz56Nr7766nubNm068sbrr7fJgJ/IAn9CBny5wy8BoEdlUd8hgOOYAGQkIPcGeJk/4JZ5BJ5sEgDg5nneU1tbG7zqS1+av2LFirNra2vne73egMvl8hNCeAd6o39QSkVBEKLxeDzS3Nz80bp16958/E9/+qi5uTksimIG2HLQJ7JuSVmeP6yFlxz8DgGMAwJQUANKRJBNBpnmI27Z7+7MY6+++uqZZ511dsOsWbMaioqKqkOhUJnX6y10oJm7Ix6P9w8ODnb19fW17t27d+dbb72589FHH90nA3JSBvxMH8mkCuizgU+zge8QwDgjAB0iyFYFrizgy9MGOWnw06dPD37hC1+Y3dAwf3pNTU1tRUXF5JKSklqv11vkwNYS4Pt6enqaOzo6Dre0tDTv3PnRwaeffnrPwYMHwzhWi5+5yeW8nAgEhWjPBHyHAMYpAWgQgRIZZJPCMCWgcONPPPHE4tNOWzJpxoz6qhkzZ02rq6ubWVlZWe8oBP0I397efuDIkSP79u/be2j//gNtGze+d3Tr1q29CoAXFCK/oBDls0HPDHyHAMY5AagQQbYq4DVIIbMa0S37Oftv/Ny5cwtmzJhZVFtbWzT/E/Mn19fPmFZVVTWlqqpqhsvlCqQ7GJEJgnNKKaWCIETa2tr2t7W1NR04sP/QRx9+dLi5ublv//59fbt27RqQgTYbyILMtBM0wC4qRXsjwHcIYIIQgAIRQEEVZP/rwvBiIyXwDyOCzI3neVcwGHIVFRV6TzzxxNKTTzmlbvLkyRUV5RVVJaWlFUVFRRV+v7+I53k3z/MuAC6O43iO47h8NR0ppaKUOkQAgiiKgiiKyWg02tfX19fR093d0dHZ0Xb48OGOTf/615GtW7d29/X1x8PhQUEURSXgZoNe6XcpK5fP/lfem98w8B0CmGAEoKMK5IQgJwMttaD2r/w5nMqNLFt2Xvm8eXPLKiorC0tKSguKi4pCwVAoGAqFCgKBQIHP5wvxPM/zHMeDEBfP8zwhJH0X4QnhOAAcSR+Zz5Faq0+49MCT0uOAZqJzehd2iVJJkiQqiqIoUpr6F5QKoiSJoiiKsVhsMBKJDAwODg6EBwfDvX19gz093QMd7e39H3+8q2v9+tc6MXyvx+ybPELLwa30r1JUzwb7sEhvBfQOAUxwApCfbVJ1dbYqgByoCoSgRgpKNyWFIT8vCQQCLp/PxweDQZfL7eYD/gDv9Xr4QCDgLiws8pSWlvpKSkv8oWDIEwgEfMFg0BcIBHwut8vl4l08l6IEnud4QgjhOJ7n0soCoigKkihKlFJJlEQqipIoiaIoiIIoJAUhEonEwuFwLBKJxAbDg4me7p5od3d3rL+/LxGJRJLxeEKMRCOikEyK4XBYiMViYiQSEWTApwpgFbMIQO8mKZxDfl5g2DReq61dBB0CyDEBHGcHyfqX01AJWopBjTQy5+OziIBTUSTIuk/pPbIOX6ryu/xf+f5eIyJuViTOAJaqRGslyS5qKIbs11F6j8erMTLqr+lyfGdLWkMOApJFCNnEMELe6/w+TAEonEspPVEiAi0yYP18SsCnKsCnCpGfqgBZ63dJAehS1ms7h0MAeUUK2cARVYDK6dzHaZAIdMAPRiVglASojhpQUwVy8EoqZKF1H0WuMjvncAggxwpB/rMaSInGfSyPUQN5rlIAMBCBGjnoPWbcyHmHAJyDRVZrgVcrqoNR5hOb37teegAGtQAH6A4BOAcbEIjF3+0gAZbNBoz+7hwOATiHCfAZBRLJ0ftwjnFy/P8BALfjQdkbSqOwAAAAAElFTkSuQmCC";		

		LocalDateTime ldt = LocalDateTime.of(1990, 4, 20, 7, 5);
		Instant instant = ldt.toInstant(ZoneOffset.UTC);
		Date date = Date.from(instant);
		
		
		FormatoNuevoUsuario formato = new FormatoNuevoUsuario();
		formato.setImagenB64(imagenCodificada);
		formato.setPrimerNombre("Rosa");
		formato.setSegundoNombre("Flores");
		formato.setCorreo("rosaflores@gmail.com");
		formato.setClave("rosabella");
		formato.setTelefono("02335863252");
		formato.setFechaNacimiento(date);
		formato.setCuentaPaypal("paypalRosa@gmail.com");
		formato.setNumeroPin(12345678);
		
		
		HttpEntity<FormatoNuevoUsuario> request = new HttpEntity<>(formato, cabecera);
		
		ResponseEntity<String> respuesta = 
				testRestTemplate.postForEntity(
							uri, 
							request, 
							String.class	
				);
		

		System.out.println(respuesta);
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void listaGanadores() throws URISyntaxException {
		System.out.println("test listado ganadores");
		
		HttpHeaders cabecera = new HttpHeaders();
		
		HttpEntity<String> request = new HttpEntity<>(null, cabecera);
		
		int pagina = 0;
		int cantidad = 10;
		String token = "5ba7a09c4b093ce04fa6654150f8e177a0700320c4264c28b8a7fc6890f1d76ce3ba1044d91390b9791c8e50bc6d6d79ac7730098b5f5ff2260b658e279549c5";
	
		ResponseEntity<String> respuesta = 
				testRestTemplate.exchange(
						  getRootUrl()+
						  "/api/servicios/ganadores/recientes/{pagina}/{cantidad}/{token}",
						  HttpMethod.GET,
						  request,
						  String.class,
						  pagina, cantidad, token
						 );
		
		System.out.println(respuesta);
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void buscarCuentaPaypal() throws URISyntaxException {
		System.out.println("test buscar paypal");
		
		HttpHeaders cabecera = new HttpHeaders();
		
		HttpEntity<String> request = new HttpEntity<>(null, cabecera);
		
		String tokenUsuario = "ff55004c6023aa66dc4523e4f13da5169ea271e2b72443f4a32baae0638f234a633e18fc8efe09b3030e4e5221a49a648ecc4feea21d8f6ecf71ae6f65327eb8";
		String token = "bac2761186d340a601f57e99f50e2c6aa313f889aef265896cbade8dc7ab683f1a16b4d5ecc719bae25c66112757ecb9be76af76e45fc519b7a126d822c2aff3";
	
		ResponseEntity<String> respuesta = 
				testRestTemplate.exchange(
						  getRootUrl()+
						  "/api/servicios/paypal/usuario/{tokenUsuario}/{token}",
						  HttpMethod.GET,
						  request,
						  String.class,
						  tokenUsuario, token
						 );
		
		System.out.println(respuesta);
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void actualizarPaypal() throws URISyntaxException {
		System.out.println("test paypal actualizar");
		
		String tokenUsuario = "ff55004c6023aa66dc4523e4f13da5169ea271e2b72443f4a32baae0638f234a633e18fc8efe09b3030e4e5221a49a648ecc4feea21d8f6ecf71ae6f65327eb8";
		String token = "5ba7a09c4b093ce04fa6654150f8e177a0700320c4264c28b8a7fc6890f1d76ce3ba1044d91390b9791c8e50bc6d6d79ac7730098b5f5ff2260b658e279549c5";
	
		URI uri = new URI(getRootUrl()+"/api/servicios/paypal/actualizar");
		
		HttpHeaders cabecera = new HttpHeaders();
		cabecera.setContentType(MediaType.APPLICATION_JSON);
		
		FormatoPaypal formato = new FormatoPaypal();
		formato.setCuentaPaypal("actualizarpaypal@gmail.com");
		formato.setTokenUsuario(tokenUsuario);
		formato.setToken(token);
		
		HttpEntity<FormatoPaypal> request = new HttpEntity<>(formato, cabecera);
		Map<String, String> mapeo = new HashMap<>();
		
		
		
		ResponseEntity<String> respuesta =
					testRestTemplate.exchange(uri,
											  HttpMethod.PUT, 
											  request,
											  String.class
										 );
				

		System.out.println(respuesta);
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void buscarInformacionBasicaUsuario() throws URISyntaxException {
		System.out.println("test buscar informacion basica usuario");
		
		HttpHeaders cabecera = new HttpHeaders();
		
		HttpEntity<String> request = new HttpEntity<>(null, cabecera);
		
		String tokenUsuario = "ff55004c6023aa66dc4523e4f13da5169ea271e2b72443f4a32baae0638f234a633e18fc8efe09b3030e4e5221a49a648ecc4feea21d8f6ecf71ae6f65327eb8";
		String token = "bac2761186d340a601f57e99f50e2c6aa313f889aef265896cbade8dc7ab683f1a16b4d5ecc719bae25c66112757ecb9be76af76e45fc519b7a126d822c2aff3";
	
		ResponseEntity<String> respuesta = 
				testRestTemplate.exchange(
						  getRootUrl()+
						  "/api/servicios/usuario/informacion/basica/{tokenUsuario}/{token}",
						  HttpMethod.GET,
						  request,
						  String.class,
						  tokenUsuario, token
						 );
		
		System.out.println(respuesta);
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	
	@Ignore("Listo")
	@Test
	public void actualizarImagenPerfil() throws URISyntaxException {
		System.out.println("test Imagen Perfil Actualizar");
		
		String tokenUsuario = "ff55004c6023aa66dc4523e4f13da5169ea271e2b72443f4a32baae0638f234a633e18fc8efe09b3030e4e5221a49a648ecc4feea21d8f6ecf71ae6f65327eb8";
		String token = "5ba7a09c4b093ce04fa6654150f8e177a0700320c4264c28b8a7fc6890f1d76ce3ba1044d91390b9791c8e50bc6d6d79ac7730098b5f5ff2260b658e279549c5";
	
		URI uri = new URI(getRootUrl()+"/api/servicios/usuario/foto");
		
		HttpHeaders cabecera = new HttpHeaders();
		cabecera.setContentType(MediaType.APPLICATION_JSON);
		
		FormatoImagenPerfil formato = new FormatoImagenPerfil();
		formato.setTokenUsuario(tokenUsuario);
		formato.setImagenB64("data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD//gA7Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2NjIpLCBxdWFsaXR5ID0gOTAK/9sAQwADAgIDAgIDAwMDBAMDBAUIBQUEBAUKBwcGCAwKDAwLCgsLDQ4SEA0OEQ4LCxAWEBETFBUVFQwPFxgWFBgSFBUU/9sAQwEDBAQFBAUJBQUJFA0LDRQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/8AAEQgBZAJsAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A8DI5NNK8GpGprda86O5BCRjFNPSpmqIrmuhAQ7OQaQck1MyVGRg4oewEZHBpjHtUrDIxTPLA6nBrFgQMmBUZWpgvPXPNNZeTVrYCApnimsO1T7OahkXmmNEEkRBUjrWlChVAT1NUNv4e9alsP3a9/egbFIzTol+SnD71SCh6kkZSgLxUo+6aaozWXLqBGU5pwj3cdc+lS7OBTtvy1aWoFS4YpEdobcOBzisG5DmUBCQvUknvW9d5do4lO0k9fSub8Szixibyzsc8KfQetalRKEl40149vkOc4rfvdY/sDS4tPtAFuZRmSYfwjsK5bw8szu9wQAueSe59aj1+8Ms24tuxneB13dqqJuRtfmC0l+Zt5Y5ZTzuru/h/4VUaOLnUH2RA7iB/G55ANcR4H0h/EuurJOpNjbkHaBjJ9/evatX1GOytt+1Air8kYHGenSuqJjPcoatfJY2qnGZG+VU3dD2rCh09o3cykM+dzHPStCztWvJUmljMlxKMqrjgf7VVvEVyliI1DhljGX29GbtVSFGRRECz6gjEBpEJKRleOleleE/DxZVndd277inqx9T9O1cD4LtpdY1cOeSX+YjoP8nFfQfh/SF4K7TGoCqQO9fP4qrZ2Po8DQv7zNnwvpGx02Jjjgnt616XoOneUYuGZmPWsLQ7Ha8at97HFd/otplwdvyrXgTl7x9PTjym3odn5Emxf4u/pXT6XBtzn+9Wbp8XlFZPSuktU/dr/wB9VjI0LNvB8zZqVbdVc7elPt1PrVgxBl60cpMpcpVkDRxqVzgHms26Rv7RhnVfmwc/Stp1PlhB94/e+lUb+EK6sBwlVEyUuY898XxJNBMEGCk25G9SRyK8ZiDC4ntJkMQifKr2YGvd/FdoZba9EYwWQD8eteOa4sVq8N5IB5dxEUkk/wBoGu2jsZ1dzkL6wjsb65iZdiSpuJUZAPp+VeP+MdAaG5LRRqh2kkgYzivatfiUm3uoWbyzguV6Ad/6VxfjfSJbmSJ4yHRkDgj0716lI8qsuaLPnXxHZOhhuHXGCGI9PerdzAr6xa3EYdUnhCt6FgK6bUdJa6S8tpRvcZaJv6Vz9oS9ksJ+V4mwT716sdj52ppIyL+wMvmxRnAdiwX/AGsZrl7r97bqZgSkuUYD1Fd5d24kcnj5v5isK7s0eG6UBWJAlUd/eulbGEmYkEoFukzElVwFz6iuusbiK5tfKnQMZV+U5wQe1cxZwgJLA2djsCPatrTEWe38ln2Lg4c+o6VZiW7KD7RA0XmuuoQkrtz99RyP5moGthNPiQ/M64bnGTWlNpn2i6gv4laNvK2Pg9M8bvwOfzp88aSWvm7cyIdrk/eBHRvoashlKzuZ7eONlzHNA3LdeO4/EVYvLm4uZS0TGL5t8KEYx6ihSC5f+JsZXuT3q4/l26+UwIk3bk9MGgCvfyi6gju7YcxkeZD/AJ/GuX1iJIL77dbYWO5O2RT/AAkdP5mtuK4+zaiqbjGGJ4PRT/gen41Qu4002+MUyqbOZs4HJjPp/n0rKQ1uZzLFqlq0VyT50ROzIzmuYjm8u+aO5ba+cHAxkdq6PUE+wzmWL5kBIHHIFYmtQtd25njA89AMEj+GueRvE6XwJra6Zrv2WaR/IfgEH9K9l0Zw0eB1GcknNfNIneGW1uVYsoIBI4xXvPhTVzK1pIMtHOoAJPeuWRMztNneneXuAFPVcVIBkVkjEQJtFAWnbcVIozTAEXinhSTTlSnlKAIzGaI4cnFSbfloVOKAEEeCRT1XAIpwViOelPC0ARqnNP2U4JzTtppMaGquKkoVadspDG05VpypS7Pz/vUAGKd2pdntS/hQAzbThTsUoXjpQB4ew4ppAwKdJ1prdq3i7CQ1ulNPSpcU0rxWidxkJ+9ULDLHirRSo5FwBim9hMh28VDKtWyMCoZevArHqIgxhaaelSclTkUwrzmtlsNDPWo2qY4IqNutJ7DvYruuTWpajEQqicDtWjZ/PEBUBcdtyaey8CpRF+lBHFHMDIwlGw+lPXmlIxS8yRmMdRTiMj6Upz9aGHynB5pKWoGbcSH7SpHXpXBeIp5L7U/sqH92p2k122oO0DNKv31UnFcNCrSXhnzku+V+taJXdzZGpAhsLRlPyOcA1zt7M19qDxQfdOBvI6DPJ+tdNrMjpY+U4DTOOO2Kj8DeGnupZbiVQ9vCPnJP3vYfjXVGI2dtpVrb+GNAt4oB+/dfMXPVs/xGmWQOpbriYlooTllP8TVQjmk1HUltnUiV+GAOfKUdBn3rba1eW7SADy4kG9l/kDWi90yLT6rLaWk17JtMsvyRIBgRivPte1bzHMS/MEPA9Sa2PF/iDZZncwDK2yOMcAH1rm/B2mt4h8RW8cql0JDtisKlTlR0UKfNI9x+EPh1odKikaPDzIOcc4Y5J/OvddB0zygkarlAM5PrWL4I0EW+nQ/IEBUDHoK9F0i1VR8q7scV8rXnzSPuMNTjCmi5pNiw2noT6V3Wl2exF2uTWfoenBUTIGO9dXpdgqOOK4H8R6SNKxsVWLa3JNaVmjeWB/danW0W1VGPmNWoYsH+7VEykOXCbcjP0q2rbl461Acxuqgbqtxwr16H0q+U5pyIQpUEk5k9fWqbSmRXDDaTxWhLHsw/YCq6QrLFkjqc1LFFnKX6h5mhYcEgH34ryHxbohWPUdPcbViYvBx1B+b+de0a7AYru3YDKknp7Vw/j3TxL9n1KJSXUFXUdCPerpylE0n70TxfSyPEnhyRC/lzx5RtvZh61yt1Kl3oj9d1q/lPjrtrrdMh/sHxRdw7NllfnenPAfvXG6rJ/wAI/wCMLmCVG+w3yFW44Vuxr0qUzza0dDzjxjaPatFPA7BUcfKR1OeP61yptYrfWZJox+4kOAD/ABGvR/Fdq/lTxOgkDplT6e9eZS3izWUG3lrZjz07mvdoS5kfM4lckrjrzTts0kRBBb51HvWG1mhm83AVhlGA7iusvLv7Raw3Kje8fBI7g1hzRI0zMOAxz9K6kcc3fU5gxra3yI3MZbbyfyrSS32XUsBHAAYj2o1jTy9q0oUSOhB49BUkMzO0U6qBI8ZVj7VsYM1orllh8yF/9Wc7OuV6EVcKxKkU8KDyJRjHUsO9YumzpbamomUvG64BBxj1z9citOyt2iF3pkpYXELCeFlPG09KCTPvbF7WQyg5CHOP9k8Uy3l+0IElwzJ8qt6DsTWtJcvIVWVOX+Ug1jvGbW8OPlgc7HA6Y9aHsMoToLiykaXP2y3bYwH8S9sf57VUOoxarEYZwRMflMo7kdGp+tbrNRcYYSwvsYjunY+9ZElzCG+0hMc52KeAK5qjGtywSJGNuw3OuVJbo/vWRewvZSAg5Xv9Kt6q3nRiaGTIYZA7iqjzvfQIzjDgYYHuKxlI6FsYr58uaJmJiLblAr0T4d6q0Ol7Wk3CKQFVJ6V5xenZKwTmMgg89DWz8Nbtit6CBI0BDFO5XOCf1rnbuxSV0fTttOLqGGZf+WigmrOOTXPeDr0zWKxmMqE4TP8AdrpkUHOKRyN8rsIqdKmCYIoVKmVKAvcQrijFKVOKci0ANxntTwnBNOKYo2kUCvYB0paco46Uu32pMakAU7adtpdpo2n/ACakfMFOpNu0c1JigadxR1pdlHSimAU8UR4NKw6UFIaV3UojIp4XjpTulIZ4WVyaY61JSNgirJIx0o5xSlaTpQAjGo25qTrTGXmgCJ1zURGKslTioCuWNAETdKjPWpWU5prfL2q47gRkcdKjPen5J7cUjLitQInArQsSPK4BFUNvOTWjZHdF9KxYFpaXFCrwacqcVIDcYo6mnlaXaMUAN4HamA5Ygde1K5CDnNLCCWBAyc5FA1ucz4slaK3uPK4LKqqR79a57TNs1wkEcIPkcsT61o+LL8teSJHjyYec/wC1Wbo0sotrmUqMyDiQ10RN+hT1a6bUNTCgEhTtCr3rs7K6OlWggiVfMOBtxwDj0rlvD9uDfJKib9jbnY98Vu3zGK5Xy2InmcY7kZ/+tXTEg3dHtP7I3XboZXmbKtnnfjv7U2bUX0qzu5bkqJJDudzzz2FPvtVit5beziG9UUGSTGcNXDeNdcF5cRwwhxAD8qf3396iTtqCMzUbyfWtQU48wZwE6D616L8L7jTtHvB57Ay7/m2/wivL7u6ksUS3gOLh+JnznZ7CvX/hP8I9T8SRLNkW6HnzGzlj61w1ppK8j0sLSbl7p9AaH4/t7lQlvFmMAYYtgmvRvC/i7T5QI3zEfUnvXkR+CGuWdgGsyk8ijIO7HNUoLS/0i7Vb6CWzZODGzZVj615bpwnse8qk6e59haEYrlE8p0dWHADV1tlaSrGCQFP86+RdD+IGraU8XlsIgPusDkEV6/4R+O2I4/7QlHpyK5p4aS95HZDGRl7p7xBahWTJyaklQfNgE54Fc1onxH0TWRGYrpUduApNdGt3EXHzgkndwciuZx5TZS5noTQJsHPzN696n2jdnkGoIZ9smBjHqameXHOc1JjJS5guQSmDxkVBG2xAh9KkllMi8rnnuaY8Wc/5xQVFGZq8XmWzOgyY2/8A11y+s6cWhkjC5t5VyK7J1YhlbhSMZrNntFeAxkBtuQtB1wPmr4kafLpMAuI0CtbSCdS3O5R94fiK4fxuqeI9Js9Ws5M2zgSAjnI7g/SvoDxn4ca4hkJQOAPmU88e1eBy6cmhapeaBeKkVpfkzWMhOAWx8yD0JrqpSuYV6V9Tg7vUG3wSyEPbhvKfcckK38X4V55PajTtZuLSeP5HZmVj0bjIrpNatW0S5WKZmW1ll+zuLg4Khjx+vesfxlYXMFq91Jl2tHEZbvgfdP4j88V7dCdtD5zE0ubUpaYgXfGjF8KS0eeBWbKVMo2twVz07+lVY7w2moLLE+Awwy54KnrUktwkU7LuG3hk+lehzao8SStoWrRUIBVd6TgxNn+FqzIY3tppYZU2KG249B6j61bUrHd7GYpFOu9SOzCpNUia6tjdJhZY1AkHTdiupHO9yjqdsIZY3C5Ew2/Qjn+taNvM94ttMqbZdux8HkqDUMoXUtIWQczL86AdiOv9KzrPVGt0BjJRl+fce2eSv55piN25zsbYQM/dYj7re9Q27RXMfl5GJfkkjbjD+o9qW5uUEYnjbzI5V/eL/eB9PoaqNCt3tRG2TKMxEnABH+ND2Ao3sDuHgmUyMqlM56jtxXDSuLG5ktnyipyrH+R9a9D1S6WTZd+V+/jG2bJ9O4rkfEthh2mPzShfM+UcOp/qMfrXNMuKuZbSOro3HlOOF67TUcUzROQ5DZ6VSW5VIGhO9SfmUdwPrUQlMi7cfN1BB5rnOhLQnvbKGYsFx+8ORnsal+Hlx9m8RNbsAPPV42yePaqsrrPDEUyXj4ce1ZyTnTtYhmRzHlsjA5FJoq1z6f8ABUryW8bscIYxtwe3T+ldnGp2j0rz34f3Ak0qxYYIwVyPrnFeiQgvEMdqxPPlHUmRcCnUi5xinqvrQJKw5RT1FIBx0pwU4oEwpCKeFyKVUoENRelSYHpShcdqXAoATFOxSrzS7aCWM609VpVUU76UmaxDHtS7aFU09VqSxEX5adszR0py96BAE4pdlPFGaBnhJA5phXk4qVl/CmkYFWAw8Co2qSmsox0oAjFKR0pSvFJ0oAaelRYHpUxHFMIxQBGyjI4qGQdanbnFRMua0QEJHFRNUzgA4PFMKAjIpgRHqavacuQR2qp5fOc1e03Cg0mBbAwKcOlKOcZFOKjHSosA0jNJtOeuKeByOKXaC3pSEyvNG0sqIrd+aL+6TStMuZyQJSNqU+Jdzs/RfX0rkNb1E6vqaWygrbw8sf7x9K0RaOf1NZZl8puZJjnmodQuls7Dy48MSAMe/tWlcIhv0mZi4VMba5Jro3uuRW6kk7uF9K0RstjrtCj8jRU6hpG8xs9h6VY0i4NxrQmkwwQ7s9sCmXt3/Z9lKvAcp5cYA/Oq+gx5t8sCAo5NXcfKW7/Uja2byZ/fSZO49ySf6VxxZ4C15MwaU5WND0z/AHq2takFw3lK2UjAIJ/L+RrBu2Z4kY8Kp2qKlthymp4C0d9b1yOR1DKJACOua+8PhtpsdpYwRKoGFHPpXyn8D9A2T20zjdljIQf0NfYHhZhDbjjkY6V4OMrX0Pr8uw/NA9R0qBBAuOw9K2G8LaT4iszBqFnFKD0c4BFcda615Sqo4yMc1r2HiY2/ynkd8c4ry4YjlPdnhIuJx3ir4Bray/aNKleaIDJtnbkfQ15vrXhK+0ckfZpQi8lW5INfS9j4it71VdZBt+6Qak1OwstUj2SxqxPOcZrthjHszyJ4FRl7p8v2Wuyac9q6SSW8oPKk16h4W+KOoW8kYuZjlflI7Y9a0PFPwjsr1xPbjY554HP5Vxv/AAh15p7srKXAOGbviiU4T3Kp0509z3vQfHserohdwmfeu4tbxZEGHDZ5DCvm/QV+zsYtxClVdCB/DnmvZfCc8jWiqzZj/hJFcL907V72jOzL/LketSSMxj4AqlG+0cnvUtzMVwB09az5iOX3hDJv4as+7j2TEpwO2KkeYhs1HcS7lzkD0qbm/KYetbZYVkYhMkq3tXjPxQ8CwazpLJGVSWN/tFu+PuSjp+deh+PNW+yWbYf592ABXnPibxT59rluGjTn3NdFFyuKq1Y+b/HssOsWsi3SiLUiBDqFrKOVx92RPbPNctpevnUDqGi6jh5UgEZc8iXA+VvcgYrW+NEt1q2oC/tQIbjy/L8zsw9DXhVzqN9BcpcuzQyqCrEnrjivdonzGIk03fY6K8ibTp0WUl1RtoY9cetPknE9uwf/AFsZ4YdwelYSa20wP2hi0cgwSex9ahhvZIHCl90e7YQf0NenE8KerZ1iyNd6TvTJa0cFhnkg1t2t0txGrMAVYhSOxzXJWNyLG7U5KwSjEmeRmte2Z7Yz2u/MSfvIm/vA11xOaRoaagsNTurRkJBBKEnPHt+VY1zaLb6k5RiyT5KL26961EkkkEd3Av7+EbipPJB4I/z603WoUubBGhbIH7xDjGV7j863exBHpqhRJFOuVjY7B6A9aguUXLq2fk4XPOR2qnaXs14xV/v424XqM9DUlpqEcqIJc+dzG64+6RWZRTnvjEWlcq6Y2SIRnj1FVbtTLD9mJ3bRvQ/7JqaYGJ23Y8vJ7dqo27mRZEd/9ITJB9U7VjMqO5yWqQPZvnOHVs49QayJm8icTQtgMe/8q6fXohNIGk5IHG30rk71Tbh1PKZ4zWDN+popdhWLY46sOxrO1S48qWKTJZXPU/yqnBqYSRQRj+E1duZBdwtA21mA6jqD7ViUz3T4La79tjgsmbC8tGjHJNe12QLW45wehr5N8C6lN4V8QWM8xKQEhlx1weD+VfV2jzC5s43JDE85XoR61LOOo7F0pjGKeFNKV44pUU5pGTdxVU8VIFwKVVxTgOelBIxTk4xT8Yp+AO1BGeKAAgbRSYHvT1XpTtntQNK5GFxS5qTZR5Y9KTLasMXpTlU0qp7VLtHpUghqg07afWnKB6U7b9aChipmpFT2oVakAxQA3ZRsp4BpwoA8G9aYQD1p9NbvWtgI2AB4pvX3pzU3NCQCHimFcmpQN3WmsmPenygR4ppHFPNI4+XinawEL4OMUxe9SlCDTSmBTAruMtTdnepXTA4pu04oEyHBOQODVuwXJYdwKg6HJqzpx/fH3pom9i8F+UD2pyrkU8Lz1pe+KbWgcwzb0pkxKKWAz7VN796ieNpHUHnisGtSr3MnWdSFlpDADErcqprnNJgbyJZmO85692J7/hUusOdc1Boo9xit22j3NOSEw+cyblVRtOO5raKKW5japdRWkU0nBwNoH9a5jwlFJNrdzeMVWKJcBe+a1PGkqwx20CsAWG9yfbmqWiB7fSHncLG90fNUHjAFD3OhbF3VpWmmji34BbcxrfUCz0UNjHmcgj09K4/TGOoaiiZJ3tgYrrdf2YW3QsGjADgetUUYUuGtSh4Mp4PoOtVrayN/e7QfkjXHPQmrlxErEBT8qqACe5rofBuiG8vFUYI3A5qZy5YmtKPMz1P4U6c1oTvQ4jQAEV9EaBlLXzD93HevKvDulLp+lNJ8yM7AK3QE9sk1PrHxeg8L20mn3mnXov40yDb4ZH9GBr52tS9rI+tw+I+q072PWm1uBbn7PcSC3iIyJnOFcdwD2Nc1YfEPw/pOtwwzamJoyWimSNizZ7MMdvWvnaLxH4t+M2oDTIg1lp7SZ29AnPXPrXt0/wAAbD4Z+D7XUFkeXXbkrFHLNyq56kD/ABrangY8l2c1bNZv3Vpc7uz+IHhS4dljv543jVsZBGDngn8K7zTdfS9MUmlX8V9Fs3uhYBlGPWvL/Dvwj1i+8OtqlvcxCMSKjvMP9YScde2KpeJvhtq/h7V7ezjla0uWCl5rfKhlPqQcGolhXycyh+IoZglLWp+B71pPieO7fD5SYHDK3UVp3Wm2eoRklMOecivBLbxNrHh3WI9L8UwSQCTCWl8oARh6k4/nXr3hvWHnQRykbx0YHIceteXVpzpS1Pew9WFeOgsfhD7POrRsQMEED0PWu70G0MFujZwp4x6VjR3GTu71taVd+am0HA64rL2lzaVM6KFC6AmpZkycdRiobOUOmw8mr7weVCT3IquU4pS5ZGTLgEg8ADOayb252A4HBrQ1RnSAHIz3rnL6fehA6ms3udSa5Tz3xnIL3UZEJOYxuGehNedeI7druRYkU8DdKR0r1rUdOikufnOS3U0200K0EgxGGPqRXTCqonO6bnueDW3wvuNZBkuIuh3RqRlce9cN40/ZW1HVd95pyqJgCRbHgSn+lfZUcMGnI5jCEqN2wrn9K5jU9ctEaRhLyBu2xtu479PSuqOIktjkqYeMvdmfmr4i8IXXhbUGtb6J4cHDhlyUOeRj+tQX1nDDKpGSjrjPbHavtH4u/D/S/iDYO8ZkstQMW6G9S3Y5HYOMcg+tfGXiPRLrQb17K/dBLESA6ZMbehHp9K9nD13NanzuKwsab91mnpuLi0EYwSv8I/nV2ycPFtLDzIztUHrWDp1w0iHa4jnVeMcbq0Wm2tFdDBjkwGQdQ3evWpyPIlE3LeRba5DSKxUH5lHp3/SrAQQRSRM/mxod0RHbPaqr3SM5I6wjBz3BGDUsd08BIVFJXkE9wa6LmD0OduQulXyXCf6psrJ7gmorlmttWkZ8NbzKMOP4Tjg1qaxbh7Z0+9uG5dvf1FZayCS0COfmQAfN3WhhFEc1zviCg/MSDk1mXdybYmVAGwcOo6sP/rf1q1fMkkLSIcFTzisiSUI2G2sjjAwa5ps6ErITUyHIAISOQZUnrisDVQLmMrtGAMDFaLljG9pId7Kd0Z9u9UJJ4riJlLFJUOOnUVi3dDOIv5DBdncMcYB961NMuvtAUBVDKwBY/XrVXXoQJ5PmDL1AA5qnpdxukEeD87BTkVl1Gj1d4o7nQ9FmRD5ttcPHIc5GM8fnX0Z8O9QN34b0+RlHyqVb256V8/eF2a88GXUMjJEXlGCF5UoeD+IzXrfwpv3gsHtQGaOR96t796u10csz1hVyKkVKZDIJkVtm3cOKnUVlsYCBaXGB704DmgrmgBdpx0oC0AHFSbcgUAIvan4pAmKlCgjpSY07DFGRS7adt9BinIp70h8w3Z7Uu0epqXZ7Uvy0g5uxHs9qeFNOwaMGmLXqKFA7U0KTT9p96UDb1pGogGKTn0p+KeBx0oA8CxxSFc04g4FAU9a6wIWXmmkAdqmK0h6UmBBgnvRyBipG4puM9aQDNobrTW4NPYEdKYeetAiORjTUO4HNSdRTWGOlIZHIOfamMMLUuc0yTkUCZDjI5qxYfLMB61CRgcVLZf8AHyPSmiGairyaV+nFLnpS4yM9qoSGupAGBnNZut3xs7CaReHwY0Udya1GJI478VzGr3Au7vy1+aO37+rUFGXpFpJbW5UAmcjd1796s3RENnErt8oBJweSfU1bgiCpGXO2TBfPSuc8S6gVhYLhdyZUj+Id6DRHDaw7a5ragN+5Vwp56Cr2vXcYysK4RVCoG9BVXR4GiN5MwGTwme2e9VlCXeoLGXLhB1PSg3imb3g63H9pI+CIoIzK5A71rXDm6umkyTvO4561TsG+y6RI8fD3MnlgDqFHWp5W2R4Q5Pr6UFk9rbJJE7KN0YYrg9jXf+BLNIpY1AzjnHvXGaZEYLcLjIPz49Wr0jwHZtNeKFHzkgkegrmxEvdO7DrU980LTI73RIkeIOrDlD3rnvE3wpsNSlWVZ7qNWXa0UZzn2BPSvRfCth5djFGRxgGuoi0ZZXBKgj2rxHU5T6SEOaNmecfDvwXpfhaax+z20yLBkuk3zZPrXr/xJtE8QeEbW6sVWWa0YTbB1A78Un/CLxXkRCExk4LOB19qsHRbqxLJBlo4VLMf72R0renibKzOTEYGNb1Rx0UKeJdISwS5listyvJDFMYw59CARWtcaLcazrGn6far53lhQWYlgqjtWza+BYXSWUwtBIibzg9e+K1dItL3RrAS2pSKWRDksMsDUJK+r0PP/s+tLRG54o8E6f4k0UWV5DHPsARSRlkPqK81sPDGo/Dy+FjfyGSwkO61uGHzL/st61tyapql3cMi3jRzKckjuau6rca5qtpapfJDqPkfdbpmpruE1e572DwlXDyvfQ0IYZGVTnJxnjvViyvJbeZtoyQcVd8FWcn9kpHeALKh247gVFd2wiv5dv3BzXh2R7MqnvHS6dcs6eYB8wxkeldFNJvi3DpiuP0mc7TzxwK37icrbbl+5jmuqPwnHUheRz/iG+EAcDHvXD6hrhiBAOBW94jvFUPj5gRnPpXmerX292VTwKx5TqiktzWttR+2XHUsc1rS3osrd27AfjWZ4H0pry3aYjBJ4zSeNpHtJ4dMs8T6jcjakYPC56E0403OVkTKSi2cN43+ILkCKGWa0ujlYI4VzLI3YHHauM03U9dudQFlODZThW82CzQ4BK8/McnceuPU19HfDT4IR6Bqcd5rh+16m6+Zvc52+w9K5zQTH4Y8c6xdzWEdxLFOxMUvAO7O0n8MV7UaMKdNNnyuKxk+blPBtXm1EBorXXtd0u7KCKLzG34UcELkc5rwbxvd38jX8GrWJvLO3kCi8Eex1B7tjqa+6PEt/deINAt01SzsoZUuzLDLbJgqp52k155pHgqx8S6r4hhltVmsJowsgYZAf1HvXXQinO8XoeVUxTatLc+EmX7DefupPMRDlWcYI9M1vL5dzZRScAgksoPANd78b/hDd+Db7z4Ns+nYIDqOUH+1XmGmyfZ1ktpGBVwCp7GvRhLU5ne1zShu/Lc+YA42YkGeo7Ee9Xba98uEITkHiFz1A96zp4d0UbxY3AnGfpUtjcpJb7SV3hsE9Sprp5jHluaceUKK5PlucjI+6axdRiOn3TyMcpIe/YHtWjGXmBjZunK+5rNST7TFPZynfICSGbqDVc4lHUrOqWzmTIMTDa3sDXO3kBsdQaG4yI3GUZf7vY1qG4kgSS1lADdFYj73tVG9ie6iCFwZI+VY/qtYyd2aGTdI8E43cyKMqwPBWsu6nAYTJ/q3b5sdq0p74XVo8bLtlU9R/D7VjzkpHJHjGRnFSUjP1hEaUFdxUjO41huWimTYSCD29a2pMz2kiEncnIFY0q/vVkGcAgms3uSj1LwZfsthqO8EiVFAXPcYJNe5fDdPK8KRXceQyz7wSOSh4Ir5+0QslmnlsWMrrtAHYkZr6R06IadoEUcJH2fygdg6gntWq2MZnothHsj2q+9V6fjzV4LWX4eZv7NiR8GQAZPqO1bA6CsmrnMhirg04/SnUoTipsDGBSRUqjjmhRgin4zSeghuM0uTS7aXn1pEsAKdj3oA4pV5oNLIcc+tKo4zSGngYFIl6bCYPrS0UUMtPuO5oanBhiggGpK5hFGaeBQoFOoE2eCsOKaeBTmptdZQ2mNT6YwzQAwikxinFQKSiwDD0qNgalYU00ARhc1ExJJHpU7VEetJoTIyDSMPWnlN3NMZdoOaSiySJjg9cU+zkCToByD1qKTk5p1vxMhx3qrWA2IyGLEetSnC4OM+1RxL8px61IM55/Sgh7mdr2prplsWH33GFX0rndFjNySq5kfJZz2zVjxVOJrqMbcqnGAckmtO0h/s/TBGq7Xf5mIH3KDVGbcEmGVR8zIQp9vWuH8RXiSSTT5Hlxnyo0Ht6V22pgW2mXUyyDcFIUH+ItxmvOLtTHdZcKY4IwP94/5NBrEoXDiG0Klss/zEDt7VFo9i2CyZ+bOc9qhncyXBU8seSFNdFo9ocQROxHG6QgdBQbF23RiIuMIgCoP5mrdtbiVtzHjec/SoLlsP+5OVJ2qe1TWytCgQnODljQBoPMUmjjUYLHC17h8LNEeWWI7QSoGWrwjQZRqWr7AN4RgFr6x+EOi+RYpI4y0oz9BXnYuXKj1sDT5nc9d8P2GIVHHCiux0/T8445xWZoFiQRx2rt9NsMqDXzUpOUj6qFO0SK0sCsYGKvrY5LHA57VrWtkFQcA1ahsQcYUcUc8i3FRZhNZna5ycMMVSuFMUBVRk+tdiunLjk/pUD6ZHghgD9a15rji1zHnklgRMHSNQx7gVraRHIyKrcjOMGuhl06KI/cB+tR2+nrDKJM4Gc4pOKfU332JEh8pZHUBdvX3rBkLSvI2OpxWzqM8syFIVwQ2G+lUHXarLj3rGUb7BGF9x9gRFgdya3Z3xbEEgLiudg5lUA9Dk1tTsrWLZOTVRT2N6kdEzzzxTcBZJdp4Ga4GQ+bIx/AV2HiuULcEAZBrlobcTTEA4Gc1VjOrdPQ9D8EtFaaSFI/ebTj61WXwTdjVG1IXgEzneNw5X6VD4Zu1QbJN2cYGBXUTXLzRBQe1OKcHdHNJXepUtrnXYbstJfmZxypfpXNeK4brVNWfUkCW9+AEmZPuyY6ZH0ro/KnGSM8HOaYgdtwdM5OTx96upVudWkctXBwnqeeaq+oX9nBaqUto8kPcdhz2/CtGzn0fwpoot7M72PLSHq7Hqc11o0y0jyhiyuS/0Jrl77wtaSBQP9WAx2mumGIVP4UedLLqTd2eE/FeY6wlzBHsIkUllk7ivlzxJpUEN2Bahl2LyrcAEdce1fbfiHwjDNHu8rBUFdx64r56+Inw2+xpO8Kkh8lfWt4YnmkRiMIo07RR45p8ySXIjkY7SvQdjTriIRStJGgj3jEn+yR3qneJJpmoKV++hAY9jW7NtmKvtBEikMD1Jx3r11K6PmGnB2kZwuWkXYGCvkH6n1FVNSkd1E8eFuUPz8dRT/LWVl2ECaI5VT1wOtJNN9rcyeXt3D5ox/EKrcDKvrlruBblBllbEsZ6pn0rNnZpIiyNuZG3D/aFW5Zxp87jZvQ9R/e9vrWdcQvaSF0bzLZvmIHUD1HtQO1zPuXiYvcRHG7hkI5z61kXyujhi2eK07o+RcpPKFME3yZz27Gqc9obaVlOXi/hZu4oGtDHllEJD5x61QuAq5x8yOMg/jVrUI1t5CR+8Q/pVcxhGRGI8vr9BUsTPS/hSxvruw3xgQqzRs3v2r6H8O/6TPHazYGF2uh9uhrwj4UR/YdImlCiVo51aMjgjJ719CeG7T7YbmfjzYWG/bxgkcimjmmdFoRNrd3Nq5yUIK/Q10Y965q2Utq0b5+9Fj64rpU4x9KZzjlXmpV5FNA5HpTgMVDdhMCMClUGjqRT+lZt3JbEo5pR1pwpCE2gUoXFFFBoh2Kd2poORS5oAKKKKAEzzUq4NRY5p60ASjAo3D1puRQKQ0eDkmijvRiuk1GtTcZFPam5AFAmRt1IpCKeME0jjrQTchJpDQTSUWGhrdTUZFSt0zUY5oKDGAajYVI3SozTuCRA65OKWIYcZ9acetKDllAx1ouPlRsQArEM9+aS5m8mB3wSAOgpBvIXgYxWf4hmNtpU0m/bgY+uabI5VcwdLRrzVZJ2CtCh3DPrW3cXGxWBJzIcfWqXh+Dy7JFYbWI3kmq2v6itpHNMGOxRlfr6VJSMnxFqMBvRbM2+CIYYju54xXD6hJ5DlB8z5IA65Iq/eTFPJEm7IBkbI5Zj0rGljuN28DY5+6znp7/lQbxQzTYUln85iWVTgerGuzs4o9OsnuJEKzSjBTvisTwpbQvO0ioZI0OS5HAPfFaOqXr3V55Rz5rEYHZRQbWGGTzmQqNo6hRxikeUwBmLnJ6Z71DJIDJtHGep9qhuroiO4mlAS3iT5D2Y+goJex0XwtXzNYkVuCjhx+PFfcfw60oiytwBjABr4L+AF42rePr9WyYig8sH2Ir9GPh7aBbCD1wK8THS6H0mWRvG56PoViwRTjtXY2FsVCjn8aydGhComK6a22gA14SR9HZpFyGMKBwKnjAz0qNZFZeOacrYzVnPLXcm49KrSEMT7U9mIqvIQBwcmgIpkbqCfmqncyccAYq1NIdvIrIN15kxwBiqsdsIiRsSCSMNUciZDVK7bj6fSmt0bPSs7m0SnAp+0t6Yq7cmVbV9ig5HeqNrLunfkelaWoYSzORjitYtGstonlHiJz9okVj8wrLs0AcHvV7X2DXz5OQfSq1qFfviqWpnWlqb+mjymVs11ls3mRBh1rjbWbZtzz9K6vSZt67egIzzQc1upsRWxljABxSGwYcZq7bALHxg/SrSRK/zd6CJJnNzWrxs57Hjisi5tGb2A7YrtLq1LrwOKz5LPcgKjpWbBHneoaRJcMSTgVwfjDwel9ayAjc69D6V7fdWKkfMvNcnrmnJycVUJ8ruROnzo+Cfix4R/s+/YRxbJNpcEdOOo+tcBp9w86kfcY4ZQfWvrD4zeFVkgkuVjAeIlw3qvcV8n63atpupvJGjCF23jPBFe/hqvOtT5THYTkk5WK946nzLiNSsyHEir1piyNKqyRMwYHcCf5VJcubi8N1CoDY+dexFUrgvbp5sZJiJyUH3krvvY8myINUhkuZJMKVyM57K3qKwo2O14N2JEHDHpjuv0rdklJjZHfcGGVcdPp9awrl/LYiUDzMcbeAf/AK9NSDYzpH8mOSKSMG3kH+qbt7j0ql5he1CKd7Rn5JCecelX5St4SjsHiZcBhxzWCSLaf5GbKnbmm2IfqFt9otRNGcgcSZ6/hXPs/wA7hsuARtPcV0U1xvmKnqefrWVqlqsQd4xgNzgCl1B7HpXwiu3mkuoS6sJY+FboTkYFfTXguZptGvLkBR5k+MKehAAI+nFfI/wp1lYL4xHEc28BCe5HOP0r6u8J4Tw9YyoRskZpZgP7xNWc80dRZBl1FeAURMZ9M10KAEA+orG02AC1eUfxnIrYtzuUfSg5SZGJ4p4PrUZG0ZFOVsiokZvceOop9NA6GnVkSAp1IBS00NBRRRQzRDsAUtJt96WkAUUUUAFKCBSAZNJg0ASUDd6ikHSgCgaPDc0lHNFdBqI68VA2asY5qN+tAiEZBoLZzTmXIqMrjvQZX1GEYPBpDxTsZ5pj8CncpOwE5FR9KdkgU2kadBG6GmN1p56GmNxTJbsyFuvWhOGFKetJnB9aQrmvGTtHPOOKwPF0vmyW1op++wLA1u2ziSJS3BHJH0rn9QYanq0dwBznA/CgC9CfLtF/LArkfEWy4nW3lc+UMu2DzXXSL5cUzdljJ/GvPpT5pubiRixcYPsKDVHPanqOQzchnYbB/sisaS8eWfHMkkhCruz+NLfTGa5chsKpyDVnQlFzO16wxAnCZ6k0G8TprZPsOnpEhI3DdgcfWoC28vOeGA5PqKr6jey7I0QESynCqewqK7uVt7dIA4dzyWzQaEsj7nYphWYbm9lFYvibUBJYJbwgrD1AB4J7k1ZM7Rxsx+ZnGPfFYepN5hyBgRjIBPegT2Ou/Z7vPI+LllbA7VkgdW9z1r9Pvh9CBYwMORivyh+Dt39g+K2izlsBpdjMf9riv1h8Cny7dE7Y4rwcwPqcoaaseq6R9wVsQ5VueRWLpRxHituIBwOxrxT6OUGi/C4UcZ5p7H5gM1CoAXHeqyXTy3fleWQB3rVK5z8pdZG253VSW6BuHiGdy+vSrr/uwAec1Giq0jHaAT3xSaCNitKHkHBFVJbUQLnjJ61siBV57Vl6kFiBLN1rZJG1OWpnvKF5I4qpc3eyMjPWo5bvzHYDoOlZ+oSgxqQeDWPKbwTcrFqxlG/J7nitHUr3faEMOgrDscvJlQSO1WdRuP3DqeTjpUXsdip33OA1kr9qdh26VTtJhvIzU+sKS+ehGeKwop2huPvdK2gcVeNpHW2kmSp7g12elKJQOOcc15/pNyJwO5BrvdEfdtwKpmcdTpbUeWCO1XVJTGBxVRV49/SrycgcVKCViQsp+X161VeLaSAOKtvHiLPf1qo8u1MHrUSM4q7Mu8j/AHmD6VyusLwciulvpMyrlsE1zGuZ2jHNZnWo6HlPjaxF7byqyh1UHg96+QPiDoJtZrlT8wDnaf6V9q+Iod8ecYKseK+Zfi3oZjkuWjH7p2z/ALtd2Gm4yVjycfT54M8DiLwMWUfIRipJE4Mq8Fh847EVLeSizZomXMZPyt70x4RImYpuHHy4/XNfRJqR8NKLi7M53aUMoRhLCzAhf7n0qCaGOe3kjJHrz61bubWW0uCoyFxyq/zqrLNuIWXasQ6S44z6GqUdSDmrkyROVyFZfXp7YrGu5NzEjKt/H9a6/VbFHt5RIhYsAVYdveuQnVoJMON/uO9a2Aopf4cIzHI6Maux3vmoySYYv09qoXsAlYFCAfaoUmYs652svFFgNOxlbT9QjuIzxHIjvgdq+xPBV0bzw3Z20bZDoJUOOx7V8fWDm5aWFiuJY9uB1yK+nvgBqL3miWBnJXyleBS59OlMxq6I9qsG22CRkcoMYrSiG0KRzxWNbkxMATndhf161tIO2enGaDkRIx4x3pY+aFANG3YaTVyGtSUHoKWmqQec04EVi1YmwoOKcOabgUq80hbC80uKSnA5oBNgDnNLSAYzS1pypml7Cc0c08DOKMe1S42C4ijNLSovGaMGpaYCUYoHPtTthquUTPDD+FIetSdBUbDnNbG4h4qJzzUpGaikFAhp9Kay5NOPJopmXUj2Y75pjcipW71E3SnYBpwKjNObkmmLzRYpOwhHFRSdM1KzYBqF23AigG0yMHNBGTxwRzmlAxSM20UiNi5cuYrSMg7Xl+Wsm23HUTjHlRjHHr3q891uUjAYKnGexqjorswmyBknk+9IvoSa3dfYdIuHJ5lG1RXnfiS4bTtIULxJKOfpXaeM5nnNnaKu043n/CvOPG8zb448glflYZ6UGsTlLiZljjiC7pJWGBmumt3S3gMA+6uAGA9uawrGANcPK4Gf4Tn7vvWjJdrtW3gVpWl4G30/vH0oOpE8V00Ylu92/ClE3Ht7VQMyqy5++fmB61W1G8bzkgTZHbWw+Zs9TUVtIN5kdtxk4AA6Cok2ijWimeQPI33B1NYV3ILiUlR8pJ6nrWlqN75NtHbKu4FSxAOMVnQRCWNMngc/SmmTId4UdrPxVo8x7X8IHr98V+tPgeYm1hA5O3O41+SO9rXVNNmX5G+2RuWPQYYV+sngmVn0i1YENuhU5Hfj/wCvXj5jG0Uz6LJp2kz1vSZ8qBmujtslRXF6JdbUGcA+hNdbaS7ohz1r5+59k3eNzRiyGwW4NWxACwI6+tU4kDOHz2xirWWHO4VrGRxzT6Eske9Q3UjtTkjC43ADPvVYXHl5DN1oebcV5NbcyMuSRPcOEj68VxniHUjPOYUzkdTXR39ziFvU+9cvJB50zMCN/c04yR0U4Naszlcx/eOajnYSyRxVrLp6xxlmHAGTWXFj7QXZfw9KlyR3RS3NvS9NMpQIpAqfV9JaOGQtHg461p+FdWgjcJMAcHitjxbd28unv5ZCtt4pckLXOKVepCtyW0PB9dtzDJIT0A6Vxd2SsuR6122uzoPOLfeNcfdRfvVI+6ec06Vr6nTWTk0W9G1H7FcRhycN616zoTqYkK8gjOa8eni3rwMMMEGvTvAGoCaFVb7ygDBNVV8jG1jvIE5VsnHvVtZUgznn0piAlew4qNjtBXP41C2IauWnYupANZ1wdqkHg1OsgQZJrOvZdzk5OPSokaUoamLqkwDKxPfFYeoTkKOeK0tSK7sHJA5xXP3024HPABrJbnY1Y5jxDIfnPBrwr4jxApJkbj0JPTFe0eIbhQrndgeteQeMds8Fy5G4EH2rqpaM8nFPRnzfrOlb9QdW4BPGO1YcUv2O7mjaMAdNnp7ivQBYCeS6l2lwvBzxx7Vxfi63isisgyk/ZuzLXuUKl9D4zExW6KFxcJKWLc7R8vY1z98xYsqruUjJIHFMnvm3mfLtC3yNgdD2pn2+Ep5ZLHHRgOnrmvTPOIRdKLcIGG08EHtWVeW9q2UlRo0PPmRckVPenIaQAFjwMHGRVH7SFVkALDH3T1/Ck2Bjavb2iYEV2zSj7u5cAisfzwzOMncOenWt67siqIZBvgYH5gM4Pv6VgXllJAVyQQP7tCGiWzunjlV1GHByM19J/AS6mSwUY3W8shKZ7GvmaNgjqe4r6C/Z81GNora3lkI2SsACcdelMwrbH0xYZdVMoPnrgk9sVuId6gjqe1Y9jGWtcsSXXvWjEAEUoTkdRQcV7FxSVYcVIRkZqFJVKqSeasDHY5oBMiLsrcCpBnGc0opMbcnNJxuAuTT8naKYOaf6Ck4pBYdSgUEGkU81kZsdijFLRTYXHL2o7UgbFAOacSojwMCnZFG+mVRQnTP1qTJqPtTx0piZ4cemKY1MaTB6UjEmqNx2aidge9Kc0pAAHGc0ARZ4yKM05sKKiY800ZNagW5pjHilzgVG7ZpiEJyTTEOKUcZpv3aYCNzmoSMZqUtmo3oAjLbTTHbOMc+1OdsUzIBBJ6+1ICuGbyuBgl8fhViygMbcZ+9njvVW3DS3ZiY4C5NaaRlVUqdo6EmkM5/xXODqcZDAiMqCTXlPie5F3qrZbfn0FegeIZBJLNcbsKGxg15s7G51SWQYC9jj0pHRBLcglZYgqElUX7zHuPSq1uzMswGfPbqAfur2GafrD4eNj8ykcY/vVnWUwhMis582Q4Ge1B0xsPvHyYYVPyry+O5961IZnUIg2sx9ugrGERhmcuclfmPuKns7wvGxwfNzyfak1cuxcaTzLiUM2TtxnHSpLOM+aExwy7vpzVGPdIzFedx24rXQi2ZUGSzYHTkVUI6mcjO12EpC2RypDqB7HNfqV8HNQGs/Drw7fjrNYwyDHugr8wtXRmUgj94gPB7iv0U/ZK1X+1fgr4c+bcbeNrc+204H6Yrzcyjenc9jKpfveVnvGlzjAHf3rrbC4yoGa5CzQAgjAzW5azkAV8oz7iL6HUxXW1RUxvcDtWBHcHGc9KnE+9arYrkRqtd7jkcinrddOKzYXU8Zx9as7toBOKLl8qG30pcEDpWNaXCASofl+p6VpTOQeATWNqmlvMm6Hhj1A71aNo00NvfF2m6ejJPdRoMYYs1YUHi/RNXvGSy1K3lfpsVuaF8LW1wCLqAEt1JGaba/CjS7W4+0W9ssUhGdyjBrTkuacqibdndNbzA85PINWNb8QH7ASDkgc81UktJLC2VMMxHeuf1J5Ar7VYtjgEVDstDFu+5zWoTzXV1uI/dv3PGKsQ6X8qu2D9ORXH+KvDet64rRieS2iJyPKOCKn8L6DrHh23SN9QkngHO2U5P51cRuzOjuo1GCBjmtvwbOYLjcDgZ5rnbjUS4EYXfOew7Vt+H7d1T5uHJyTSlJmSp3PXrC781Blu1WW2nJrmdLuGCgZOAK2o5xgc5zWXtGHs7E7sAD6Vn3OSDjHFSzXAGao3N3tQ4o57mkVymPqJJc8cVyGrXBG4Djmuk1S7zkCuN1eUgNnrSjqypSucnr852ugGQR1zXkvj6c2+mSbm2KOrD0r0rWZC0hrxf456quk+D7+4Yj5YzjPqa9GhHnfKfP4+t7ODZhpqGm6RoKCCRLq6mBIG4EBu2favCvElrrWuauIoC0rBiFjEZAY9wPauf+HHjaWVF02a4SCMSmaSWc9VHOAa9F+IP7Q2k2OjR2vhqBTrLIFlvMAhRjBwa9inh3CWh8fWxKlseduZgz8FZEO2SBuMEdRUDPGJSwB8mQbWx/D7Vy+k+JW893ZyyysWlLckN3Nb9w5eIS27eapGWRf4vQj9a77NHLzXBUKK0LtvTd8hHUCqV3AY2AOd27gjir8IjIVH3Ix+YMe3tSXEbPGyypujJ+WRTzQxXMpbo24eBzgOd3I4zVDUII7wKynypiPmU/dq3e2cbyYikLqo5LDkGohOEspYWcea3G8LzUplrYwbm2dOHjPy9z0/Ou8+EWtnTPEVryF+Zcluh5rj/tTRyMGZpI9uNrc5rR0h7aG5gkjdrObIKhuUyK0IqK6P0F06X7bb5GMOobA9MVeiVoUEYA471yPw41f+1vDtrcLIsm6JULj1711zfu146nrQeZJaisCJF7qevtVxecEdKqrzGoqVH2jFA0WKKi8zHapA2RmncY7OO1G6m5yKWnuA4Oxp6jBpABilNYtdiWh9FMpwOaz16kC0DiiimmNCjJqQdqaopd2K0RaF4paTHFNJPrQDPCGGTS0hPJoJ4qzcDzSO2B9KaSc0jHK4oAY7bjTe1BODTDz0poybuDNxUZbmh8imiqEBOKaeac3SoyaADpUbtmgnrTCaAGs3tUbAkdsU8j3qG4cRpu5yPSkBHYoWnlkzkkhcVryY+xMccAcVm2DEWsjgfN1rQI863WMHCsAcmlca3PNvFVwbdZo8E7U3kVw1nLuWZ2UAc9a674jyNFeuqnGRtP0rjLH95NJGOcnIFI6Y7F2+0sjQ/tCrudBuGK4q4eS5nLqDuLYA969bgtdtp9lYDzGUPGG6GvO9WsW0+/YblCyliAD9w09C07MqSthCoO4qMk+p9KNOG/fIx2sxwQKqFTGNqkkdye9XNP3eaI+jfzFI15jQtGAkGxcLuznua1mh3zLKuWTGSR/CRWXhFljQcYOM102jRb7hlVc7l2FT/OtIkyehDrFiIXjbOTNFu5r7K/YX16O88BXumeYC9pck7PYivk3WbUT6XA+0h7TgtjqucV7J+xfrf9gfFC/wBKdglvfwbkyeCR0rnxsOalZHZganJWTPvm2/hrSgYgVm2nzKG7etacYwf1r4uSs7M+/hK8eZFhXbtVmKZhwcCq6Y9OKcY2HT8qg0Ui2ZlOMHmplvcLjOTWC+pLaTrHKdpbpmtOGdJYxhQWPp2qkrmqmupp28m9cmnypv8Au1Ts8ru3nOegFalug8sEqcmnZlKVupWjtA7/ADDNXY4Ttx6DFSBo1A9TTGnCDAzmj3hOTkQyQCRcEZ+tUptIhdWygyatm4Jk45FPZldM5x9anXqO7OO1XSYolIUc1y93p28kMxAFd7fqpkbc3096wb2AMSdtap2HoupzFtp8MbEqnOfvGte2IiYY4qFgEP3cVFLNtPBxSeoJnW6fckqdpA+tbEExA5xXEaPfN5hTdXSxXO5BzzWT0KLtzdZBrIu7xtpAPFS3U+Ae9Y11Ocn0qBN9Cve3LMMHAHrXKavNkkZzW7eyZXrXO6ggBz+NXDcxnOxymoISzk9e1fK/7X3iE6f4attNiwZb2QjHfaK+rNXfZ0A3YP64FfBX7Vfikal8RTaIweHTkMXXgMetfQ4Cnd3Z8pmdVW5TwyRVghbcDkDAIrNaXe+AOOwqe6uDM7DPyioxEVTzCMDHAPWvfR8q1cfZStC5YNj19/au28PawltFAjKESTI3f3elcYls0QTeMbyMD2rSg814UgjG87zgj070mrgzvbr9wjeYQy7chvrWU85jt1G47D3Bq9ZzfatKZGXMsIwM/wAQ7/lWddhQSmP3Y6Cs3EcX3KhmKzsV+Rum7rk02W/D8fKsgPOF+9TkiEzEA7WP3c+tZ1zFJ5jxk7ZlrJaM1JZ7NJpdy7VkxnjpUM2nXMMZPllgxyHHNK9uyplyC55JBqKwvLj7fHCJisZcA7jwK1CT0Pqz9mDXzqPhya3kXbJbvgA+le7EZGB0PIzXzH+zrqAh8Uahp28EyAYKdBx619LW8zE+WRjaO/Wg4J7ltTtUD0pN2GzTlGeT1prnnpQZkqtnFSFqhQ421ITQBKDTqiDigP7UATBsVJwRnIqvxT16daTAmxx1oU0wcilUGs2hNXJKKYZAPWlDZHShRCyHr1pc00HvSc1aQx+6k3UlFNgeFFtpz60hfimyE7vpSdaZd9RTzTScClzTW6U0G4xuaaTilbmmmmTYQnIqMnnpT6YwwaYiNjnNNBpxB5poGOaAGE9aZ1px70wmgaEPUVBeqGibbyQM1IwLVDOnyOAeWGKQ7Emk/vbfGOvXNaSfLbsOOOBWTobdUzkr1rZAHknjJpWDbY8u+JwVdbhAXAMfOe5rgBIYLpJQNuTtOK9V+IumiVluemY9u49jXmLgzxT26n94mCrdwRSN47anqNzYLNpVnInN0kQeMYzvHcV5z4zsoJv9IgjMfmc4x0Ydc13vgfWhfacqXBVJguOeoYen61z/AI9tEgnhjjYgSgt5RGMGiwI82RDgDGcd+1XLRFiQSc+Yxxn0qKa3ZJSDlcdQK0LK0y/UMMcA+tVYu5ZtLLzWDbt3lnceK6nSdPzIk+4ghgxTplaxtIiaGbIXJc4KnvXbaZaxCFyPmGMsT/D7VvGJLehomwhu9NlVlKRyKyc9Mms/wLr48JfEDSdUX92LK6UMPWM8Guuskih0doS4kAAfGOoJArgtas1TXLnBKGRd3H8Oe9OpC6sTRm1O5+o+h38Oo2dvPE2YplEisOmCOK6CPjA6ivmz9kv4nL4v8BR6bcS/8TPRz5Eqs3JT+Fq+ibSYvGCc18PiqfLVZ+j4Sop0krmgAQRjpVqN+VHWqi5K9easxMPMY/TFch2aIp+IfCcOv2qK7uhByHjOCp7EV55J4u1XwNfNp+pp50YP7u5H8S9sjua9ltEDp94jNef/ABV8MLqlibhYz50Pf1FbQszrw6jOolLYy4fipFIdylSfQVYHxOkJIiIUdsmvFLnTpAxaCYwsDgiiK51W3U4aKVR69a15EfbUcsptKSWjPcR8RbqV1+cAfStvS/iAvmbbpAI8431882Hjn7I2y9t5IMHBfqtdTa+KbO9jHlTxsuOzVLVjSplcJKyie6P4jsLjJiuEH41BP4t0+yj/AHtyn514k+tdQrZ/3TVCbUBKfnkyfQHNXa5zRyiPVnpus/Ee0kudkYzGP4wKzR44hlkA83Ct615tqOt21pDh5VTHPJArm5/GFmWbFynJ6DmocOxtLK6XLtqe6jWoLjlZEb6Go7m7RV3HGB1NeER+MVVv3VyFxzzmlm+JmpLiK0ga5foN/Sl7OS3PncTgXTV4u57rp12pud4bA612FjIrR5znivIfhpF4i1W8SbUTH5DjIRFxiva3skt7UALtbHOK5Z6Ox4/M1oylcTbUPH51kXL7wTVu7l2nBOc1m3EpC4HNKxDkUrpiRxyKx9QwR1rWnGxc/oawdUmVQzZAA61pCLb0OarJJXZyfiS8htrOeWVtojVpCfZeT/IV+XnxI1v/AISTxnrN3Ed0ct075U/eya+2f2q/HUvhvwHNZ2cuy/1OX7NGVPzBer/oRXw+mlpGPlDSzseMDoPU19bgabjHU+FzCsqk7LoZlpprXFwQV4UZ9qsNp63F6idUQ7nPYe1bq2AhtgseQT/Ef4vpT4tPMMRjAG5jlm9favT5TzOYwpbUyyGVRtjBIXd2rR0S02upzjaT83bJq7daXJJJFbLgchmwent9a27bTIoOWOyBMEkdzTsK5W2GKaI8DnHFV5VB3iQZ+Y5x2P8AhWreQOCshIXf90e1ZkpXz3AHI656E1MloOLMq5BjJMY6HhvSopmklG93Vnx8xA6mrLQB5GdvuKDkCr2neGr5gkwQHavmeU/Rx6D3rmsbpmRdAW0apKyFnXIx1qlFMUnSTyYm3MOvIFO1uSY3sqtEInyPlA4UUhhOxc5ZAOdvaqG2rHrvwJuLo+MVZGQIHB+Rdu6vrhOZVkzglcMPSvlv9m7TlN3c324skR2rnsTX09CxZUyeSOaDgluafmA4xRnNQIMEU8nAoIJQ3Sn7h61Ep4p33ugFAEm/igNTMEDmlBAFAEuTTleo91OBxQBYV6chJquj5NTowzigAbvSqcCkPzUvagBwOaU01aUMB0oABz7U8dKYHD+1LkD+KpkB4XIOtMp7tmmVRpYTBzTGBqWkOMUAiA8daY+etPlPNMJytUhNaEfNB6c0ZxTXNMgCeKhZ8Gn545qNiKAGlwaawpxAAqMk0DBsjio3Q4pxOaikY9KAuR6K4Fy/pzW2rY+U+lc/bj7LqgP/ACzccfWuhUjbz1A5pMaZS1myS6tikihkcYwfXtXhuvac2jazFNz5bSYf0HNe66xKTZfLklCHGPrXm/iayS/Do2NjSEsfT0qTVPQaI1sNRTosNztdGU9DWJ4v1WW98RRx53eSuG9q1dNiewH9naipnhkT9zKOqrkfyrl9TiJ16eLzQ+3CJIv8YNNblIhvoBLKzDG7rx3FSWdoxj8wD5hxgU29haIJGGxJHyv+0O9b3hyFL1R5g+V1BIHUVqtyhtinmGAhckNkjvXZ+HmhN15DjcHJ+Yevoa5rWbOTQbiCZEPl9D7itu1RJYy9rlTnf5noe9dMTCTsdobVbaNlKkhjjb6CuQ8RIbiWQk/PAcFsYJX3roprmWbTdxO59u3K9c4zXI3l45tpRjc5HzE9TTm0kEF1Nf4X/EdvhV4/0vW4mIsbwi2v4s8FScBse1fpP4f1WG/t4Z4JBJDJGrKynIKkZBr8otU8q7tIPlG0gq5PRT619Pfsm/tDLa/ZfBWvytuU7bG7Y8MOyGvBxuFU1zLc+iwGK5HyvY+4oXDAY7VZtxulGP1rHs5xJGpBzkA1pRS4dSDXzbgfXRmpK6N6L7mfSk1O2Go2ZXAwVwabaHelWl4GwfrWUdDdSd7nzl438MyaDqhdFJhduT2rHtbdbpRg4PevoDxj4Zi1a2MZVTkcfWvDL/Srrw3fNFLG2w/dPauyNRPc/QcmzCNWmqUnqQ3PhoSKSIhIuPugd6yNU8CxSruSGS3b1hO2u70nVoQmGAz6GtT+0LWRfmANaqCmfSvETj9k8OPgu+tpGEWp3cQPT5s09fBV46f6RrN25/3gP5V7K8VldKxMQXHQ1SurW0EH3BkU3B9DSNeEvsnkP/CBWSyM9w09y4H3pJCRUcugWsCjZCigdwtd7rVxbQjCgKNuM1xd1PJdytDBl+cfLUXUTOtUio88loY01tCj7FQFieDiu28BeD2vrxZ54yQMbeOKt+E/AhmZJbldxPOCOley+HdAi02JQEGcVlOdz89zHGRm3GkafhnS1skBYAbR2q5rGohF2IetRXF0LeNgPlOOlYtzc78E81yuN9T5xu+4yWUNls9KpyTd+9Na4BbBHB61UuLoIDxS5bmbbSGX1wF5LY46VwXinX47O3nklcJHGpcuTgKB1ra1vVhnap5r5O/al+La6db/APCLadIZLy5P+lPGc+UnofrXqYTDuckzxcZiOWDTPGPjd8SW+IXio3MEZjtbcGK0izkAbjl/qf6VzehaQfJaV/mYjPPanaHppC+feqqxqP3Y7mtQXqu2xQBnoqdSPevrIw5EkfGyak+YrLYgSeYTtTH3iP5VNbxpDA9zIihekaEd/WrttASHaXlR/D1xVmKxF6VklXZEvQHpWlzFyMjTtNeRnnKBmPLOR0q7PFCwOB0GS3ar1/PBAM7tiKOQvQ1kRzPf3SRBCiH5mUDoOwP1pEt9R9+PKSJUT5cZAPJHvWP9j+1fNyC/I961tSidZMBi0j4VVHp/9an3yRWZKRY8uJRlz+uKe6sEWc5qaLp9ugYYZpF+YHtmu51mWHw7pm5JFe4dQYxnPJWvL9avFuZmmLYH3Ujrc0qzn1+4ilvnY24IJA9AOlcj3N0yrdaK7WL3U523Ug4Dd+9UNA05tb1qKyQmNpCF5/Wuo8TyteXIjjB8kYEYA5GOBW94C8Kmx8XaNcPH5qySYY56NSBs9a+Fnw9Hhq8kLK6xsgx6HHcivXoFBwenpVO1UBl6HHGPSr23EmB0oOaTuywWwKQtuFM24NOUGgklVuKerUxRUgwKAF3Uu6m55ooAcDzUu4VCOTxSgnOKAJ4xjmnq3NRKSVxQCRQBYBxS8+lRBiacmc80ASKcU6mUmfagB5+YUmKaMilyfSoepMjxLimE4JpxIpjc1ZuxuTmkJOKWmNkUCuRt96kYjHFB60000JsbjPNNb6UpyBTGJqiAyMVHIOM0ufl96iJYn2oAAc0wjtTgevNIetAEfXNMYbqcTz7UHB6UgIZYhI6EfwsGBrUtZTLuPZuD7Gs08EfWrkB27k6HrQwZLICoZRySpFcHfxBNb8l1/dO+V5613jf6s4PzEHBriNaUJ4gihZgxV1IYfSpNIkep2MoiKJ88lvueL/aB6rmuMMIupppo4/LIwVP93H8P4Z616dqCPApJQsFGTj0rz7Ubd7O4nmj3eTL99QPve9UkaoybwxSRPJuxJH0Tv6Hn8adpV99knimTOEbY6Us9udrMgyrZwGHrWVYo5aQEkORgKOma0W43sevXapeaNFNJDugAALZ5rH0W7FhrdxZXTFIpgNmB8pGOSKufDTVYtX0mSwmYFwpAWTvWFeqYS8cshN3asY4mbrtHSuhWsc1rs6m58/T1WdZcouY2jJ59jXJ39w0NzId37tv4e4NaUOtR6nbFZ87gRubpz0rB1j93OYdwc5wHBzxWM5X0OiCsh8J8y3uLckAuuEI7EcisoXz2t/FMkjxkkOkiHBjdfT8adp1xveZFDNJHyD9KZcWq3AuI42wrgTxsD93PUVCV9GWnbVH6E/sr/HOP4meHjpl9Mv8AbunRKJRn/XR9Aw+nevoaCUNGGBzxkV+Qnw38e6l8O/E1jrukXBjvrNyWjI+WePurV+pPwk+JOmfE/wAHWOu6XIfJnUCaBj80MgHKH6HmvnsZh+R80T6vAYtVY8rPT9Hut6c9q2QiuNwrl7GUxNkdCa6W0cPGOK8VrufQRaYs0HnoRjGOlc14i8IW+sWrpJGrseh7rXYKc44o8hTk9PaosbwlKDvF2Pn3WPAl7o0hMH72Idsc1jvDdQA74iv1FfQ17pySEkjk9jWHdaJbyFsojH3WqvLufV0M6qRVpanibXkgjYAEVnS3MzAhdzH2BNeyS+GrRpOYY29gKVPC9tkGO3VfoKaqTR3vPUl8J4e3hy91YgFWVCecjtXW+G/h9FZhWMWWJ5Nemx+HUjAKx8j2q/DYBB90Cnz3PncZmdbE6J2RhaZokVuoAXBArQkbyRwORWg8YjG7oBxms2/lwhbHNSnc8Nsy9Tnd8EY685rGmuyWwCMCptWvgI8/mawvtHU1rYxc7FmSfDcnBrF1XUPLXhufSlv79VHByem0da8G+P3x9sfhlYNaWzJda7Mv7q2Bz5X+03p9K3o0XOWh5+IrqnHVjPj18b7b4eaS1vZsl1r9yNtvbg5CA/xt6Y7V8XxTz3t/Pq2pSvdajcOZHkb+Mn/Cqeqa9feINUutR1Sc3N1O3mOznp6Y9APSpLJ2mBec7IT0YdX+nvX0+HpqmkkfI16rqPU1hez3JXLEqOGwvC1uWFkZVGBtRhywHzGqul6ZLqIRP+Pe1XkrkZb3Y1tJex28LpEyuoODKOn4V36nBPQigskinzJkQx8hQfvH3o1W+SGL96TFGeQuetV3vJ5U2xqQgPMjDr9KwryO61G5wGY468ZwP6UGfLfUr3mpteyDcpeNThI1/jPYV0GmQf2fbMXffcMN0jE8BvSse1t0iuEMbFnHHI6DuR71qGOW7ZI4ByT8vcN6596At0NDS4GvJ/MZ12qNxKckntj+tYPiFRK5jjbEan5mTnca6PVblPDelrYoiyX84yzLyqD6isG20s3YIQs5AzvbgUDjoZUWnQOqyShggYYQL8zVuWl3bW9ndyFSsikCJF9PemXts1pbRlSqzkY3ZpfDegSa/q0VjCGOQSxJ5YgZ/wAaxkrGy2LmkaDdeIpLS6RfKCk89FUjua9o8F+Dbazu7O4RDcsmWa4Y/wAXsPSrnhP4f2ek2tvBIGl4DsGPAPvjrXewqkOzaqqijG0DAH0xWRhOWpagQIMYBbqTjmn7sAk8UxX5LngHpTiTMo4oM07ixymQbugFToSarheAOlSxgAZzQMnGRS5NRg5IqSgCRTxS0wdKXJoAcvDZpw+9mo1zUlAEopaiVqkz70APUgYpVY0ynKQO9AEwNJuqPJpc+9AElJuFKOlJipaE1c8RIySe1NzSuxH0pp+7VG4jdMiomY4qXtULU0ZMaabTiaZTsIRj2pjd6c5z/jUfQUwGMcU08qaV+aiZiDjrQAg6mlPpSZxTGf5xQAh461HuxTmc5PGaiaTnGMUAP3ZwO+atKrfaBK3CkYrP5BHc54rTkUvbAn+H5qQE5X58g8qORXFXtureIRFGuGTk7u+a6iK4Lx7lP3utcle3og1D7WASzErke1CWppFF3xBfmILH5uFIw4rm9RUzgnP3E6HjipZLj+1JY3J+SE5dyeT7VQ1WWQyN0VXGBWuyNrHJSlhcFFdtsmRgHpUFpcC3vHBz6HPpTHeSy1SKN+Mk0y8JhO8HIYnmobsM6DwvqLaFrInQ7klO07uy+oro/Hs8Rniu4wNr4LMvX8a88tZpn+ct82Dj2rpNTumvNATcdzRkc56+tXzaEpamdbahMsssZJQiTGT0AIyKtm4WUou5mBPYViiQH7IwfcXGx8euetWYGMV7FCHJUZ5rG9y+UnWZ7DVY3gbB37WDdGU1cuoxBNtjUjy5Ny46bT1FRapGDsdQMhsFjSpcC5iGSQxUgVpEGrFfVU+zym5iQFSASo/l+Ne5fslfGF/hv45XT7ybboeryCOUFuI5+z/SvHABdaVcROuGVSMntgrj9KyLSOSFyrZRo23Bs9zWdaCnCzNaNV0ZXR+z2nXa3Mauj7gQCCpyD3rp9On3gZODXxV+yJ+0KNct7fwjrdyRqkCYtJpDxOg7H/aFfX2n3hZ1+Y/jXyFek4PU+4wtVVIXW518B3YqYfIpzzVKwk3VpBd4rlPRuUJQGJH8XpWbPCzscdK2JbdS6tyCvb1qNrYEZxV6nRCdjnhZFZCRxVqKPy0HGTWi0WGwRUFxEdpwKRvzKTuNUK3QjPpUMsOFY44p1tEyyEmorucqGXPFZ2M6iSWhm3UuAy9q5/ULgqp3HFaN/N5bHDZz1B7Vy2sXmCQDg1rCNzz5uxk6nP5jBc8VmXF2sMZyQDTb26Iy7HgdTXFeKdeaFHEZ+Yjg+1dcY8zscVSdlc4f48fGYeA9CmOnsH1VlKRnqsZP8R96+ANV1W71vUrjUL25e7vJ2LSTSHJJz0r1z9ojWnk1cW6SM3mMS4JryG2tU8wGYkKOgHU171GCglY+WxNRzbuWLKAyycx/aXPKxH7o9ya6rT7WKN1kunWeZeSo4Rfb2+tYcVwsBVIz5e8446muo07RzIgDg7j0XHH1PrXbC9zzZMvxztqm9YgAi9X6KPYetatrptvFiWdQzdo+n6CrmlWAtLdcohkYYVTwD7gelZWo6lJN5kdoVBQYkuCOB7CuxbHM/fdit4jvY4kILJGFH+rQ8gf41xk2pT3ZMduXWFjj5fvsfeprrE0zAM0rluWbrW9pOkxwSKVX98wyT/dFSyvhVhNM0treNFYeZO+OAflUeproIP8AiTWcbookmeQjLDBP+6Ow/nSLPHBAREOXGeOrAetZ8vn3CvKzNNKQNo7KB0A/M0Gd7lW6sZLmRpGmBklbcAOgHpWvZ2EoVWlIGBgL2+tNtdNaGZJLyZYYFUtgdTxT59QeaJktsRxj+JzyRQNMxtZkQxMAS7jOwY44qX4Zal9h8TafevITHMT5megGcH+dZVzMhtG2l2WGXc3vniofDzx27XVowdmBd4XH8ORkfyrOSNOh9iWUShDGG3SrjLZ4IPT9MVJgxOGJzjtWB4C1WLUPD2ny5+ZokUsTznGDn8a6CONgzA885BrAwauXIpBOMsMD0qaPCnHQVViLKcdqtLgnNAkrDqenSo6UMQcUDJxwKNx96YrU/tQA4McYpQDTKkXpQA9T1pck8Y/GmA1JnNADlUetPyKjHWl70ATDmikXpTS/NAEwo61GHzTlagCQNx1o30zIopMDxXORSMOKYSeaQsRTNhS+0YxVeRsmpXYYPNR4FNGLeowmlpH4NNZsAY5qhjWfbxiomctyOlPZc5phwB7UDsRliDikJHrTZW54pgNAWHbjzziiVYDbBlbMueRUbckUxx1xQFhGbn/Ck3/IykcnofSkzikPJoCw0MVXb3z1rSQkwhQc5GDWa6574rStpAIVB4A5JNAbGNq+oJo9rIFOGYEDNcHqOqpLp8OwthJGLe5q38QtSkutVjjXKJjAWsIxmTTMoOFkyw70I3jsX7a8VgqqgAf5pSxxmob27SSRbdiS7HKBfYHiqrSHedy4hUZHvVZI3urmBo2J5LbR94Y6VdyjN16z3yWk6DErkgMfX0qrdnyrMLKh3A85rWvZlvm2KA3lNuwD0Y/erHS+L28sU6+aN2Bng1nICrZzFGCAct/Kthbz/QtoQABmySax4mikul2F1Rexq9EwlSSMMABzyakszrW7LSMmwsSSB/snFbMDo00DkbWZACB61h7fsl3Ix+UCRSM961baWK42SkMGyOccfSgDV1IkwxgZBB5z3pi3CixV0x5m7OBT71t+mRzKRjeQTUCoHswAg+U8sK1iRJm7Yss1wgxuimiDKo9wVI/8ez+FZui2xkmvbeXcJBlMEc5U1b0xs2kTKcZDJn0yOP1rT02JpNYF0cBriMM3HBJoqaRHT1epBpMl5p08d1p85gvrBhPFKrYbIOcV+hf7Ovx6t/id4btjdEQa1bIEuIyf9Z/tCvgS8tfsPiS0dcNHNgEdjXb/AAgv7nw98QJrW1la2TeWQqfvd8fSvHxFJTiezhK/s52Wx+qujakJ1XHJYflXRQz+1eCfDfx89zZW7XeUkYYJHT617DZa1HcojRv19a+bkuV2Z9lDlnG6Z0mxZY9xGKqvAUPqKgj1btuGPSrAvBMK0TXUEpIY0I25PX1qu0Yepmk+brhaQzxqrdMVMtNjRXRVkiWNGZTg4rndSufcZq9qWsIgZenbNctf6goVnJ4qDSTsipqFztVyTiuJ1fU8ykDmresaw87lIiWzWXbacJmLzE7uoFaJ8u5wTdzIuzJdKwPyp1NcB8Q7+HSLCWaVgoWP5R3NemX6GON/l/IV85/HbXjHptyqn5iMfTFd+H96SZ5te0YNnyl8RdWbWfEt1O7FlHC8Vygm35VRvYnjHWr9/Kby9lfdjLHJq/oViIpwqKGdz8pI+cn2r6OELnydSd2XfD3hWZh5sriIEZLzDhR7V21haxWtmwtXJyMyXc/Qf7oqOx0lg+NRZnwvy2ydT/vUav8Au4InvnWO0QfLbRnAH1NdsYqJxN3M+7vjeRSJHLJFaLzLO5w0g9F9BXPXusS6uDaWJENmvAwMEmq2o6jJrDrEpK2pbbHGvGef1rQsLNGlKxJu2DbxVNkiaZYfZmAZczH75J6D61Ne6osWI4CWBbaWXn9ah1xkjiS2hdi+cu/Yn0zVfSoWR1JQbVyeTS3He50+mW6rCGkRmZjvJY4Cj0qaS5FiGO0zXBJb5D8qg9BUdhDJeQxq7Es5LDA44qxcRqYJgoxGu1cgck55/Diq5TPUzrq+mYb5wJco2I+3Sm6WrTGVZBuRoyFHofSpo9Pa4eJFGW+Ylu2BzTtIEaXMm59qDPUelJoqJhSMIbXUVb77oBGo7MD3qnpcpe6V8ESwFVZAfvL1B/nVm9tci92tlcFueOtZOnXDC7t5MBXK+W/uPWs2jY+iPghqo+yXumSkGSGUypu/utyBXrBkJcAngV83fC64ubfxXCkkgEc4KZz1x0r6GtpdqqroQ/Q5rnZm9C8zYzg5qxAf3QNUg/lk55B6VaV9qrxSIdyYGnA1Gsm7jFPUYHSgEgVjmpUbNRr3oztPFA7EucU9WNRq3HNOBwKBMlpVYiosmnbiRQIkVsHmpc1D/DSqxHWgCfdmm0qkbcUY9qAFU4608EVFkU8UAP7UZpAeKTJpMDxWo3bHtTi3JqKR8kUzRuwhORSHikYYxTXfgZpozHOQahLYNG/56YxySaoL2HbuKrlju61IWwDntUO7OT3oKuKzDNNJFN2syklSfemqvBOD170guKTimHkcUhmRchnC1C97BGPmkXFGoyRutRSNtI5qhPr1jA+POAJ7VVm8SWucGURKf4iMmqSbHY15LgKh9SOKzNb8Wx6Xp4UH99jAA6/lVSe/e9t/NimjhjHG+U8n6CsJra2guEnlc3QyfmY/0quUOW5nmKbWH824d44mO4t0ZvbFWHS3ht5IXPlxs2cKPmqS5v47hJI7ZACOjHr+FZF7IYkAnYlyOrdRSsarTQg1O8+0SqnaP5AnTK+9Qrvsx5hJMpGEU8BR/Wqtic3TSsd4GS30qw0wmRpW5VgdoJ4FDKMy0uANQnYnGVGQOgqpDl97k4AY49atWAiE+yQEvK3QDpVWfchkUDKAnhfSs27jQ2HbN5gEZIHRh/Wr0Nm1vBveQFcZG3nPpWTFMGj+TcgBHPrWreXOy2jiL7WxuJA4FIoydVuXuHkmPDEgYxwK0tInKxWqkAqQST2JrFu5MEgMG3cZNatkWighRl/eLwQPSgDa3s9sillETOcAmrNsn+hyRnHJyazHAWCDnnzBgVqo5WFiAOprZEtDbIOqkdNkgwM9Rnmut8NKZdQ8nAK28nU90I4rmECi0cKcNuJ3ehx0rr/BsX2i7mlD4P2cY9CR0pVNhwXvaD/E1sA0cyIS0MgKt261taMG07x9pEobPngHJOOfSma3p+fCU85VjIsiszf3Rnr9Ki1CXyrrQLtX3qjKdwrikrxsd6XJJeZ9ufD/AHPYgF8FAe1dx4L1K92XSm4LGO4YL/u15r4Cu2h0tZ1bcJo1YV6j4OtDteQL/rf3mK+Wmve1PqaTaV0dnBrzoMSA59RV6HxEQMMxX8Kp2tgXhUhcH061aNkm3DqA1c7Vz0PbMsf8JIjIf3oqhfeKo1UqshPHaoLm3ReAorOl08Ft2Bgc0rsp1fIrX2urL1zXNatqUswKKMJ7Gt69tgAzbfwrn7xGwPl68U9TJzuUtPg8189QK1hbhBkKcHvijS7LZ261qyRhINuOlOKb3MHqcXrjBIJcdQK+L/2gdaY+fajDEudrA9K+y/FMy20Ur424HWviL4+x/abqOVlx50jHjqcHGB+VevhF76PKxj5YM8W0jTZri5JZBI543twK7ewsBo8TCHBmkXBuWwXH+yo7fWqVk5t4wsYEbsAMryQPf0NdHDGLCATzEhVH3n5LV9XGKWp8hKTbG2izQWoMsJQY3Ek7nNcN4jvhqd0LYMxiyS7A9/T+Vamp6zeXZlLS7FbiNV9KzbWM20ZfYMuej9aq5NlYj0yxacBI4wN33m7cdAK1pZ205GtlQIVAGVGeT70zTkT7QUEgWPafMIP3QOePenwRbmkuMswUkqG6GkStTBv4w2opEWAdOvcnPtWlp9qsxOccnbuUEmq+lRfadSnllyXzgE10llaEOQBtAbPAqkBc0gRRXqpFHIfKQ7mLdfwp1/G08DPuxE5OAPUUaYj3k85RgnykZ/nmo5C04eNGIEDZBx69a0sMpqDFbRSRxFh1znqKfYRRiyu5ydpiyBg9aW+ZbZYAp5Ubck9ac5Fvp025RkknHrUvQFucxeXqXMUrKG3bcGsC4RtpkHMiHIYdlxn+la9gx8udVUDcSMntWVfXfkQW7MwGRtIA6kZGP1rBs1sdx4Z1X/RLC+UhsTJweAB0OT2r6SttREqQToAY5hux/d9vevkLQL37NZ3NqXB8zmPB4BBBH9a+i/BWqfatHgdSwKxAmM+vqKweomj0aCZbpSEGVHep485wTmszSp8QgoPlbqO4NaWduDnk0iCdDgjmp93A5qsrZGR0709X4oCxMhp2ajD0b+aAsTjoKdVdJS2RT0lLEigzZKG+apF61Dn3p4egRJuo3VGGp2/3oAmjbaOaXeai30eZQBLvNP3VCtO3UASq3B5qQEY61XDqPejfSYHi2/5qhZ8E80FsOailcRIWchV/vGqRbuOeXjriodztt2jcCcZNc5rXjjTtKLIrieRRnIPeuHv/AIj6lqhEdkj9f+WY6Cockty4QbZ6pd3kFnC0k06RgNjk81z994+0jTdwa684nsorzB7TWtbmZri4YKW2gk4Cgc81Z07wFLqrr5f2q6lckBLeEtnn1rJ1oLqdkcLJ6nSX3xhtYFIiiLD371kP8YrhgTFaqQemTWi37PXiSSAzvpb2MK8+bctt/SsS7+FTWZw1wGfPKQjcPzqfrEO5qsLJdBsvxV1ORGHkKm7owPSqV18RtZllC+YoUDqppzfDDWAC9vYTypn7xXHFOg+FWtzBs2csfcDYaPbwezH9XkuhR/4T3UpUy8hXtzVaTxbqE3/L2wx2ArqYPgxr7xBjauPcripl+CesucPCzewQ5o9tH+Yf1aT6HJQeIp5jh5PMkxkFlzV8akXZfMlHIyRjGK7G0+AGvToPKhZTjgBcGrK/s6+Jj96xlIHUt3prEQ/mE8LNdDnLfULGRYxNM23nBFWDJbXBHkEuPVuBWhdfBbxVpTEnTZDF6hc1jnQtY0eRluNNYnuhOOK3jWg38RjKjNLYuWtxEzvAXUSMjcKPQetc3JdSsCioql+C7cnAq7BqstvcL/ogjG4jLdRxWaJpLi5zsChcrgc/jW3NFrRmPJJblm3t44rfJYs0rAegxVfVbhIUKxgbFJwvpVprGa5jBd8Io7DFYd2Qk4iXMgQ5kOec9jWbnfQfKxlkShlujkhEBOe2agkbbbjBBUDGT3rQkVfKEJbcZRubFUrq3JA2rsjHGexpF2sitbmNsZtxgd1NTXMpMIyMZPP0qAOjHylU7j/dqLUp8ARqduFxSbsDVlcquUmlRSMKp4I7muiiZV8vHzOnBPrXP2kZQozrznit1cLGOdvFMSRaVlMVsT1afvWxKCS5XB2ZBX3rFgCzXNmg+X5zye9dE67Li64G0rtJ961gyZFa5/eadH5aBGdyfxArtfCNqJYP3YZhOytHz/D3FcYVKPpwJI8n7644ORXrHw60sXNxEyrmNDujA7LU1XpY6MNFykdh8RrCG28GRNEohlACME6MuOh968wv5lNhpKYIZXGR7V6Z8SrxZdGjteB5f7xvrXm15ie8tiMNjaVUetc8FpqehiElNLsfYHw8SW+stKsYT8ixo0jegxX0FodkUhVlXYAVC/7teB/BK3m0bSrOW/Qq9xjbI3T2FfQmnXAkZdjfuxXy1eD5tD6OleMVdbm9aBl21aeJmG4jNU7aXfFkfeWrAusoykHNc/K1ubEE1oHNVrmBYk6VaafaaoX0hfPzdqaVy9WZN+w547Vzd1G084XGAK6GWNpDz0qEWYV87c1fKc5Dp0KxIO/FM1CQYbBNaSqsaYxWVqR2q2Kdhp3PMvH7M1lMgJ3EHB9K+IfifqravrJjSXzZ7dtg9EJ4zj1r7W8fXSQ2s+QGdlO3n0Br4hvbM/2vumISR5Xklf0X1r2sBC71PEzB8sbFbTdKjsoA9x1TkyE/erI1fXX1KfyYOFHGT0qTWdZGo3XkR5+xxKSwXq3YD696gs7XDoNo+VeB7+hr6PY+Wa6jYLXYQzD5sAbm6ZqNgDdnklV7+9S3zESpEM/IN7EetMt4SJAjDBHJpGfUmEG222qvmSznaG6bfU1S1zUvIs2jJZUZtvmL3wOtWp2Bd3j3ABSEP90etc3rly0sotmwEVeB70rmse50Xh2ElRMV44PPetr7Q6LKVHzEnj0rL8OyCOz8lv7gwa0Q6yx4Rv3u7Bq4kPVk2lziGC4Cn5ySSfrSuwitmljJy8m1qpWWFuHCncSCCKt3pRLfZnAV85I69K2EZ+qHzHVMEn27U/VQEUqOR5S5JPeoYpFurmSUNkLgfhTdYnUQxxkfNswffFTP4RxXvHOW7+VDOAc7jWVqZzZRkrlEkcE+hx3q5hpPP2E5K8D05qtNcqkUmfnjYkSA/hzXA2dLRX0m6RZQm4bto249a98+GWqfadIETBA8fyllPOO1fOYtDaoJmyQJCeD2rtfD2pyWKCe2uZLdyASF5yvaoJcbn05p188bKzHc4OHUcADtXSRzFvmTlSOhr5x03x1rVjclpWjmUcgnjcK9G0T4r6dLCgvVltZDwQeQauxm4tHqCS4H+FPVtxB7ViabrdjqKRSWlykyHqAea1fNC49DSsxFrdmkLVBHKD9akxnmkBOj5FSIcZzVYHABqTO/FBkyxmjf71Cr4+Wnb6BEwenK/NR5pRxQBP8AeoqPf8tG/igCTzDSZak3UbqAJB8tPGSKhWRalD8VN7geCajqsGmRCWV/nIJCV5h4j8az6pcG2gZ2/upH/WqGo6vda5Ow3Mqb2Xc3yDp0Hqa9J+GfwO1fxJiW3ikt7OU/Ndsvzv8A7Irmq11BHr0sO5HnOleEhczb9TlkEv3hBGu7af8Aar2Twv8ACPWtYtYpNPsodKsBjzbqdcM6+gzy34V7/wCBfghpnhGGKOSG2W4JBae9+eUn2Su91XwiZ4UjjMscIbP2iUbS3+yq+leJVxd9D16WGUdWeM+G/gn4P0IRXGrCTW7wkbLdz5cS/VBz+ddfBpGq6mstpolgukwpws0EQjCj+tej6B4Fjd2SS3W22jcCfnd/r6V0MWjW9sqQx/vJDkkbskc158qrfU9KFOKPDY/gzJqVwTr+p3eruf8AljvKxD3IGK6K2+D+kWG0R6LEeMB8Yr2C30LbKnlrsc9WcYrds/DCxZeQ7m6/NWftGa8iPDx8LLSQFF0iBlOOSSalb4XrbNvS1iiHUosXX8a9zNmsIwY+P9mmf2csoJK8ehqedvcTgux4mvg22jG0AIx52yR5Aol8Oww7fki9yFr1m+0facqgH071i3WmRShgIfmz8wprUrlSRwLeGra6GBt3f7J2moW0OXT+SDLEOqt2H1710134fUOSh2j0zjFUws9m2Jv3sPTKHO36iqSIvYwmt4ZVDQhSncY5rn/FfhfSZdPlmntIp3YYBMeGJrsNR0tJ9l5by4YNtCZwD9RXH63ezxXMUDRSRyYxt3cZPpXRT30M5a6M+ffG3whghja5jhALZZYcd/WvJoPCqQTzkFd2cNu9a+rfFs8Y067k2ZKxlEJbkcc4rxC20qIWscyQ4QtwzHlvevZozlax5lSnFs4HxXp8Gj6UNuWlYZdicDP+z7V5/DabpiRnc/PT+td748vhqdzDp0TbY0cq2Tg/j7Vii1C3eWMaKE8tUTkE+p/Wu+DbPKqNRZzvlSSyoGwgIIOOoqvdSnHkqP3K9FJ4Fb02nodqRyGO3UkF2PzSEdgPSsOfBJIjGzGMd8V1NWOdu7KTSogZ1TYQOx4rKnkLMTwWY8Zq3NPuDYUlPugCq8MZllXIGSdoHepauaWvoWrWPCq0udzEKAOlaU/+jzqgYMSOc9qgsyouVIHyQxkM3q1TRqk1ypwC23nFMVrIv6aomvrQvhQCduO5rorWPz5J2PTaSK5zSYfN1RWHCRqeK6exDQMGP3ip/KumOiuYt6jtFspNYvo4AN+47yfZe1e5+F7CPQo0GzYVTI5rz/4deHWKz6gU3JGdoO7ua9H8Suum2i8hGVR1964ajbdj28NT5I8zOT8U6ml0jrhjJKxyT0wKo+BtHk8QeLrW0Qbog4LH0FYGq6pm5YTPiNCTg9D9K95/Zu8EM7/2tcx5aRwYvde1TUmqdPU0oQeJr2R9P+G9Lhg0K1gaHfGke1VYdD61pWDXmhLI53XNlu6r95Ks6XB5duY/vMFzW9p1mohb+JdtfOylzO599LDxdNRS2Gabr8dwd0cmBWsl5ujGW3Vymo+B7lrhrrTrrypm+ZoW+61ZUmp6rojbb21dVX70o+ZTU6dTzJ4epHZHoBmWop3UrkVy9h4nhuh95VrZi1CF4htdWq1CxwTcokkq5HFRqMVIbiNhTTIijNLlZCncjmk2ZwM1z2qzgKSTitW9vY40J4/DrXJavqRuJPLQkk9FHWklc3gm9jzD4j30dppGp3TyAARmNCf4Mg5P8q+INe1G4FmkRbc6nbK/95uwr6h/aI1C80zR4rSRfI+1XHlMA3zEH2r5P1kuLqK3UEIjsx3dfWvewceU+czN3lyon0y1P2hVwWkKcbez9zWs0YhtXnyqD7kZ7se9VbQraW3zDEkmAD9RipNdYfYbazwWkVuvtXrnzTbZkh8AnAeSQ9e5P/1qt28YVZmcnGMtIf4apovm3yxrwQSEzTtUvj5X2NAwkPzOR05p2KWxBc3L3RaUjyox/q0XqwH3j/KubeX7dcmVgNgbIf1HatG7uW+ypGAApUxnPp/k1SYiCaOMBDH5ePesmzRLQ6rSJmltE2j5wvH+NaFtKq3ShMsQOSBnJrDsSYraMrlVwBx71oW80lvfSBGZQDtBxniqiQ4mvp8KS3cTxqQ6lmZSPvVLq0zJEN75BYkZHQnt+GKpQyGGWMoVfe33dvQ1av8AFwLSJH8wjl2IxtNdCZnezE0+yjhtSzEKGbk+9YutFZLUEMMxkkMO+a6J1BtSSQQGChgcYNYusxhYjHtAcIVKkYyRWc3dFp31OahdYJgzn5XjOTWFdWxkDIG5A+bHpWzZKLqXYSTHghv8Koo6JJGHABckMD2FcRvEpxzxy28ccqsrbhgY6jpmt7QZJbE4KZKsMqF6ishLQM80TnOxsLjuDzitfTZzp91b7stExxgjpSexstDrz4ne4tiF0gTmP5dmzB/GqkOtaowVF0GTb2CjoK9d8I6HBqSJNFIixTrkSSJkE+lel6R4HhmtjE8X2d8bcdQR3ZT2rhq13SPTpUFVWx8sS6zq+iyC6gtZ4MfMGKsu0+noa7Xw58bdThWJtTsHuLUn/WqMV6p8TfBtvpXhPU7qO58xraLI4znjg/nisP4VeEBP4Xt5pbRXS4jDiGUY3L0P8s1McZpqE8uUnY0NL+KOgagQq3Bt5GP8f+NdVaXkF6uYJlmXGcqazNW+CGjiCKXzVthOflVIslR9awL74X6too8zRNSZkHIIOHA9/atY4qEupyTy6dPY7vdtp4avKrD4oXGjzpZeI7R40OQL2JO4/vL/AFr0bTdRt9Zsvtdhcw3lvxl4n4H1HY10qV9jzJUnF2ZpK9KarJcYcAjrUzN71oZNWJFepPMzVdWzThjmkCVy0DzRuqGNqmoEG6nq/Soh1p1AhwK5NOD1HSbsVNhN2Mn4S/s6aX4bhgu9VkOs6ki/cC5VD6AHp7nvX0Povhq4ESRW0SWMYGMQrgY9M9q6bSfDtvZhNiKfUYwP/rmtuC1mn/dQr5a4HVelfE1K8pvc+/VNRVkYFr4YiUgLGvmDrcSHcR+NXRpMFtLG9wTfMDxiumh0XGFlfd79qttYwW8BVEAI5yK5W7l8py8pluJ0iS1MSTNsEi9U9zWhaaetpLIixhzjBkxz9av2say580hVbhSaehMbOsa+YBwxHagrYsRadARGMZ4+Zu9TSWZ3YRj7ZpLOTYhcHeDx9K0I1DAYOc00F2ZjQtGR5ig54yKhuIIwCAea31gyhz9azJoWYvKvKntTEmzFnAt0+b945+6fSqklpHIu4H94eWrc+zq68gfjVabTgfmjIyOwrRaGyehyF9bA27KpyM4rDubZXX7u3bxmuz1KFY2jJGNxwa5y8tyJniHQnOa0QmrnK3mlPMp8pgsuQdp9q4TXbn7RcSRzAGQHBLe1ehao0tk22MgSv8ik9D61xl1apHb3F4V82WRm+VhyD61rB2MJqx4v8SdfMEU1p5e6NRw317V5lrOvR6XpSsrl8J908YY16f498MuVsrdnJlmczyf7Ke/pXz7rsT6zr1xY25MtvC5MjDkL6c17NDXU8qqznrb7RJq6XjIEYOdxI3Dn2rWJW0keRlQljj5kyDj+VLpdsDfEKCUDZfcOOK0YNM/ti4eEHy4YyScDrXs04aXPDnLVnNy6ZNqEjSyOQhHJbjHoB7VzesKySbQuT0PsK9Q1CBYLcqo/1fbtnvXC39qrSSPIdxHOR/H7VtLYlO5yEluyJvO0EdB3xUcMRGX5GPukVqXGnS798inzDztPYdhS/YXEi5B2qN4z/EfSsTa5HBH9nyhIYyLmQ45PpUtrH5IcHgj7oqS4XYFZxmaYj8BmpzD5cS9yMkn1pjbLfhi3Yyyzk4JO2untYjPMFdd3yf1qnptm1vDEu3IcbsjtWppavveX5VUsEXPcDrXTLSJgleR6Xokw0jTZXRxFAWUMdvC/L/DXH+LvEwurlDG7CzQFUEhyzMe+OwqO71prhVt1dlto+eD19a5zT9K1Dxvr8dlpsMkt0S21E5VBu6mvPbUXdnttyqRVOG50Hgnwve+MPES2UMZliOFndukcfWvuf4ZeFIPD2lW1vAp8qJAE3dzXE/B74S2/grQ0Zj593MQZpu+ccgV7HpCJENq8YryMTV9ofXZZgvYLnludDAmwIP71dLp8W2Ndy/LXPWHzdegrp7dv3f8AwGvNPpUru5IwV5GaoJ4FnXa6hl9GqX+LrTwn8X3qVy9t0c/qHgfS9QKyLbmCQd4Tiub1DQNW0SdlgSW4th8wP8Qr0yyXzLokr8q0XC5nPPFbKTieZVoQqOyR5FHr0ySATFo27q/BrSt725u0Pkozn0UZrtdW8O2WousstsjOvfFZ/kJpEkBhQIPN7Ue0MI4A5WfTtRuQnnRtDGxwSx6e+Ks21ha6cFZEWRz1Zv6V1uuLv0+WT+MsK5O7CxooYkDaW4rSMrkzoKm7I+Gv2otanufii9o0jfZ4kDKp7NjqK8n8UWDw+IoZXV1WWHeM/SvS/wBpRftPxZncAgi3RQW9+/6VmfEjS1Nnp+oKu9Ci5I+tfQ0GlFM+CxcXKc2ziLeJZTApOVRvMbPooz/M1n6rfmSUSEYcAuSvQZNbToILe4MmYvMwiAe9cbrd79mVkYjazbDj2r0XorngpXEtLz/T5pTl4Y1249W65qs9yy3ILtmQtkn2PNRQyokbcEIDnPrVK4usQzTZOXO1fasXO5rYszy+bPIV5jxgD1qF8BY2JCs3z4boAPT3plgHNoVP3oyCc9+RS3LJc3CsTiMsVK+nNK4jagnMcNqpLksd2W681egPlzgAnht2DXPxXe+SFWbCo2M+3pV65nKXoZZMLjaBVpjex0yTB7USbusnI9MVYsj9ogkn3ZCyADd/SsZXkuYkjjIMowxX19q1rYxpDcxDLAR/uh6Z7fpWqlcwaV9i5fhXQStwoIzWTrEpuoijMBJ0Vx3HpWwCLi1DS4VGG/j1HWuf1GMSW+Pm+RjyKJagjBsoTbylgSSp+bHpWZqunul4GhyQGJXPp1rYjQxyFupIxj1qrqNqzxFVJRmPH1rmcTaLFjUxTwzAbt2Nw9fWr2qQpHJvtyyciTaOmD0rMs/N+wM+G3RsFzjqf85rp9LEVzGYpAu1vUdT6VDWhutj1T4U6tHcadFFOxjMP72LZ6+9fTfhOaO9Vby5KyREAb8cA45+lfHfg+3uIBOlswaeD5kC8EivuH4M7de8KwXUIR4pQFkV1z84GDn0rx8WtLnv4GdpWOJ/aEsY7rwRfNYYYLATM0fQp71U+G0MP/CuNDnSNQFLqs5OcDc3Fafx80F7bQdRawHlLcqllJCn3ck9TWH8HLUz/CHSrkSboVMglQnniUjgelcaipRO6U+WdzavtVg8K2stxcOGsrb965U5KsfbvXE2ekeIPifeXGpwtceGfDF22xQiYuJgOp/2RXSfD7wpdfFXxbfJ5ofw5pNwJCQv/HxOfuofULXv/iDwjFZaPhZ2G1cGONQMN7VzS5ab0OpXnufN958A/DlgrQ/aLy689Nii6l3ZPr7V5HrHwp1z4W6pcXXhy4ZGhBke1kyYpl64+tfW0HhmPUwuRLJkbS+TkfSsTxN4EH2aO3guZ5WBJmdzllX0xV08VJS1ZjWwVOotDxHwV4/tvGltIvk/YtSi+Wayl7EdWWusHygV5p8VPh1P4Y1KLXfD10xuojllJ5f1Brq/BHiuLxh4fivABHdJ8lxEeqSCvo6VWNSKsfHYjDyozszpI2qRX61WBqRWrc4rk26ndqipVeghokV6lD8VWL/NTt1MViffSbx/dqPNIPrSGkfX8ANnEHCR3BTsx6GrUF9eFNzWwRG5JU1zLXsiyKqAR7eTk5rRg1OcL8ybx/ez0r88sfo7WtzbW/3yfKu89Pm4AqwfNcDe689hWNHdefgGdcf3QuMVIsbiTMcxDe9AWLllIAHikhd9r5zU8cpt5dx/1bNzt7fWsee8urS5DScg9HUfzq5Z34uRIfM3L3xQJo1yEizJEcD+561LBeIFJQYYclTWWHHLLIxX1x0py7LrbhijdpB3+tMixvR3nmxFlwoPGCajO1I2G85b0rElM0Z2x4Y55PrVtrhOPkY7RyAeQady+UmmtWZzh+lVi21sDaH6fWtGIQyor7uT69vrUFzCkzttXIH8QPFVfSwuVs5XxF5kyiJIPmDZLBscVi6lZyLFmMhVUZLE5zXY6lZi2tJH2A55JJrIdVvP3YHlxd81aNLWRwl1bRXB3zmTaOhAzn6VyGpQfZ7p7ZVcW5+cSkc/SvVXhtbVJZHb93E2EQjlif8A9VeX/GjxND4H8J3Gp3JSOZ8iKMH5iSOOK3pLmZyVXaLPnL40+OUg1G5gso3a+ugLaNEOSyjjI/GuB/4R9/DHh+ZZAI7913uMZyT61f8Ah5pNz4u1uXXLuGSco7C2TOMkn+QqX4lawmmSSWUcomf+Ns5+buAa+goK1kjxKzsrnA2oW0iiDsTM5zlf89K24mSCJpEKx23Vmbhn9qxt8VpAbmdwZHGdqDlTVdby61orG37uOHrnhUHv6170VZI8OWrZNrVxLfphXAQH/Vpxx7mueuLbARgjSkHAjrTmik1JxbWJdoF++44Ln1J7Cl8qHT0IJWSQcb1Gcew96GrhHQzINKFzKzShhEmC+PvMeyisrUykt0TkKqnCog+6P8a6S7uDDE0bMFkQbtv9wHufeubhxN5rklzk4PTrWbjY2RRghe+uHcACNfkUN19KS7SRLuK1hYOZGA47AV0ml6Fc3sKR26eZO3G7HCj3PrWzF4R/syXz5SJGIxuxjp1xWcdXY1asrsjkX7PpccjfeRSvFV/MEVugkkCokfmnPXJqzdTC4Y2y4K8OT6CotB8I6h8QdeFrawP9nJw8nRQvbFa1JpKwUKUqkvdK2gafqvjfVU03S4DIsvAJHy/ifSvtD4HfB6y+HuhR7o1m1KSPdNc4+Yk9R9BUXwv+EuneAdOjSIA3bhfMlwCcnoo9BXtGlae0CrCgALLkivna9W7sj77LcAoNTnuJZ6UYAZoF+QjmM/rirkdshXfGDu7r3WtpEWIRIFDbR82KlbTgJPOVfLk/hx0ry5SPp4oh024WMbZBgV0UEgCLhs/LXOMC/wAsyeRITgHs3+FWInmtXXkunrSuaRidAjBzuxU6MAOtZlpe72weBWioLsoUVmpXdjWVkrmlZDbG74OWqu+XfNWvMMVv0xVBZtzHHHPerkzihG75h75yV9BWTqaCV7UY/wCWgNbB556n2qu9v5t3CuOnzfSoWptzOJV1dQtjLnsRXCaq4fODyF5rvvEZ22ijqSa4i+smC7gOSec966YOxx1febZ8PftJabNYfEsTlSyTW/GR1Hf+dX4tGPirwBOsTB5rRUdf9xua9F/ar8GvIul60iN5MQaOcj+EHvXnfwa1eCW6t7Z8LBMzWcqE/fAGUY/nj8K96jL92mfB4qny15QZ4/40uxawQxREPvJZgexzxzXnt1u1C/ihyCPvEkdD716R8XbKPR/EdzZw4aKCVgmOhHWvNknNpasX4mmPy8c4r1JSulY+alHllYh1BwdsEeA2doI7+tZd9810tqrHbDj8TV2JP9IaVuqAnJ7GobWQSL5kyqpY7snrmsQLvnfZhAW2qMFmP0FZdtcL8zZyoJbce5NXtQEa2rdGdlwBWLbSGSLyiuB2IoFY14nXGWHcMDV+colyJGJxgFEHc1n2A80LuP7pTzntUt5KJt9wh3sjAKemPwp3Cxv6Hcq1yTISJgCVPp7VuWk6G9DjJRhx7ev9K5bT5RFqVu+R5bgZJ9+tdVYRBopsIGZHyDnjb3raBm9C5cKYFMXUsNy49DVW6VHimMhK7l3ZHfirN5IBEjdWjIjbHYHoaq6rOotk4AJGGz61qybamI8ZeMSBRu4HFJNakwOyZfJyQf6VOgd4pFiCqeu6rkLxfZo0kypJ27u2amxS0MePZaotvI7BZPmLKPunsDWjoMgEhDqG2nJ9TVi90o70zH+7mGcKPu471BpcbwXgxkkdR3rGSNE7s7bwpc3Oh+I0LRq0ZIKNjPymvrv4E6xBoGsSWwmZbHVyJIYj92OTuD6V8f21zIt3BNCCZI+GD9MV9HfDm/S50PHnCG4gkWWMsf4vTNedVhzKx6tCpys9Q+L01rDPY20TR5luzczRN1YIp/TivFPDl7c6V+zPoktj8+qeILo2ls2OVBkJbA9ORzXbeNPF9r4gXUNTkjCTWWnTqWbpvCHgfWpP2fPD6+NfFPgHQ4VWbSvCmjpfTEDANzMN2P8AgIKj8K4JR5Y2PV5udntfwt+H8Xw48NadocW7zTGs00j9ZJMZJ/OvUJdC8+EXckYyV2hCP4jUN/aCDWBK6+cYwAhHQZNdZdyRNYMM7sEMMdc15M9z0ouyseaWdh/Zv2i3EYEqEmMY6msy70BlLySjdK3zdOld+2npGRPIMyk/d9qp6nasIiGTjB5x61gzdPQ+cfGPgmyGrOksKvAyM+7Hytzzn6V8vQJP4H+I2tSwRA6LLcC3dBwAw7/rX3H4/tIrbR7p5FAjijMrt0z6KPrXyz468OyQaHGlxH/pUkxuJWPVmb5h+QOK9XCTakjx8dBVIXNCCVZFVw25G+YGlDYbdXM+F9TLiS0kYZTlB6CukyV6ivpL31PjJLldiyrb1pFb5ah37AtShqTJTH+Z7fpTvNqHzB6U8Couyh6vk1MOgqqr81KH4rSPmM9W+HnjjUfEFqJbwQvKVLGQKQSfzr0e1u5JYVcnBbk4oor88P0UfPcOhDKdp9RxWhY3kzxglzmiigC9JM7R4JznrmsS0JVJXU7SWOccd6KKANPT7yRw4JyABx2qxpt5ImozxggLjOMUUUAbYOYY2/iz1qxbKN8rYG4tye/QUUUMFuPdQV244J5qWW1WPywjOi+gbiiipLM3Wox5MaEllLc5rMz+7IwMFfSiitoiMTXI1jNo4UZO7gjjtXxN+1xrV5c/ECHTZJ2a0j2RqmezHn8eKKK9DD7nmYguqo8KfDm9udOAingjMcbHnYMdvfmvnq+d5mhkkdpJJQXZnOSCfT0oor3qG6PAxJnCd8+bnLq2Aa0tWURHT7NRiCdd0gzyxyOtFFe2tjz5G9fwppdolnbjZEwLMTyzY6ZPeubsbh7pp5nIzAv7tFGFXPtRRTJW5j687GWK2yfLb94SOpP1qbT7dJp03KDtTIHbOaKKzkdMN0e6/Dnw5YDwzJP5I8wqZCfU571wGpahNqOoXBlIGwlFCDaAPTFFFc8fiO2svcOf0KBLy+uFlGRKVibH90ntX198KvB2laWwt7e2CRQRqyjuxxn5j3oorDEM9XKEro9Q0e3Q3QYjJB4zXY6f/rpH6sF4NFFfOy+I++gacCAIvqx5rTjUYUdqKK5pHTEettHcSusihwDnB5rIWVoNWNkuGgP94ZI/GiipNYmjLCsP3MjitTTXbK854ooqV8Qp/CaNwSYM5rNc420UU5EU9i1b9fwqW25u2PfFFFaRJqGb4lGYlHoa57UAAqDGee9FFaox6HnXxt0yDVvBWswXILRx2bTKAcfMOlfDng+6ktbm5ERC7cEEdQfWiivbwh8TmulZGT8X4gL1GyxZoFYsTzk4rzDWmIu4QOiKMflRRXpx2Plq/wARRvXKxkf3uppUG+fB+6qcAdKKKGc43UOgPfKis6BRFdSqOQDgZoopDL1xIQoj/gCFse9T2y+dp6zt/rCnUUUUAWYmzAGIBKtxkfSu10RQJCvVSQMHpj0oorWJk9y0g36vcQEny3iYkfQcVm6t8yIDyAABn6UUVqIhjOAowMEcinyH/RHHUEgY9OaKKYG94cma6snSQ7gshUeuAKh1S2jtzY3Ea7ZLjcJMdDjpRRUS2LjuaGn/ACTw/wAW9DndzXs3wlAvppoZvniMW/aemQKKK45bnfTK/j+8ltfCmoCJynnRNG5HUjeO9fVX7D2k2r6L4gvvKUXElwkRYDooQcD2oorgxPwHqUtz6B1O1jjubgquNjDb7Uq26z3lsGyAV3EA4BNFFeFI9ZdBsR87U7hXAIUYHFJq6CSBUI4JxkdaKK5ZbmvQ8I+MM7uNAsyf3F7qirMB/Eq4IH09q8a+IUC6xdaylxnbZoPJ2HG3iiivSw/xI4avws8N8JahM3iN4jtIKkEkcn8a9IgmaReTRRX00dj4ut8bJm+7QGOaKKbMkPqRWPFFFQykA5NSDpRRVFH/2Q==");
		formato.setToken(token);
		
		HttpEntity<FormatoImagenPerfil> request = new HttpEntity<>(formato, cabecera);
		
		ResponseEntity<String> respuesta =
					testRestTemplate.exchange(uri,
											  HttpMethod.PUT, 
											  request,
											  String.class
										 );
				

		System.out.println(respuesta);
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	@Ignore("Listo")
	@Test
	public void loginOut() throws URISyntaxException {
		System.out.println("Login out");
		
		HttpHeaders cabecera = new HttpHeaders();
		
		HttpEntity<String> request = new HttpEntity<>(null, cabecera);
		
		String token = "4c30c12a2818afe40f854682f30c0a5df4c43494ad20d05d4d8a192f452fe3bd40781dd451ccd04ea90fbe5c24471ad9fa30d3ed5ecd2556d7123ed98ee436f9";
	
		ResponseEntity<String> respuesta = 
				testRestTemplate.exchange(
						  getRootUrl()+
						  "/api/servicios/loginout/{token}",
						  HttpMethod.GET,
						  request,
						  String.class,
						  token
						 );
		
		System.out.println(respuesta);
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
		assertNotNull(respuesta.getBody());
	}
	
	
	
} // Fin de la clase de pruebas
