package com.tecnobackendev.lottoringo.controladoresTest;

import static org.junit.Assert.*;

import java.nio.charset.Charset;

import org.apache.tomcat.util.codec.binary.Base64;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.tecnobackendev.lottoringo.LottoringoApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LottoringoApplication.class,
webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WebControllerTest {

	@Autowired
	private TestRestTemplate testRestTemplate;
	@LocalServerPort
	private int puerto;
	
	
	private String getRootUrl() {
        return "http://localhost:" + puerto;
    }
	
	@Before
	public void setUp() throws Exception {
		System.out.println("-------------------PRUEBAS DE INTEGRACION WEB-SERVICE-----------------");
	}
	
	@Ignore
	@Test
	public void test() {
		HttpHeaders cabecera = new HttpHeaders();
//		String username = "andyaacm@gmail.com";
//		String password = "6fa97762927c7f7237c3f4b147ea934e2c0c9329d5e675144af5dbd11a2bb3a553e3edf6e7a056057d83df83a1be54bb6c67b38cbfc406834c4a9a17f0badbfd";
//		String auth = username + ":" + password;
//		byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
//		String authHeader = "Basic " + new String( encodedAuth );
//		
//		cabecera.set( "Authorization", authHeader );
		HttpEntity<String> request = new HttpEntity<>(null, cabecera);
		
		
		String respuesta = 
				testRestTemplate.getForObject(getRootUrl()+"/applications", String.class);
		
		System.out.println(respuesta);
		assertNotNull(respuesta);
	}

}
