package com.tecnobackendev.lottoringo.serviciosTests;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tecnobackendev.lottoringo.LottoringoApplication;
import com.tecnobackendev.lottoringo.servicios.EmailService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LottoringoApplication.class)
public class EmailServiceTest {
	@Autowired
	private EmailService email;
	
	@Before
	public void setUp() throws Exception {
		System.out.println("Correo Electronico");
	}

	@Ignore("Listo")
	@Test
	public void test() {
		System.out.println("Enviando correo");
		email.enviarCorreoHTML("andyaacm@gmail.com", "PRueba LottoRingo2019", "Mensaje de prueba");
		
		Assert.assertTrue(true);
	}

}
