package com.tecnobackendev.lottoringo.serviciosTests;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tecnobackendev.lottoringo.LottoringoApplication;
import com.tecnobackendev.lottoringo.entidades.Sorteos;
import com.tecnobackendev.lottoringo.servicios.SorteosService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LottoringoApplication.class)
public class SorteosServiceTest {

	@Autowired
	private SorteosService sorteosService;
	
	@Before
	public void setUp() throws Exception {
		System.out.println("-------------------CARGANDO PRUEBAS DE INTEGRACION (Sorteos Service)----------------");
	}

	@Ignore("Listo")
	@Test
	public void test() {
		System.out.println("Prueba listado ganadores por fecha");
		List<Sorteos> lista = 
					sorteosService.listarSorteos(0, 10);
				
		System.out.println("cantidad sorteos: "+lista.size());
		lista.forEach(System.out::println);
		
		Assert.assertTrue(!lista.isEmpty());
	}

}
