package com.tecnobackendev.lottoringo.serviciosTests;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tecnobackendev.lottoringo.LottoringoApplication;
import com.tecnobackendev.lottoringo.entidades.Autenticacion;
import com.tecnobackendev.lottoringo.entidades.Paypal;
import com.tecnobackendev.lottoringo.entidades.Roles;
import com.tecnobackendev.lottoringo.entidades.Usuarios;
import com.tecnobackendev.lottoringo.servicios.AutenticacionService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LottoringoApplication.class)
public class AutenticacionServiceTest {
	
	@Autowired
	private AutenticacionService autenticacionService;
	
	@Before
	public void setUp() throws Exception{
		
		System.out.println("-------------------CARGANDO PRUEBAS DE INTEGRACION (AutenticacionService)----------------");
	}
	
	@Ignore("Listo")
	@Test
	public void listarAutenticacion() {
		System.out.println("listarAutenticacion");
		
		List<Autenticacion> elementos = autenticacionService.listarAutenticacion();
		System.out.println("Elementos: "+elementos.size());
		elementos.forEach(z->{
			System.out.println("id:"+z.getIdAutenticacion());
			System.out.println("usuario: "+z.getUsuarios().getNombreUsuario());
			Paypal p = z.getUsuarios().getPaypal();
			System.out.println("Paypal:"+ (Optional.ofNullable(p).isPresent() ? p.getCorreo() : "None"));
			
		});
		
		Assert.assertTrue(!elementos.isEmpty());
	}
	
	@Ignore("Listo")
	@Test
	public void buscarAutenticacionPorToken() {
		System.out.println("buscarAutenticacionPorToken");
		
		String tokenUsuario = "887375daec62a9f02d32a63c9e14c7641a9a8a42e4fa8f6590eb928d9744b57bb5057a1d227e4d40ef911ac030590bbce2bfdb78103ff0b79094cee8425601f5";
		Optional<Autenticacion> autenticacion = autenticacionService.buscarAutenticacionPorToken(tokenUsuario);
		
		System.out.println(autenticacion.get().toString());
		
		Assert.assertTrue(autenticacion.isPresent());
		
	}
	
	@Ignore("Listo")
	@Test
	public void buscarAutenticacionPorUsuario() {
		System.out.println("buscarAutenticacionPorUsuario");
		
		Optional<Autenticacion> autenticacion = autenticacionService.buscarAutenticacionPorUsuario("andy");
		System.out.println(autenticacion.get().toString());
		
		Assert.assertTrue(autenticacion.isPresent());
	}
	
	@Ignore("Listo")
	@Test
	public void agregarNuevaAutenticacion() {
		System.out.println("agregarNuevaAutenticacion");
		
		String tokenUsuario = "adfc9dbc14818864220085a69c5bb48665390df32992758b4a92ca655bb8286094994576cf71ec45a9296ace790afcc858dd457e320d41e19696c72453bc9d4d";
		
		Usuarios usuarios = new Usuarios();
		usuarios.setIdUsuarios(tokenUsuario);
		
		Roles roles = new Roles();
		roles.setIdRoles(2); // Participante
		
		Autenticacion nuevaAutenticacion = new Autenticacion();
		nuevaAutenticacion.setUsuarios(usuarios);
		nuevaAutenticacion.setPassword("555555555");
		nuevaAutenticacion.setRoles(roles);

		System.out.println(nuevaAutenticacion.toString());
		
		Autenticacion autenticacion = autenticacionService.agregarNuevaAutenticacion(nuevaAutenticacion);
		System.out.println(autenticacion.toString());
		
		Assert.assertNotNull(autenticacion);
	}
	
	@Ignore("Listo")
	@Test
	public void actualizarAutenticacion() {
		System.out.println("actualizarAutenticacion");

		String tokenUsuario = "adfc9dbc14818864220085a69c5bb48665390df32992758b4a92ca655bb8286094994576cf71ec45a9296ace790afcc858dd457e320d41e19696c72453bc9d4d";

		Autenticacion nuevaAutenticacion = autenticacionService.listarAutenticacion().stream()
				.filter(elemento -> elemento.getUsuarios().getIdUsuarios().equalsIgnoreCase(tokenUsuario))
				.findFirst()
				.get();

		nuevaAutenticacion.setPassword("(-.-)");

		System.out.println(nuevaAutenticacion.toString());

		Autenticacion autenticacion = autenticacionService.actualizarAutenticacion(nuevaAutenticacion);
		System.out.println(autenticacion.toString());

		Assert.assertNotNull(autenticacion);
	}
	
	@Ignore("Listo")
	@Test
	public void borrarAutenticacion() {
		System.out.println("borrarAutenticacion");
		
		String tokenUsuario = "adfc9dbc14818864220085a69c5bb48665390df32992758b4a92ca655bb8286094994576cf71ec45a9296ace790afcc858dd457e320d41e19696c72453bc9d4d";
	
		Autenticacion autenticacion = autenticacionService.listarAutenticacion().stream()
				.filter(elemento -> elemento.getUsuarios().getIdUsuarios().equalsIgnoreCase(tokenUsuario))
				.findFirst()
				.get();

		System.out.println(autenticacion.toString());
		
		autenticacionService.borrarAutenticacion(autenticacion.getIdAutenticacion());
		System.out.println("Borrado");
		
		Assert.assertNotNull(autenticacion);
	}
	
	@Ignore("Listo")
	@Test
	public void listarAutenticacionFiltrada() {
		System.out.println("listarAutenticacionFiltrada");
		
		List<Autenticacion> elementos = 
				autenticacionService.listarAutenticacion()
									.stream()
									.filter(elemento -> {
										System.out.println("tipoRol: "+elemento.getRoles().getTipo());
										return (elemento.getRoles().getTipo().equalsIgnoreCase("Jefe") || 
										elemento.getRoles().getTipo().equalsIgnoreCase("Administrador"));
									                    }
									)
									.collect(Collectors.toList());
		
		elementos.forEach(System.out::println);
		
		Assert.assertTrue(!elementos.isEmpty());
	}

}
