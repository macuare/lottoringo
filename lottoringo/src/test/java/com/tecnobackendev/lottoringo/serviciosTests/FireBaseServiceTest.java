package com.tecnobackendev.lottoringo.serviciosTests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tecnobackendev.lottoringo.LottoringoApplication;
import com.tecnobackendev.lottoringo.servicios.FireBaseService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LottoringoApplication.class)
public class FireBaseServiceTest {

	@Autowired
	private FireBaseService fs;
	
	@Before
	public void setUp() throws Exception {
		System.out.println("--------------TEST FIREBASE-----------------");
	}

	@Ignore("Listo")
	@Test
	public void test() {
		
		fs.inicializarServicio();
		fs.enviarMensaje("Prueba","Saludos soy andy. Esta es sin la inicializacion otra vez.");
		
		Assert.assertTrue(true);
	}

}
