package com.tecnobackendev.lottoringo.serviciosTests;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tecnobackendev.lottoringo.LottoringoApplication;
import com.tecnobackendev.lottoringo.entidades.Boletos;
import com.tecnobackendev.lottoringo.entidades.Ganadores;
import com.tecnobackendev.lottoringo.entidades.Sorteos;
import com.tecnobackendev.lottoringo.entidades.Usuarios;
import com.tecnobackendev.lottoringo.servicios.BoletosService;
import com.tecnobackendev.lottoringo.servicios.GanadoresService;
import com.tecnobackendev.lottoringo.servicios.SorteosService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LottoringoApplication.class)
public class BolestosServiceTest {
	@Autowired
	private BoletosService boletosService;
	@Autowired
	private SorteosService sorteosService;
	@Autowired
	private GanadoresService ganadoresService;
	
	
	@Before
	public void setUp() throws Exception {
		System.out.println("-------------------CARGANDO PRUEBAS DE INTEGRACION (BoletosService)----------------");
	}
	
	@Ignore("Listo")
	@Test
	public void test() {
		List<Boletos> lista = 
				boletosService.listarBoletos();
		
		lista.forEach(System.out::println);
		/*
		Boletos [idBoletos=1, bola1=3, bola2=8, bola3=23, bola4=12, bola5=19, bolaSuerte=30, fechaCreacion=2019-06-08 20:44:40.0, usuarios=2e356bd0d87b1d104cf43eff925a811b16c5766f14c826588678c1218e982dcc3f80af84b1eb2a2a05c5a87d04a9b75466cfef3bc8b4c7f434670591cb68b1a5, sorteos=1]
		Boletos [idBoletos=2, bola1=34, bola2=16, bola3=20, bola4=5, bola5=2, bolaSuerte=13, fechaCreacion=2019-06-08 20:47:40.0, usuarios=2e356bd0d87b1d104cf43eff925a811b16c5766f14c826588678c1218e982dcc3f80af84b1eb2a2a05c5a87d04a9b75466cfef3bc8b4c7f434670591cb68b1a5, sorteos=1]
		Boletos [idBoletos=3, bola1=18, bola2=46, bola3=17, bola4=7, bola5=5, bolaSuerte=12, fechaCreacion=2019-06-08 20:44:45.0, usuarios=483afc42bef778701d047ac06294400c2326d1955e1f6481cf41e15b01c49a4f723de1c2055fa47a3c3e631748778a95c9c09791e6900b540d68337bf7d2a16f, sorteos=2]
		Boletos [idBoletos=4, bola1=18, bola2=46, bola3=1, bola4=7, bola5=35, bolaSuerte=54, fechaCreacion=2019-06-08 20:44:49.0, usuarios=99992757fae3944bf7ca33e2f48e1af1409889791a9f9a698afb045b720ee3e85484adedc6cd2eb5d7231a465f0f324530dbbca9b75eba19192bbd77adfd5f3c, sorteos=2]
		 * */
		Assert.assertTrue(!lista.isEmpty());
	}

	@Ignore("Listo")
	@Test
	public void buscarPorUsuario() {
		System.out.println("Buscar por usuario");
		
		Usuarios usuario = new Usuarios();
		usuario.setIdUsuarios("99992757fae3944bf7ca33e2f48e1af1409889791a9f9a698afb045b720ee3e85484adedc6cd2eb5d7231a465f0f324530dbbca9b75eba19192bbd77adfd5f3c");
		
		List<Boletos> lista = 
				boletosService.buscarBoletosPorUsuario(usuario);
		
		System.out.println("cantidad: "+lista.size());
		
		lista.forEach(System.out::println);
		
		Assert.assertTrue(!lista.isEmpty());
		
	}
	
	@Ignore("Listo")
	@Test
	public void buscarPorSorteo() {
		System.out.println("Buscar por Sorteo");
		//2019-06-23 18:44:40
		LocalTime lt = LocalTime.of(12, 0, 0);
		LocalDate ld = LocalDate.parse("06/04/2019", DateTimeFormatter.ofPattern("MM/d/yyyy"));
		LocalDateTime fechaBusqueda = LocalDateTime.of(ld, lt); 
		
		Optional<Sorteos> sorteo =
				sorteosService.listarSorteos()
							  .stream()
							  .filter(entidad ->{
								  
								  LocalDateTime ldt = 
										  LocalDateTime.ofInstant(entidad.getFechaSorteo().toInstant(),
		                                  						  ZoneId.systemDefault());
								  
								  boolean decision = 
										  (
										    ldt.getDayOfMonth() == fechaBusqueda.getDayOfMonth() &&
										    ldt.getMonthValue() == fechaBusqueda.getMonthValue() &&
										    ldt.getYear() == fechaBusqueda.getYear()
										  );
								  
								  return decision;
							  }).findFirst();
		
		List<Boletos> lista = null;
		
		if(sorteo.isPresent()) {
			System.out.println(sorteo.get().toString());
			lista = 
					boletosService.buscarBoletosPorSorteo(sorteo.get());
		}else {
			System.out.println("Sorteo por la fecha no existe");
		}
		
				
		
		System.out.println("cantidad: "+lista.size());
		
		lista.forEach(System.out::println);
		
		Assert.assertTrue(!lista.isEmpty());
		
	}

	@Ignore("Listo")
	@Test
	public void buscarPorBoletoListado() {
		System.out.println("Buscar por Boleto comprado que sea ganador");
		
		Optional<Sorteos> sorteo = 
				sorteosService.buscarSorteoPorId(6);
		
		System.out.println(sorteo.get().toString());
		
		List<Boletos> boletosValidos = 
				boletosService.buscarBoletosPorSorteo(sorteo.get());
		
		boletosValidos.forEach(System.out::println);
		
		List<Ganadores> listaGanadores = 
				ganadoresService.listarGanadores(boletosValidos);
		
		listaGanadores.forEach(System.out::println);
		
		ganadoresService.borrarGanadores(listaGanadores);
		
		System.out.println("Nueva Lista");
		List<Ganadores> listaLimpieza = 
				ganadoresService.listarGanadores(boletosValidos);
		
		listaLimpieza.forEach(System.out::println);
		
		Assert.assertTrue(true);
		
	}
	
	
	
	
	
}
