package com.tecnobackendev.lottoringo.serviciosTests;

import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tecnobackendev.lottoringo.LottoringoApplication;
import com.tecnobackendev.lottoringo.entidades.Usuarios;
import com.tecnobackendev.lottoringo.servicios.UsuariosService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LottoringoApplication.class)
public class UsuariosServiceTest {
	
	@Autowired
	private UsuariosService usuariosService;

	@Before
	public void setUp() throws Exception {
		System.out.println("-------------------CARGANDO PRUEBAS DE INTEGRACION (UsuariosService)----------------");
	}

	
	@Ignore("Listo")
	@Test
	public void listarLosUsuarios() {
		System.out.println("listarLosUsuarios");
		
		List<Usuarios> elementos = usuariosService.listarLosUsuarios(); 
		elementos.forEach(System.out::println);
		
		Assert.assertTrue(!elementos.isEmpty());
	}
	
	@Ignore("Listo")
	@Test
	public void buscarPorToken() {
		System.out.println("buscarPorToken");
		
		String tokenUsuario = "887375daec62a9f02d32a63c9e14c7641a9a8a42e4fa8f6590eb928d9744b57bb5057a1d227e4d40ef911ac030590bbce2bfdb78103ff0b79094cee8425601f5";
	
		Optional<Usuarios> usuarios = usuariosService.buscarPorToken(tokenUsuario);
		
		System.out.println(usuarios.get().toString());
		
		Assert.assertTrue(usuarios.isPresent());
	}
	
	@Ignore("Listo")
	@Test
	public void buscarPorUsuario() {
		System.out.println("buscarPorUsuario");
		
		String nombreUsuario = "profesor";
		
		Optional<Usuarios> usuarios = usuariosService.buscarPorUsuario(nombreUsuario);
		
		System.out.println(usuarios.get().toString());
		
		Assert.assertTrue(usuarios.isPresent());
	}
	
	@Ignore("Listo")
	@Test
	public void agregarNuevoUsuario() {
		System.out.println("agregarNuevoUsuario");
		
		Usuarios nuevoUsuario = new Usuarios();
		nuevoUsuario.setIdUsuarios("f03d4eed1f2949b3d09d2c330744f4d7221aff13278702e5c95c6281b643f9ea0bbeaefbbc6ddcb04e373a6aaa362e21eba5679e667aca99dc39eea14f5779be");
		nuevoUsuario.setNombreUsuario("Rosa");
		nuevoUsuario.setCorreo("virginia@gmail.com");
		nuevoUsuario.setImagen("bella.jpg");
		
		Usuarios usuarios = usuariosService.agregarNuevoUsuario(nuevoUsuario);
				
		System.out.println(usuarios.toString());
		
		Assert.assertNotNull(usuarios);
	}
	
	@Ignore("Listo")
	@Test
	public void actualizarUsuario() {
		System.out.println("actualizarUsuario");
		
		Usuarios nuevoUsuario = new Usuarios();
		nuevoUsuario.setIdUsuarios("f03d4eed1f2949b3d09d2c330744f4d7221aff13278702e5c95c6281b643f9ea0bbeaefbbc6ddcb04e373a6aaa362e21eba5679e667aca99dc39eea14f5779be");
		nuevoUsuario.setNombreUsuario("Virgina");
		nuevoUsuario.setCorreo("virginia@gmail.com");
		nuevoUsuario.setImagen("bella.jpg");
		
		Usuarios usuarios = usuariosService.actualizarUsuario(nuevoUsuario);
				
		System.out.println(usuarios.toString());
		
		Assert.assertNotNull(usuarios);
	}
	
	@Ignore("Listo")
	@Test
	public void borrarUsuario() {
		System.out.println("borrarUsuario");
		
		String tokenUsuario = "f03d4eed1f2949b3d09d2c330744f4d7221aff13278702e5c95c6281b643f9ea0bbeaefbbc6ddcb04e373a6aaa362e21eba5679e667aca99dc39eea14f5779be";
		
		usuariosService.borrarUsuario(tokenUsuario);
				
		System.out.println(tokenUsuario);
		
		Assert.assertNotNull(tokenUsuario);
	}
	
}
