package com.tecnobackendev.lottoringo.serviciosTests;

import static org.junit.Assert.*;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tecnobackendev.lottoringo.LottoringoApplication;
import com.tecnobackendev.lottoringo.entidades.Ganadores;
import com.tecnobackendev.lottoringo.servicios.GanadoresService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LottoringoApplication.class)
public class GanadoresServiceTest {
	@Autowired
	private GanadoresService ganadoresService;
	
	@Before
	public void setUp() throws Exception {
		System.out.println("-------------------CARGANDO PRUEBAS DE INTEGRACION (GanadoresService)----------------");
	}

	@Ignore("Listo")
	@Test
	public void test() {
		List<Ganadores> lista = 
				ganadoresService.listarGanadores();
		
		lista.forEach(System.out::println);
		
		System.out.println("======================================");
		
		List<Ganadores> lista2 = 
				ganadoresService.listarGanadores(0, 10);
		lista2.forEach(System.out::println);
		
		Assert.assertTrue(!lista.isEmpty());
	}
	
	@Ignore("Listo")
	@Test
	public void test2() {
		List<Ganadores> lista = 
				ganadoresService.listarGanadores();
		
		List<Ganadores> listadoGanadores =
				  ganadoresService.listarGanadores()
								  .stream()
								  .filter(ganadores -> ganadores.isStatus() == false)
								  .collect(Collectors.toList());
		
		int sinCuentaPaypal = 
				(int) listadoGanadores.stream()
									  .filter(entidad -> entidad.getBoletos().getUsuarios().getPaypal().getCorreo().equalsIgnoreCase("none"))
									  .count();
		
		System.out.println("sinpaypal: "+sinCuentaPaypal);
		
		Assert.assertTrue(!lista.isEmpty());
	}
	
	
	

}
